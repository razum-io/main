function show_image(key) {
	pathArray = window.location.href.split('/');
	protocol = pathArray[0];
	host = pathArray[2];
	url = base_url + 'assets/css/images/farve/'
			+ key + '.jpg';
	$('#farveimage').attr('src', url);
}

// new function for custom colors
function show_image2(key) {
	$(".roof_type_img").css( "opacity" , 0.4);
	//alert("#roof_type_img"+key);
	$("#roof_type_img"+key).css( "opacity" , 1);
}

function tagtype(baseurl) {
	antalVinklerVinkel = 0;
	antalVinkler = 0;
	antalTilbygninger = 0;
	antalKnaster = 0;
	// alert($("#vinklervinkelhus option:selected").index());
	if ($("#hovedhus1").prop("checked")) {
		hoved = 10000;
		$("#tilb2").prop('disabled', false);
		$("#tilb3").prop('disabled', false);
		$("#knaster").prop('disabled', true);
		$("#kvist2").prop('disabled', false);
		$("#vinkel45grader").prop('disabled', true);
		$("#vinklervinkelhus").prop('disabled', true);
		$("#tilbygninger").prop('disabled', true);
	}
	if ($("#hovedhus2").prop("checked")) {
		hoved = 20000;
		$("#tilb2").prop('disabled', false);
		$("#tilb3").prop('disabled', false);
		$("#knaster").prop('disabled', true);
		$("#kvist2").prop('disabled', false);
		$("#vinkel45grader").prop('disabled', true);
		$("#tilbygninger").prop('disabled', true);
		if ($("#tilb2").prop("checked") || $("#tilb3").prop("checked")) {
			$("#vinklervinkelhus").prop('disabled', true);
			$("#vinklervinkelhus").val('1vinkel');
		} else {
			$("#vinklervinkelhus").prop('disabled', false);
		}
		antalVinklerVinkel = $("#vinklervinkelhus option:selected").index() + 1;
		// console.log(!$("#tilb2").prop('checked'));
		if (antalVinklerVinkel !== 2 && !$("#tilb2").prop('checked')) {
			dynValmPlacering[2] = "3";
			dynValmType[2] = 2;
		} else {
			dynValmPlacering[1] = "1";
			dynValmType[1] = 2;
		}
		if ($("#vinklervinkelhus option:selected").index() > 0) {
			$("#tilb2").prop('disabled', true);
			$("#tilb3").prop('disabled', true);
		}
		if (antalVinklerVinkel == 2) {
			dynValmPlacering[4] = "4";
			dynValmType[4] = 2;
		}
	}
	if ($("#hovedhus3").prop("checked")) {
		hoved = 30000;
		$("#tilb2").prop('disabled', true);
		$("#tilb3").prop('disabled', true);
		$("#knaster").prop('disabled', true);
		$("#vinkel45grader").prop('disabled', true);
		$("#vinklervinkelhus").prop('disabled', true);
		$("#tilbygninger").prop('disabled', true);
		$("#kvist2").prop('disabled', true);
		$("#kvist3").prop('disabled', true);
		$("#kvist4").prop('disabled', true);
		$("#valm1").prop('disabled', false);
		$("#valm2").prop('disabled', true);
		$("#valm3").prop('disabled', true);

	}
	if ($("#hovedhus4").prop("checked")) {
		hoved = 40000;
		$("#tilb2").prop('disabled', true);
		$("#tilb3").prop('disabled', true);
		$("#knaster").prop('disabled', true);
		$("#vinkel45grader").prop('disabled', true);
		$("#vinklervinkelhus").prop('disabled', true);
		$("#tilbygninger").prop('disabled', true);
		$("#kvist2").prop('disabled', true);
		$("#kvist3").prop('disabled', true);
		$("#kvist4").prop('disabled', true);
		$("#valm1").prop('disabled', false);
		$("#valm2").prop('disabled', true);
		$("#valm3").prop('disabled', true);
	}
	if ($("#hovedhus5").prop("checked")) {
		hoved = 50000;
		$("#vinklervinkelhus").prop('disabled', true);
		$("#tilb2").prop('disabled', true);
		$("#tilb3").prop('disabled', true);
		$("#knaster").prop('disabled', true);
		$("#kvist2").prop('disabled', true);
		$("#kvist3").prop('disabled', true);
		$("#kvist4").prop('disabled', true);
		$("#tilbygninger").prop('disabled', true);
		antalVinkler = $("#vinkel45grader option:selected").index() + 1;
		$("#vinkel45grader").prop('disabled', false);
		$("#valm1").prop('disabled', false);
		$("#valm2").prop('disabled', false);
		$("#valm3").prop('disabled', false);
	}
	if ($("#hovedhus6").prop("checked")) {
		hoved = 60000;
		$("#valm1").prop('disabled', true);
		$("#valm2").prop('disabled', false);
		$("#valm2").prop("checked", true);
		$("#valm3").prop('disabled', true);
		$("#tilb2").prop('disabled', false);
		$("#tilb3").prop('disabled', true);
		$("#knaster").prop('disabled', true);
		$("#vinkel45grader").prop('disabled', true);
		$("#vinklervinkelhus").prop('disabled', true);
		$("#kvist2").prop('disabled', false);
		$("#tilbygninger").prop('disabled', true);
		if ($("#kvist4").prop("checked") == true) {
			$("#kvist2").prop("checked", true);
		}
	}
	if ($("#hovedhus7").prop("checked")) {
		hoved = 70000;
		$("#tilb1").prop("checked", true);
		$("#tilb2").prop('disabled', true);
		$("#tilb3").prop('disabled', true);
		$("#knaster").prop('disabled', true);
		$("#valm1").prop('disabled', false);
		$("#valm2").prop('disabled', false);
		$("#valm3").prop('disabled', false);
		$("#kvist2").prop('disabled', false);
		$("#vinkel45grader").prop('disabled', true);
		$("#vinklervinkelhus").prop('disabled', true);
		$("#tilbygninger").prop('disabled', true);
	}
	if ($("#tilb1").prop("checked")) {
		tilbyg = 1000;
		$("#hovedhus2").prop('disabled', false);
		$("#hovedhus5").prop('disabled', false);
		$("#hovedhus6").prop('disabled', false);
		$("#hovedhus7").prop('disabled', false);
	}
	if ($("#tilb2").prop("checked")) {
		tilbyg = 2000;
		$("#hovedhus2").prop('disabled', false);
		$("#hovedhus3").prop('disabled', true);
		$("#hovedhus4").prop('disabled', true);
		$("#hovedhus5").prop('disabled', true);
		$("#hovedhus6").prop('disabled', false);
		$("#hovedhus7").prop('disabled', false);
		$("#vinklervinkelhus").prop('disabled', true);
		if ($("#vinklervinkelhus option:selected").index() !== -1
				&& $("#hovedhus2").prop("checked")) {
			$("#tilbygninger").val('1tilbygning');
		}
		if ($("#hovedhus1").prop("checked")) {
			$("#tilbygninger").prop('disabled', false);
		} else {
			$("#tilbygninger").prop('disabled', true);
			$("#tilbygninger").val('1tilbygning');
		}
		antalTilbygninger = $("#tilbygninger option:selected").index() + 1;
	}
	if ($("#tilb3").prop("checked")) {
		tilbyg = 3000;
		$("#hovedhus2").prop('disabled', false);
		$("#hovedhus3").prop('disabled', true);
		$("#hovedhus4").prop('disabled', true);
		$("#hovedhus5").prop('disabled', true);
		$("#hovedhus6").prop('disabled', true);
		$("#hovedhus7").prop('disabled', true);

		$("#knaster").prop('disabled', false);
		if ($("#hovedhus1").prop("checked")) {
			$("#knaster").prop('disabled', false);
		} else {
			$("#knaster").prop('disabled', true);
			$("#knaster").val('1knast');
		}
		antalKnaster = $("#knaster option:selected").index() + 1;
	}
	if ($("#valm1").prop("checked")) {
		valm = 100;
		$("#kvist3").prop('disabled', true);
		$("#kvist4").prop('disabled', true);
	}
	if ($("#valm2").prop("checked")) {
		valm = 200;
		if (!$("#kvist2").prop('disabled')) {
			$("#kvist3").prop('disabled', false);
		}
		$("#kvist4").prop('disabled', true);
	}
	if ($("#valm3").prop("checked")) {
		valm = 300;
		$("#kvist3").prop('disabled', true);
		if (!$("#kvist2").prop('disabled')) {
			$("#kvist4").prop('disabled', false);
		}
	}
	valmtilbyg = 0;
	if (valm > 100) {
		$("#hovedhus3").prop('disabled', true);
		$("#hovedhus4").prop('disabled', true);
	}

	if ($("#kvist1").prop("checked")) {
		kvist = 1;
		AntalKviste = 0;
	}
	if ($("#kvist2").prop("checked")) {
		kvist = 2;
	}
	if ($("#kvist3").prop("checked")) {
		kvist = 3;
		$("#valm1").prop('disabled', true);
		$("#valm3").prop('disabled', true);
	}
	if ($("#kvist4").prop("checked")) {
		kvist = 4;
		$("#valm1").prop('disabled', true);
		$("#valm2").prop('disabled', true);
	}
	if (kvist > 1) {
		$("#hovedhus3").prop('disabled', true);
		$("#hovedhus3").prop('disabled', true);
		$("#hovedhus3").prop('disabled', true);
	}
	if ($("#kvist1").prop("checked") && hoved < 21000) {
		$("#valm1").prop('disabled', false);
		$("#valm2").prop('disabled', false);
		$("#valm3").prop('disabled', false);
	}
	if ($("#kvist2").prop("checked") && hoved < 21000) {
		$("#valm1").prop('disabled', false);
		$("#valm2").prop('disabled', false);
		$("#valm3").prop('disabled', false);
	}
	if ($("#kvist3").prop("checked") && hoved < 21000) {
		$("#valm2").prop('disabled', false);
	}
	if ($("#kvist4").prop("checked") && hoved < 21000) {
		$("#valm3").prop('disabled', false);
	}
	if (tilbyg == 4000) {
		$("#valm2").prop('disabled', true);
		$("#valm3").prop('disabled', true);
		$("#kvist2").prop('disabled', true);
		$("#kvist3").prop('disabled', true);
		$("#kvist4").prop('disabled', true);
	}
	if (tilbyg == 1000 && kvist == 1 && valm == 100) {
		$("#hovedhus3").prop('disabled', false);
		$("#hovedhus4").prop('disabled', false);
	}

	if ($("#kvist1").prop("checked")) {
		$('#antalkviste').prop('disabled', true);
		$('#antalkviste').val('0');
	} else {
		$('#antalkviste').prop('disabled', false);
	}

	tag = hoved + tilbyg + valm + valmtilbyg + kvist;
	drawillustration('tagtype');
	genererIllustration(tag, '', true, true, false, false, 'tagtype');
	addVariable('antalKnaster', antalKnaster);
	addVariable('antalTilbygninger', antalTilbygninger);
	addVariable('antalVinkler', antalVinkler);
	addVariable('antalVinklerVinkel', antalVinklerVinkel);
}

function addVariable(name, value, form) {
	// delete previous var
	if ($('#' + name)) {
		$('#' + name).remove();
	}

	// Get a reference to the form
	if (form == undefined) {
		form = document.getElementById("tagtype");
	} else {
		form = document.getElementById(form);
	}
	try {
		// Create an element (IE)
		element = document
				.createElement("<input name='newfield' type='hidden' value='success' />");
	} catch (e) {
		// Create ane element for everyone else and set its attributes.
		element = document.createElement("input");
		element.setAttribute("type", "hidden");
		element.setAttribute("name", name);
		element.setAttribute("value", value);
		element.setAttribute("id", name);
	}
	// Add the input field to the form
	form.appendChild(element);
}
function tagtypesubm() {
	if ($('#antalkviste').val() == 0 && !$("#antalkviste").prop('disabled')) {
		$('#sbHolder_'+$('#antalkviste').attr('sb')).css('background-color','#E97451');
		alert('Der mangler data i et indtastningsfelt');
		return;
	}
	
	$('#tagtype').submit();
}

function valmfrm(tag, hoved, typevalm, antalVinklerVinkel, antaltilbygninger,
		laengehus) {
	antalValgfrie = 0;
	if (hoved == 2) {
		if (antalVinklerVinkel !== 2 && antaltilbygninger !== 1) {
			dynValmPlacering[2] = '3';
			dynValmType[2] = 2;
		} else {
			dynValmPlacering[1] = '1';
			dynValmType[1] = 2;
			if (antaltilbygninger !== 1) {
				dynValmPlacering[2] = '4';
				dynValmType[2] = 2;
			}
		}
	}
	lang = '';
	genererIllustration(tag, '', false, true, false, false, 'valm');
	genererIllustration(tag, '', true, false, false, false, 'valm');
	console.log(dynValmType);
	console.log(dynValmPlacering);
	danFelter(laengehus, "", typevalm);

	addVariable('dynValmPlacering', JSON.stringify(dynValmPlacering), 'valm');
	addVariable('dynValmType', JSON.stringify(dynValmType), 'valm');
	for ( var i = 1; i <= 6; i++) {
		if (dynValmPlacering[i] == 0) {
			$('#valmblock' + i).hide();
		}
	}
}
function valmchange(placering, type) {
	if (placering !== undefined && type !== undefined) {
		dynValmType[placering] = type;
		genererIllustration(tag, '', false, true, false, false, 'valm');
		genererIllustration(tag, '', true, false, false, false, 'valm');
		console.log(dynValmPlacering);
		console.log(dynValmType);
		console.log(laengehus);
	} 
	antalTilbygninger = antaltilbygninger;
	drawillustration('valm');
	if (hoved == 5){
		$('#imgcont3d').css('padding-top','30px'); 
	}
}
function danFelter(medsendBygning, niveau, typevalm) {

	// ----------------Side 1----------------------F
	if (medsendBygning.side1GavlTypeValgfri == true) {
		antalValgfrie = antalValgfrie + 1;
		dynValmPlacering[antalValgfrie] = niveau + "1";
		visRektangel(antalValgfrie, typevalm);
	} else {
		// Der kan kun være en tilbygning, hvis man ikke samtidig skal vælge
		// valm type
		if (medsendBygning.side1TilbygningAktiv == true) {
			tempNiveau = niveau + "1";
			danFelter(medsendBygning.side1Tilbygning.bygning, tempNiveau,
					typevalm);
		}
	}
	// ------------------------------------------------------
	if (medsendBygning.side2GavlTypeValgfri == true) {
		antalValgfrie = antalValgfrie + 1;
		dynValmPlacering[antalValgfrie] = niveau + "2";
		visRektangel(antalValgfrie, typevalm);
	} else {
		// Der skal tjekkes for tilbygning
		if (medsendBygning.side2TilbygningAktiv == true) {
			tempNiveau = niveau + "2";
			danFelter(medsendBygning.side2Tilbygning.bygning, tempNiveau,
					typevalm);
		}
	}

	// ------------------------------------------------------
	if (medsendBygning.side3GavlTypeValgfri == true) {
		antalValgfrie = antalValgfrie + 1;
		dynValmPlacering[antalValgfrie] = niveau + "3";
		visRektangel(antalValgfrie, typevalm);
	} else {
		if (medsendBygning.side3TilbygningAktiv == true) {
			tempNiveau = niveau + "3";
			danFelter(medsendBygning.side3Tilbygning.bygning, tempNiveau,
					typevalm);
		}
	}

	// ------------------------------------------------------
	if (medsendBygning.side4GavlTypeValgfri == true) {
		antalValgfrie = antalValgfrie + 1;
		dynValmPlacering[antalValgfrie] = niveau + "4";
		visRektangel(antalValgfrie, typevalm);
	} else {
		if (medsendBygning.side4TilbygningAktiv == true) {
			tempNiveau = niveau + "4";
			danFelter(medsendBygning.side4Tilbygning.bygning, tempNiveau,
					typevalm);
		}
	}
}
function visRektangel(index, valmType) {
	dynValmVinkel[index] = 0;
	dynValmTop[index] = 0;
	if (hoved == 2 && (antalVinklerVinkel == 2 || antalTilbygninger == 1)) {
		if (index == 1) {
			dynValmPlacering[index] = "1";
			dynValmType[index] = 2;
		} else {
			if (index == 4 && antalTilbygninger != 1) {
				dynValmPlacering[index] = "4";
				dynValmType[index] = 2;
			} else {
				dynValmType[index] = valmType;
			}
		}

	} else {

		if (hoved == 2 && index == 2) {
			dynValmPlacering[index] = "3";
			dynValmType[index] = 2;
		} else {

			dynValmType[index] = valmType;

		}
	}

}
function vinkel45(stenvalgt, antalvinkler) {
	$('input[name=under]')[0].click();
	$('input[name=valg_af]')[0].click();
	if (stenvalgt == 0) {
		$('#valgaf').show();
	} else {
		$('#valgaf').hide();
	}
	if (antalvinkler == 2) {
		$('#labeld').text('d: Længde på hus:');
		$('#labele').text('e: Bredde på hus:');
		$('.datavinkel2').show();

	} else {
		$('.datavinkel2').hide();
		$('#labeld').text('d: Længde på hus incl. udhæng:');
		$('#labele').text('e: Bredde på hus incl. udhæng:');
	}
}
function hvhus1(stenvalgt) {
	$('input[name=under]')[0].click();
	$('input[name=valg_af]')[0].click();
	if (stenvalgt == 0) {
		$('#valgaf').show();
	} else {
		$('#valgaf').remove();
	}

}
function hvhus3() {
	$('input[name=under]')[0].click();
}
function hvhus2(hoved, stenvalgt, tilbyg, antalVinklerVinkel,
		antalTilbygninger, antalKnaster) {
	$('input[name=under]')[0].click();
	$('input[name=valg_af]')[0].click();
	if (stenvalgt == 0) {
		$('#valgaf').show();
	} else {
		$('#valgaf').hide();
	}
	if (tilbyg == 2 || tilbyg == 3 || tilbyg == 4 || antalVinklerVinkel == 2) {
		$('#l3').show();
		$('#b3').show();
		$('#label3').show();
	}
	if (tilbyg == 2 || tilbyg == 4 || antalVinklerVinkel == 2) {
		// vinkel for tilbygning
		$('#v3').show();
		// Picture3.Visible = false
	}
	if (tilbyg == 3) { // ingen indtastning af vinkel
		$('#v3').hide();
		// Picture3.Visible = true
	}
	if (tilbyg == 1 && antalVinklerVinkel !== 2 && antalTilbygninger !== 2) {
		$('#label3').hide();
		$('#l3').hide();
		$('#b3').hide();
		$('#v3').hide();
		// Picture3.Visible = false
	}
	if (hoved == 2 || antalTilbygninger == 2) {
		$('#l2').show();
		$('#b2').show();
		$('#v2').show();
	} else {
		$('#l2').hide();
		$('#b2').hide();
		$('#v2').hide();
	}
	if ((hoved == 1 || hoved == 6) && antalTilbygninger !== 2 && tilbyg > 1) {
		$('#label3').hide();
		if (hoved == 1 && antalKnaster == 2) {
			$('#l2').show();
			$('#b2').show();
			$('#v2').hide();
		}
	}
	if (hoved == 2) {
		$('#label2').text('Data for vinkel:');
	}

	if ((hoved == 1 || hoved == 6) && (tilbyg == 2 || tilbyg == 4)) {
		$('#label2').text('Data for tilbygning:');
	}

	if (hoved == 1 && tilbyg == 3) {
		$('#label2').text('Data for knast:');
		$('#label_d').text('d: Knastens længde:');
		$('#label_e').text('e: Knastens bredde:');
		$('#label_g').text('g: Knastens længde:');
		$('#label_h').text('h: Knastens bredde:');
		// Picture3.Picture = LoadPicture(App.path &
		// "\gfx\maelsaetninger\knast.bmp")
	}
	if (hoved == 2 && tilbyg == 3) {
		$('#label3').text('Data for knast:');
		$('#label_g').text('g: Knastens længde:');
		$('#label_h').text('h: Knastens bredde:');

		// Picture3.Picture = LoadPicture(App.path &
		// "\gfx\maelsaetninger\knast2.bmp")
	} else {
		if (antalKnaster == 2 && hoved == 1 && tilbyg == 3) {
			$('#label3').text('Data for knast 2:');
			$('#label_g').text('g: Knastens længde:');
			$('#label_h').text('h: Knastens bredde:');
		} else {
			$('#label3').text('Data for tilbygning:');
		}
	}
	if (hoved == 1 && tilbyg == 2) {
		// Picture2.Picture = LoadPicture(App.path &
		// "\gfx\maelsaetninger\hvhus_tilbyg.bmp")
		$('#label2').show();
	}

	if (hoved == 1 && (tilbyg == 1 || tilbyg == 3)) {
		// Picture2.Picture = LoadPicture(App.path &
		// "\gfx\maelsaetninger\hv1_m.bmp")
		$('#label2').hide();
		$('#label3').hide();
	}

	if (hoved == 2 && tilbyg == 1) {
		// Picture2.Picture = LoadPicture(App.path &
		// "\gfx\maelsaetninger\hv2_m.bmp")
		$('#label2').show();
		$('#label3').hide();
	}

	if (hoved == 2 && tilbyg == 2) {
		// Picture2.Picture = LoadPicture(App.path &
		// "\gfx\maelsaetninger\hvhus_tilbyg+v.bmp")
		$('#label2').show();
		$('#label3').show();
	}
	if (hoved == 2 && antalVinklerVinkel == 2) {
		// Picture2.Picture = LoadPicture(App.path &
		// "\gfx\maelsaetninger\hvhus_tilbyg+v.bmp")
		$('#label2').text('Data for vinkel 1:');
		$('#label3').text('Data for vinkel 2:');
		$('#label3').show();
	}
	if (hoved == 1 && antalTilbygninger == 2) {
		// Picture2.Picture = LoadPicture(App.path &
		// "\gfx\maelsaetninger\hv1-2tilbyg.bmp")
		$('#label2').text('Data for tilbygning 1:');
		$('#label3').text('Data for tilbygning 2:');
		$('#label3').show();
	}
	if (tilbyg == 3) { // ingen indtastning af vinkel
		if (hoved == 1) {
			if (antalKnaster == 2) {
				$('#label3').show();
				$('#labeld').text('f: Knastens længde:');
				$('#labele').text('g: Knastens bredde:');
			}
			$('#label2').show();
			$('#labelg').text('d: Knastens længde:');
			$('#labelh').text('e: Knastens bredde:');

		} else {
			$('#labelg').text('g: Knastens længde:');
			$('#labelh').text('h: Knastens bredde:');
		}
	} else {
		if ((hoved == 1 || hoved == 6) && antalTilbygninger !== 2) {
			$('#labelg').text('d: Længde på hus incl. udhæng:');
			$('#labelh').text('e: Bredde på hus incl. udhæng:');
			$('#labeli').text('f: Taghældning');
		} else {
			$('#labelg').text('g: Længde på hus incl. udhæng:');
			$('#labelh').text('h: Bredde på hus incl. udhæng:');
		}
	}
	for ( var i = 2; i <= 3; i++) {
		if ($('#l' + i).is(":hidden")) {
			$('#l' + i).remove();
		}
		if ($('#b' + i).is(":hidden")) {
			$('#b' + i).remove();
		}
		if ($('#v' + i).is(":hidden")) {
			$('#v' + i).remove();
		}
	}
}
function h_hus(stenvalgt) {
	$('input[name=under]')[0].click();
	$('input[name=valg_af]')[0].click();
	if (stenvalgt == 0) {
		$('#valgaf').show();
	} else {
		$('#valgaf').hide();
	}
}
function maalsaet(tag, laengehustmp, valm, antalVinklerVinkel,
		antalTilbygninger, dynValmType, dynValmPlacering) {
	antalValgfrie = 0;
	antalValgfrieIaltLokal = 0;
	var valmId = [];
	for ( var i = 0; i <= 6; i++) {
		$('#framevalm' + i).hide();
		$('#framecapt' + i).hide();
	}
	// dynValmType = JSON.parse(dynValmType);
	// dynValmPlacering = JSON.parse(dynValmPlacering);

	laengehus = genererIllustration(tag, '', true, false, false, false,
			'maalsaet');
	maldanFelter(laengehus, "", valm, valmId);
	addVariable('valmId', JSON.stringify(valmId), 'maalsaet');
	ii = 0;
	for ( var i = 0; i <= 6; i++) {
		if ($('#framevalm' + i).is(":hidden")) {
			$('#framevalm' + i).remove();
			$('#framecapt' + i).remove();

		} else {
			if ($('#valmtop' + i).is(":hidden")) {
				$('#valmtop' + i).remove();
			}
			if ($('#valmvinkel' + i).is(":hidden")) {
				$('#valmvinkel' + i).remove();
			}
			// sets frame numbers
			$('#framenum' + i).text(ii);
			ii++;
		}
	}
}
function maldanFelter(medsendBygning, niveau, valm, valmId) {

	// ----------------Side 1----------------------
	if (medsendBygning.side1GavlTypeValgfri == true) {
		antalValgfrieIaltLokal = antalValgfrieIaltLokal + 1;
		if (medsendBygning.side1GavlType == valm) {
			antalValgfrie = antalValgfrie + 1;
			valmId[antalValgfrie] = antalValgfrieIaltLokal;

			// visrektangel
			$('#framevalm' + antalValgfrie).show();
			$('#framecapt' + antalValgfrie).show();
			// add black rectangles to show which valm to edit
			if (medsendBygning.side1GavlType == 2) {
				$('#framecapt' + antalValgfrie).append(' - Helvalm');
				$('#valmtop' + antalValgfrie).hide();
			}
			if (medsendBygning.side1GavlType == 3) {
				$('#framecapt' + antalValgfrie).append(' - Halvvalm');
			}
			// --visrektangel

		}
	} else {
		// Der kan kun være en tilbygning, hvis man ikke samtidig skal vælge
		// valm type
		if (medsendBygning.side1TilbygningAktiv == true) {
			tempNiveau = "1";
			maldanFelter(medsendBygning.side1Tilbygning.bygning, tempNiveau,
					valm, valmId);
		}
	}
	// ------------------------------------------------------
	if (medsendBygning.side2GavlTypeValgfri == true) {
		antalValgfrieIaltLokal = antalValgfrieIaltLokal + 1;
		if (medsendBygning.side2GavlType == valm) {
			antalValgfrie = antalValgfrie + 1;
			valmId[antalValgfrie] = antalValgfrieIaltLokal;
			// dynValmPlacering[antalValgfrie] = niveau + "2"
			// visrektangel
			$('#framevalm' + antalValgfrie).show();
			$('#framecapt' + antalValgfrie).show();
			// add black rectangles to show which valm to edit
			if (medsendBygning.side2GavlType == 2) {
				$('#framecapt' + antalValgfrie).append(' - Helvalm');
				$('#valmtop' + antalValgfrie).hide();
			}
			if (medsendBygning.side2GavlType == 3) {
				$('#framecapt' + antalValgfrie).append(' - Halvvalm');
			}
			// --visrektangel
		}
	} else {
		// Der skal tjekkes for tilbygning
		if (medsendBygning.side2TilbygningAktiv == true) {
			tempNiveau = "2";
			maldanFelter(medsendBygning.side2Tilbygning.bygning, tempNiveau,
					valm, valmId);
		}
	}

	// ------------------------------------------------------
	if (medsendBygning.side3GavlTypeValgfri == true) {
		antalValgfrieIaltLokal = antalValgfrieIaltLokal + 1;
		if (medsendBygning.side3GavlType == valm) {
			antalValgfrie = antalValgfrie + 1;
			valmId[antalValgfrie] = antalValgfrieIaltLokal;
			// dynValmPlacering[antalValgfrie] = niveau + "3"
			$('#framevalm' + antalValgfrie).show();
			$('#framecapt' + antalValgfrie).show();
			// add black rectangles to show which valm to edit
			if (medsendBygning.side3GavlType == 2) {
				$('#framecapt' + antalValgfrie).append(' - Helvalm');
				$('#valmtop' + antalValgfrie).hide();
			}
			if (medsendBygning.side3GavlType == 3) {
				$('#framecapt' + antalValgfrie).append(' - Halvvalm');
			}
		}
	} else {
		if (medsendBygning.side3TilbygningAktiv == true) {
			tempNiveau = "3";
			maldanFelter(medsendBygning.side3Tilbygning.bygning, tempNiveau,
					valm, valmId);
		}
	}

	// ------------------------------------------------------
	if (medsendBygning.side4GavlTypeValgfri == true) {
		antalValgfrieIaltLokal = antalValgfrieIaltLokal + 1;
		if (medsendBygning.side4GavlType == valm) {
			antalValgfrie = antalValgfrie + 1;
			valmId[antalValgfrie] = antalValgfrieIaltLokal;

			// dynValmPlacering[antalValgfrie] = niveau + "4"
			$('#framevalm' + antalValgfrie).show();
			$('#framecapt' + antalValgfrie).show();
			// add black rectangles to show which valm to edit
			if (medsendBygning.side4GavlType == 2) {
				$('#framecapt' + antalValgfrie).append(' - Helvalm');
				$('#valmtop' + antalValgfrie).hide();
			}
			if (medsendBygning.side4GavlType == 3) {
				$('#framecapt' + antalValgfrie).append(' - Halvvalm');
			}
		}
	} else {
		if (medsendBygning.side4TilbygningAktiv == true) {
			tempNiveau = "4";
			maldanFelter(medsendBygning.side4Tilbygning.bygning, tempNiveau,
					valm, valmId);
		}
	}

}
function kvistfrm(kvist, baseurl) {
	switch (kvist) {
	case 2:
		$('#kvistpic').attr('src', baseurl + '/maelsaetninger/gavlkvist.bmp');
		$('#listv_v').remove();
		$('#listv_b').remove();
		break;

	case 3:
		$('#kvistpic').attr('src', baseurl + '/maelsaetninger/kvisthelv.bmp');
		$('#listv_v').show();
		$('#listv_b').remove();
		break;
	case 4:
	case 5:
		$('#kvistpic').attr('src', baseurl + '/maelsaetninger/kvisthalv.bmp');
		$('#listv_v').show();
		$('#listv_b').show();
		break;
	}
}
function met2q(tagd_n, DekDobbv, dekn_ialt, lengde, ka, knast_met, locat,
		mang_dek, dekbredde, halvstendek, stenvalgt) {
	halve = false;
	var t1 = '';
	var fra_til = [];
	fra_til[2] = mang_dek;
	fra_til[3] = (mang_dek - dekbredde) + halvstendek;
	fra_til[4] = mang_dek - halvstendek;
	fra_til[5] = mang_dek - dekbredde;

	// Find first dem der kan reducere:
	iii_found = 0;
	iii_found_array = [];
	metq3_opt1_value = 0;
	for ( var iii = 2; iii <= 5; iii++) {
		if (iii !== 3) { // Mulighed 3 midlertidigt afskaffet
			if (fra_til[iii] > 0) {
				if ((iii == 3 || iii == 4)) {
					if ((stenvalgt == 0 || stenvalgt == 3 || stenvalgt == 4)) { // kun
						iii_found_array[iii_found + 1] = iii;
						iii_found = iii_found + 1;
					}
				} else {
					iii_found_array[iii_found + 1] = iii;
					iii_found = iii_found + 1;
				}
			}
		}
	}
	// debugged
	if (iii_found > 0) {
		iii_found_lowest = fra_til[iii_found_array[1]];
		iii_found_lowest_array = iii_found_array[1];
		for ( var iii = 1; iii <= iii_found; iii++) {

			if (fra_til[iii_found_array[iii]] < iii_found_lowest) {
				// NedenstĆ�ende var en klar programmerings fejl!
				// if ( iii_found_array[iii] < iii_found_lowest ){
				iii_found_lowest = fra_til[iii_found_array[iii]];
				iii_found_lowest_array = iii_found_array[iii];
			}
		}
		metq3_opt1_value = iii_found_lowest_array;
	}
	// HVis metq3_opt1_value = 0 stadig:
	// Der er ingen af mulighederne der tilbyder at reducere!
	// Hvad sĆ�?

	// Find dem der kan Å³ge:

	iii_found = 0;
	metq3_opt2_value = 0;
	for ( var iii = 2; iii <= 5; iii++) {
		if (iii !== 3) {
			if (fra_til[iii] < 0) {
				if ((iii == 3 || iii == 4)) {
					if ((stenvalgt == 0 || stenvalgt == 3 || stenvalgt == 4)) { // kun
						// halve
						// sten
						// ved
						// Dobbelt-S
						// og
						// Å²ko
						iii_found_array[iii_found + 1] = iii;
						iii_found = iii_found + 1;
					}
				} else {
					iii_found_array[iii_found + 1] = iii;
					iii_found = iii_found + 1;
				}
			}
		}
	}

	if (iii_found > 0) {
		iii_found_biggest = fra_til[iii_found_array[1]];
		iii_found_biggest_array = iii_found_array[1];
		for ( var iii = 1; iii <= iii_found; iii++) {
			if (fra_til[iii_found_array[iii]] > iii_found_biggest) {
				iii_found_biggest = fra_til[iii_found_array[iii]];
				iii_found_biggest_array = iii_found_array[iii];
			}
		}
		metq3_opt2_value = iii_found_biggest_array;
	}

	// Vi gĆ�r med livrem og seler, sĆ� vi tjekker lige at alt er initialiseret,
	// inden vi indsetter noget pĆ� skermen!

	// vaelg_igen:
	if (metq3_opt2_value == 0) {
		// Der er ikke nogen der kan forÅ³ge
		metq3_opt2_value = metq3_opt1_value + 1;
		if (metq3_opt2_value > 5) {
			metq3_opt2_value = 2;
		}
		// goto vaelg_igen;
	} else {
		if (metq3_opt1_value == 0) {
			if (iii_found > 1) {
				for ( var iii = 1; iii <= iii_found; iii++) {
					if (metq3_opt2_value !== iii_found_array[iii]) {
						metq3_opt1_value = iii_found_array[iii];
					}
				}
			} else {
				metq3_opt1_value = metq3_opt2_value - 1;
				if (metq3_opt1_value < 2) {
					metq3_opt1_value = 5;
				}
			}
		}
	}

	if ((stenvalgt == 1 || stenvalgt == 2)) { // kun halve sten ved
		// Dobbelt-S
		// og Å²ko
		metq3_opt1_value = 2;
		metq3_opt2_value = 5;
	}
	if (metq3_opt1_value == 3) {
		switch (metq3_opt2_value) {
		case 2:
			metq3_opt1_value = 4;
		case 3:
			metq3_opt1_value = 2;
		case 4:
			metq3_opt1_value = 2;
		case 5:
			metq3_opt1_value = 4;
		}
		// select case

	}

	if (metq3_opt2_value == 3) {
		switch (metq3_opt1_value) {
		case 2:
			metq3_opt2_value = 4;
		case 3:
			metq3_opt2_value = 2;
		case 4:
			metq3_opt2_value = 2;
		case 5:
			metq3_opt2_value = 4;
		}
		// select case

	}
	// Nu skulle met3_opt1_value og met3_opt2_value gerne have en verdi.
	// SĆ� indsetter vi den rigtige tekst pĆ� skermen!

	// Option1(0):
	if (metq3_opt1_value == 2) {
		// fra_til[2] < 0 - Å²ge tagets lengde: - LoadResString(237)
		// fra_til[2] > 0 - Reducer tagets lengde: - LoadResString(228)
		if (fra_til[2] < 0) {
			t1 = String(((fra_til[2] * -1) / 10));
			$('#0').text('Øge tagets længde');
		} else {
			t1 = String(((fra_til[2]) / 10));
			$('#0').text('Reducer tagets længde:');
		}
	}

	if (metq3_opt1_value == 3) {
		// fra_til[3] < 0 - Fjern en hel sten, tilfÅ³j en halv sten og Å³ge
		// tagets
		// lengde: - 387
		// fra_til[3] > 0 - Fjern en hel sten, tilfÅ³j en halv sten og reducer
		// tagets lengde: - 388
		if (fra_til[3] < 0) {
			t1 = String(((fra_til[3] * -1) / 10));
			$('#0')
					.text(
							'Fjern en hel sten, tilføj en halv sten og øge tagets længde:');
		} else {
			t1 = String(((fra_til[3]) / 10));
			$('#0')
					.text(
							'Fjern en hel sten, tilføj en halv sten og reducer tagets længde:');
		}
	}

	if (metq3_opt1_value == 4) {
		// fra_til[4] < 0 - TilfÅ³j en halv sten og Å³ge tagets lengde: -
		// LoadResString(229)
		// fra_til[4] > 0 - TilfÅ³j en halv sten og reducer tagets lengde: -
		// 389
		if (fra_til[4] < 0) {
			t1 = String(((fra_til[4] * -1) / 10));
			$('#0').text('Tilføj en halv sten og øge tagets længde:');
		} else {
			t1 = String(((fra_til[4]) / 10));
			$('#0').text('Tilføj en halv sten og reducer tagets længde:');
		}
	}

	if (metq3_opt1_value == 5) {
		// fra_til[5] < 0 - TilfÅ³j en hel sten og Å³ge tagets lengde: -
		// LoadResString(234)
		// fra_til[5] > 0 - TilfÅ³j en hel sten og reducer tagets lengde: - 390
		if (fra_til[5] < 0) {
			t1 = String(((fra_til[5] * -1) / 10));
			$('#0').text('Tilføje en hel sten og øge tagets længde:');
		} else {
			t1 = String(((fra_til[5]) / 10));
			$('#0').text('Tilføj en hel sten og reducer tagets længde:');
		}
	}
	if (t1.slice(-2).slice(0, 1) !== ".") {
		t1 = t1 + ".0";
	}
	$('#text7').text(t1 + ' cm');

	// Option1(1):
	if (metq3_opt2_value == 2) {
		// fra_til[2] < 0 - Å²ge tagets lengde: - LoadResString(237)
		// fra_til[2] > 0 - Reducer tagets lengde: - LoadResString(228)
		if (fra_til[2] < 0) {
			t2 = String(((fra_til[2] * -1) / 10));
			$('#1').text('Øge tagets længde');
		} else {
			t2 = String(((fra_til[2]) / 10));
			$('#1').text('Reducer tagets længde:');
		}
	}

	if (metq3_opt2_value == 3) {
		// fra_til[3] < 0 - Fjern en hel sten, tilfÅ³j en halv sten og Å³ge
		// tagets
		// lengde: - 387
		// fra_til[3] > 0 - Fjern en hel sten, tilfÅ³j en halv sten og reducer
		// tagets lengde: - 388
		if (fra_til[3] < 0) {
			t2 = String(((fra_til[3] * -1) / 10));
			$('#1')
					.text(
							'Fjern en hel sten, tilføj en halv sten og øge tagets længde:');
		} else {
			t2 = String(((fra_til[3]) / 10));
			$('#1')
					.text(
							'Fjern en hel sten, tilføj en halv sten og reducer tagets længde:');
		}
	}

	if (metq3_opt2_value == 4) {
		// fra_til[4] < 0 - TilfÅ³j en halv sten og Å³ge tagets lengde: -
		// LoadResString(229)
		// fra_til[4] > 0 - TilfÅ³j en halv sten og reducer tagets lengde: -
		// 389
		if (fra_til[4] < 0) {
			t2 = String(((fra_til[4] * -1) / 10));
			$('#1').text('Tilføj en halv sten og øge tagets længde:');
		} else {
			t2 = String(((fra_til[4]) / 10));
			$('#1').text('Tilføj en halv sten og reducer tagets længde:');
		}
	}

	if (metq3_opt2_value == 5) {
		// fra_til[5] < 0 - TilfÅ³j en hel sten og Å³ge tagets lengde: -
		// LoadResString(234)
		// fra_til[5] > 0 - TilfÅ³j en hel sten og reducer tagets lengde: - 390
		if (fra_til[5] < 0) {
			t2 = String(((fra_til[5] * -1) / 10));
			$('#1').text('Tilføje en hel sten og øge tagets længde:');
		} else {
			t2 = String(((fra_til[5]) / 10));
			$('#1').text('Tilføj en hel sten og reducer tagets længde:');
		}
	}
	if (t1.slice(-2).slice(0, 1) !== ".") {
		t1 = t1 + ".0";
	}
	$('#text7').text(t1 + ' cm');

	// Option1(1):
	if (metq3_opt2_value == 2) {
		// fra_til(2) < 0 - Å²ge tagets lengde: - LoadResString(237)
		// fra_til(2) > 0 - Reducer tagets lengde: - LoadResString(228)
		if (fra_til[2] < 0) {
			t2 = String(((fra_til[2] * -1) / 10));
			$('#1').text('Øge tagets længde');
		} else {
			t2 = String(((fra_til[2]) / 10));
			$('#1').text('Reducer tagets længde:');
		}
	}

	if (metq3_opt2_value == 3) {
		// fra_til(3) < 0 - Fjern en hel sten, tilfÅ³j en halv sten og Å³ge
		// tagets lengde: - 387
		// fra_til(3) > 0 - Fjern en hel sten, tilfÅ³j en halv sten og reducer
		// tagets lengde: - 388
		if (fra_til[3] < 0) {
			t2 = String(((fra_til[3] * -1) / 10));
			$('#1')
					.text(
							'Fjern en hel sten, tilføj en halv sten og øge tagets længde:');
		} else {
			t2 = String(((fra_til[3]) / 10));
			$('#1')
					.text(
							'Fjern en hel sten, tilføj en halv sten og reducer tagets længde:');
		}
	}

	if (metq3_opt2_value == 4) {
		// fra_til(4) < 0 - TilfÅ³j en halv sten og Å³ge tagets lengde: -
		// LoadResString(229)
		// fra_til(4) > 0 - TilfÅ³j en halv sten og reducer tagets lengde: - 389
		if (fra_til[4] < 0) {
			t2 = String(((fra_til[4] * -1) / 10));
			$('#1').text('Tilføj en halv sten og øge tagets længde:');
		} else {
			t2 = String(((fra_til[4]) / 10));
			$('#1').text('Tilføj en halv sten og reducer tagets længde:');
		}
	}

	if (metq3_opt2_value == 5) {
		// fra_til(5) < 0 - TilfÅ³j en hel sten og Å³ge tagets lengde: -
		// LoadResString(234)
		// fra_til(5) > 0 - TilfÅ³j en hel sten og reducer tagets lengde: - 390
		if (fra_til[5] < 0) {
			t2 = String(((fra_til[5] * -1) / 10));
			$('#1').text('Tilføje en hel sten og øge tagets længde:');
		} else {
			t2 = String(((fra_til[5]) / 10));
			$('#1').text('Tilføj en hel sten og reducer tagets længde:');
		}
	}
	if (t2.slice(-2).slice(0, 1) !== ".") {
		t2 = t2 + ".0";
	}
	$('#text8').text(t2 + ' cm');

	met3qsvar = 0;
	if (knast_met == true) {
		$('#label8').text('Knastens længde:');
	} // "Knastens
	// lengde:
	$('#text1').text(ka + ' stk.');

	if (String(tagd_n / 10).slice(-2).slice(0, 1) == ".") {
		$('#text2').text(tagd_n / 10) + ' cm';
	} else {
		$('#text2').text((tagd_n / 10) + ",0" + ' cm');
	}
	if (String(DekDobbv / 10).slice(-2).slice(0, 1) == ".") {
		$('#text3').text(DekDobbv / 10 + ' cm');
	} else {
		$('#text3').text((DekDobbv / 10) + ",0" + ' cm');
	}

	if (String(dekn_ialt / 10).slice(-2).slice(0, 1) == ".") {
		$('#text4').text(dekn_ialt / 10 + ' cm');
	} else {
		$('#text4').text((dekn_ialt / 10) + ",0" + ' cm');
	}

	if (String(lengde / 10).slice(-2).slice(0, 1) == ".") {
		$('#text5').text(lengde / 10 + ' cm');
	} else {
		$('#text5').text((lengde / 10) + ",0" + ' cm');
	}
	if (mang_dek > 0) {
		if (String(mang_dek / 10).slice(-2).slice(0, 1) == ".") {
			$('#text6').text(mang_dek / 10 + ' cm');
		} else {
			$('#text6').text((mang_dek / 10) + ".0" + ' cm');
		}
	} else {
		if (String((mang_dek * -1) / 10).slice(-2).slice(0, 1) == ".") {
			$('#text6').text((mang_dek * -1) / 10 + ' cm');
		} else {
			$('#text6').text(((mang_dek * -1) / 10) + ".0" + ' cm');
		}
	}
	addVariable('fra_til', JSON.stringify(fra_til), 'met2q');
	addVariable('metq3_opt1_value', metq3_opt1_value, 'met2q');
	addVariable('metq3_opt2_value', metq3_opt2_value, 'met2q');
}
function met3q(tagd_n, DekVindskH, DekVindskV, dekn_ialt, lengde, ka,
		knast_met, locat, mang_dek, dekbredde, halvstendek, stenvalgt) {
	var t1 = '';
	var fra_til = [];
	fra_til[2] = mang_dek;
	fra_til[3] = (mang_dek - dekbredde) + halvstendek;
	fra_til[4] = mang_dek - halvstendek;
	fra_til[5] = mang_dek - dekbredde;

	// Find first dem der kan reducere:
	iii_found = 0;
	iii_found_array = [];
	metq3_opt1_value = 0;
	for ( var iii = 2; iii <= 5; iii++) {
		if (iii !== 3) { // Mulighed 3 midlertidigt afskaffet
			if (fra_til[iii] > 0) {
				if ((iii == 3 || iii == 4)) {
					if ((stenvalgt == 0 || stenvalgt == 3 || stenvalgt == 4)) { // kun
						iii_found_array[iii_found + 1] = iii;
						iii_found = iii_found + 1;
					}
				} else {
					iii_found_array[iii_found + 1] = iii;
					iii_found = iii_found + 1;
				}
			}
		}
	}
	// debugged
	if (iii_found > 0) {
		iii_found_lowest = fra_til[iii_found_array[1]];
		iii_found_lowest_array = iii_found_array[1];
		for ( var iii = 1; iii <= iii_found; iii++) {

			if (fra_til[iii_found_array[iii]] < iii_found_lowest) {
				// NedenstĆ�ende var en klar programmerings fejl!
				// if ( iii_found_array[iii] < iii_found_lowest ){
				iii_found_lowest = fra_til[iii_found_array[iii]];
				iii_found_lowest_array = iii_found_array[iii];
			}
		}
		metq3_opt1_value = iii_found_lowest_array;
	}
	// HVis metq3_opt1_value = 0 stadig:
	// Der er ingen af mulighederne der tilbyder at reducere!
	// Hvad sĆ�?

	// Find dem der kan Å³ge:

	iii_found = 0;
	metq3_opt2_value = 0;
	for ( var iii = 2; iii <= 5; iii++) {
		if (iii !== 3) {
			if (fra_til[iii] < 0) {
				if ((iii == 3 || iii == 4)) {
					if ((stenvalgt == 0 || stenvalgt == 3 || stenvalgt == 4)) { // kun
						// halve
						// sten
						// ved
						// Dobbelt-S
						// og
						// Å²ko
						iii_found_array[iii_found + 1] = iii;
						iii_found = iii_found + 1;
					}
				} else {
					iii_found_array[iii_found + 1] = iii;
					iii_found = iii_found + 1;
				}
			}
		}
	}

	if (iii_found > 0) {
		iii_found_biggest = fra_til[iii_found_array[1]];
		iii_found_biggest_array = iii_found_array[1];
		for ( var iii = 1; iii <= iii_found; iii++) {
			if (fra_til[iii_found_array[iii]] < iii_found_biggest) {
				iii_found_biggest = fra_til[iii_found_array[iii]];
				iii_found_biggest_array = iii_found_array[iii];
			}
		}
		metq3_opt2_value = iii_found_biggest_array;
	}

	// Vi gĆ�r med livrem og seler, sĆ� vi tjekker lige at alt er initialiseret,
	// inden vi indsetter noget pĆ� skermen!

	while (metq3_opt2_value == 0) {
		// Der er ikke nogen der kan forÅ³ge
		metq3_opt2_value = metq3_opt1_value + 1;
		if (metq3_opt2_value > 5) {
			metq3_opt2_value = 2;
		}
	}
	if (metq3_opt1_value == 0) {
		if (iii_found > 1) {
			for ( var iii = 1; iii <= iii_found; iii++) {
				if (metq3_opt2_value !== iii_found_array[iii]) {
					metq3_opt1_value = iii_found_array[iii];
				}
			}
		} else {
			metq3_opt1_value = metq3_opt2_value - 1;
			if (metq3_opt1_value < 2) {
				metq3_opt1_value = 5;
			}
		}
	}

	if ((stenvalgt == 1 || stenvalgt == 2)) { // kun halve sten ved
		// Dobbelt-S
		// og Å²ko
		metq3_opt1_value = 2;
		metq3_opt2_value = 5;
	}
	if (metq3_opt1_value == 3) {
		switch (metq3_opt2_value) {
		case 2:
			metq3_opt1_value = 4;
		case 3:
			metq3_opt1_value = 2;
		case 4:
			metq3_opt1_value = 2;
		case 5:
			metq3_opt1_value = 4;
		}
		// select case

	}

	if (metq3_opt2_value == 3) {
		switch (metq3_opt1_value) {
		case 2:
			metq3_opt2_value = 4;
		case 3:
			metq3_opt2_value = 2;
		case 4:
			metq3_opt2_value = 2;
		case 5:
			metq3_opt2_value = 4;
		}
		// select case

	}
	// Nu skulle met3_opt1_value og met3_opt2_value gerne have en verdi.
	// SĆ� indsetter vi den rigtige tekst pĆ� skermen!

	// Option1(0):
	if (metq3_opt1_value == 2) {
		// fra_til[2] < 0 - Å²ge tagets lengde: - LoadResString(237)
		// fra_til[2] > 0 - Reducer tagets lengde: - LoadResString(228)
		if (fra_til[2] < 0) {
			t1 = String(((fra_til[2] * -1) / 10));
			$('#0').text('Øge tagets længde');
		} else {
			t1 = String(((fra_til[2]) / 10));
			$('#0').text('Reducer tagets længde:');
		}
	}

	if (metq3_opt1_value == 3) {
		// fra_til[3] < 0 - Fjern en hel sten, tilfÅ³j en halv sten og Å³ge
		// tagets
		// lengde: - 387
		// fra_til[3] > 0 - Fjern en hel sten, tilfÅ³j en halv sten og reducer
		// tagets lengde: - 388
		if (fra_til[3] < 0) {
			t1 = String(((fra_til[3] * -1) / 10));
			$('#0')
					.text(
							'Fjern en hel sten, tilføj en halv sten og øge tagets længde:');
		} else {
			t1 = String(((fra_til[3]) / 10));
			$('#0')
					.text(
							'Fjern en hel sten, tilføj en halv sten og reducer tagets længde:');
		}
	}

	if (metq3_opt1_value == 4) {
		// fra_til[4] < 0 - TilfÅ³j en halv sten og Å³ge tagets lengde: -
		// LoadResString(229)
		// fra_til[4] > 0 - TilfÅ³j en halv sten og reducer tagets lengde: -
		// 389
		if (fra_til[4] < 0) {
			t1 = String(((fra_til[4] * -1) / 10));
			$('#0').text('Tilføj en halv sten og øge tagets længde:');
		} else {
			t1 = String(((fra_til[4]) / 10));
			$('#0').text('Tilføj en halv sten og reducer tagets længde:');
		}
	}

	if (metq3_opt1_value == 5) {
		// fra_til[5] < 0 - TilfÅ³j en hel sten og Å³ge tagets lengde: -
		// LoadResString(234)
		// fra_til[5] > 0 - TilfÅ³j en hel sten og reducer tagets lengde: - 390
		if (fra_til[5] < 0) {
			t1 = String(((fra_til[5] * -1) / 10));
			$('#0').text('Tilføje en hel sten og øge tagets længde:');
		} else {
			t1 = String(((fra_til[5]) / 10));
			$('#0').text('Tilføj en hel sten og reducer tagets længde:');
		}
	}
	if (t1.slice(-2).slice(0, 1) !== ".") {
		t1 = t1 + ".0";
	}
	$('#text7').text(t1 + ' cm');

	// Option1(1):
	if (metq3_opt2_value == 2) {
		// fra_til[2] < 0 - Å²ge tagets lengde: - LoadResString(237)
		// fra_til[2] > 0 - Reducer tagets lengde: - LoadResString(228)
		if (fra_til[2] < 0) {
			t2 = String(((fra_til[2] * -1) / 10));
			$('#1').text('Øge tagets længde');
		} else {
			t2 = String(((fra_til[2]) / 10));
			$('#1').text('Reducer tagets længde:');
		}
	}

	if (metq3_opt2_value == 3) {
		// fra_til[3] < 0 - Fjern en hel sten, tilfÅ³j en halv sten og Å³ge
		// tagets
		// lengde: - 387
		// fra_til[3] > 0 - Fjern en hel sten, tilfÅ³j en halv sten og reducer
		// tagets lengde: - 388
		if (fra_til[3] < 0) {
			t2 = String(((fra_til[3] * -1) / 10));
			$('#1')
					.text(
							'Fjern en hel sten, tilføj en halv sten og øge tagets længde:');
		} else {
			t2 = String(((fra_til[3]) / 10));
			$('#1')
					.text(
							'Fjern en hel sten, tilføj en halv sten og reducer tagets længde:');
		}
	}

	if (metq3_opt2_value == 4) {
		// fra_til[4] < 0 - TilfÅ³j en halv sten og Å³ge tagets lengde: -
		// LoadResString(229)
		// fra_til[4] > 0 - TilfÅ³j en halv sten og reducer tagets lengde: -
		// 389
		if (fra_til[4] < 0) {
			t2 = String(((fra_til[4] * -1) / 10));
			$('#1').text('Tilføj en halv sten og øge tagets længde:');
		} else {
			t2 = String(((fra_til[4]) / 10));
			$('#1').text('Tilføj en halv sten og reducer tagets længde:');
		}
	}

	if (metq3_opt2_value == 5) {
		// fra_til[5] < 0 - TilfÅ³j en hel sten og Å³ge tagets lengde: -
		// LoadResString(234)
		// fra_til[5] > 0 - TilfÅ³j en hel sten og reducer tagets lengde: - 390
		if (fra_til[5] < 0) {
			t2 = String(((fra_til[5] * -1) / 10));
			$('#1').text('Tilføje en hel sten og øge tagets længde:');
		} else {
			t2 = String(((fra_til[5]) / 10));
			$('#1').text('Tilføj en hel sten og reducer tagets længde:');
		}
	}
	if (t1.slice(-2).slice(0, 1) !== ".") {
		t1 = t1 + ".0";
	}
	$('#text7').text(t1 + ' cm');

	// Option1(1):
	if (metq3_opt2_value == 2) {
		// fra_til(2) < 0 - Å²ge tagets lengde: - LoadResString(237)
		// fra_til(2) > 0 - Reducer tagets lengde: - LoadResString(228)
		if (fra_til[2] < 0) {
			t2 = String(((fra_til[2] * -1) / 10));
			$('#1').text('Øge tagets længde');
		} else {
			t2 = String(((fra_til[2]) / 10));
			$('#1').text('Reducer tagets længde:');
		}
	}

	if (metq3_opt2_value == 3) {
		// fra_til(3) < 0 - Fjern en hel sten, tilfÅ³j en halv sten og Å³ge
		// tagets lengde: - 387
		// fra_til(3) > 0 - Fjern en hel sten, tilfÅ³j en halv sten og reducer
		// tagets lengde: - 388
		if (fra_til[3] < 0) {
			t2 = String(((fra_til[3] * -1) / 10));
			$('#1')
					.text(
							'Fjern en hel sten, tilføj en halv sten og øge tagets længde:');
		} else {
			t2 = String(((fra_til[3]) / 10));
			$('#1')
					.text(
							'Fjern en hel sten, tilføj en halv sten og reducer tagets længde:');
		}
	}

	if (metq3_opt2_value == 4) {
		// fra_til(4) < 0 - TilfÅ³j en halv sten og Å³ge tagets lengde: -
		// LoadResString(229)
		// fra_til(4) > 0 - TilfÅ³j en halv sten og reducer tagets lengde: - 389
		if (fra_til[4] < 0) {
			t2 = String(((fra_til[4] * -1) / 10));
			$('#1').text('Tilføj en halv sten og øge tagets længde:');
		} else {
			t2 = String(((fra_til[4]) / 10));
			$('#1').text('Tilføj en halv sten og reducer tagets længde:');
		}
	}

	if (metq3_opt2_value == 5) {
		// fra_til(5) < 0 - TilfÅ³j en hel sten og Å³ge tagets lengde: -
		// LoadResString(234)
		// fra_til(5) > 0 - TilfÅ³j en hel sten og reducer tagets lengde: - 390
		if (fra_til[5] < 0) {
			t2 = String(((fra_til[5] * -1) / 10));
			$('#1').text('Tilføje en hel sten og øge tagets længde:');
		} else {
			t2 = String(((fra_til[5]) / 10));
			$('#1').text('Tilføj en hel sten og reducer tagets længde:');
		}
	}
	if (t2.slice(-2).slice(0, 1) !== ".") {
		t2 = t2 + ".0";
	}
	$('#text8').text(t2 + ' cm');
	halve = false;
	met3qsvar = 0;
	if (knast_met == true) {
		$('#label8').text('Knastens længde:');
	} // "Knastens
	// lengde:"
	$('#text1').text(ka + ' stk.');

	if (String(tagd_n / 10).slice(-2).slice(0, 1) == ".") {
		$('#text9').text(tagd_n / 10) + ' cm';
	} else {
		$('#text9').text((tagd_n / 10) + ",0" + ' cm');
	}
	// ///
	if (String(DekVindskH / 10).slice(-2).slice(0, 1) == ".") {
		$('#text3').text(DekVindskH / 10 + ' cm');
	} else {
		$('#text3').text((DekVindskH / 10) + ",0" + ' cm');
	}
	if (String(DekVindskV / 10).slice(-2).slice(0, 1) == ".") {
		$('#text2').text(DekVindskV / 10 + ' cm');
	} else {
		$('#text2').text((DekVindskV / 10) + ",0" + ' cm');
	}
	// //
	if (String(dekn_ialt / 10).slice(-2).slice(0, 1) == ".") {
		$('#text4').text(dekn_ialt / 10 + ' cm');
	} else {
		$('#text4').text((dekn_ialt / 10) + ",0" + ' cm');
	}

	if (String(lengde / 10).slice(-2).slice(0, 1) == ".") {
		$('#text5').text(lengde / 10 + ' cm');
	} else {
		$('#text5').text((lengde / 10) + ",0" + ' cm');
	}
	if (mang_dek > 0) {
		if (String(mang_dek / 10).slice(-2).slice(0, 1) == ".") {
			$('#text6').text(mang_dek / 10 + ' cm');
		} else {
			$('#text6').text((mang_dek / 10) + ".0" + ' cm');
		}
	} else {
		if (String((mang_dek * -1) / 10).slice(-2).slice(0, 1) == ".") {
			$('#text6').text((mang_dek * -1) / 10 + ' cm');
		} else {
			$('#text6').text(((mang_dek * -1) / 10) + ".0" + ' cm');
		}
	}
	addVariable('fra_til', JSON.stringify(fra_til), 'met3q');
	addVariable('metq3_opt1_value', metq3_opt1_value, 'met3q');
	addVariable('metq3_opt2_value', metq3_opt2_value, 'met3q');
}
