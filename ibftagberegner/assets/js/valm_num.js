function load_valm_num(hoved,antalVinkler,antaltilbygninger) {
	if (hoved==1) {
		if (antaltilbygninger==0) {
			$("#valm_num1").show();
			$("#valm_num1").css({top: 25, left: 10});
			$("#valm_num2").show();
			$("#valm_num2").css({top: 25, left: 160});

			$("#valm_num3d1").show();
			$("#valm_num3d1").css({top: 180, left: 10});
			$("#valm_num3d2").show();
			$("#valm_num3d2").css({top: 160, left: 160});
		}
		if (antaltilbygninger==1) {
			$("#valm_num1").show();
			$("#valm_num1").css({top: 25, left: 10});
			$("#valm_num2").show();
			$("#valm_num2").css({top: 25, left: 160});
			$("#valm_num3").show();
			$("#valm_num3").css({top: 110, left: 100});

			$("#valm_num3d1").show();
			$("#valm_num3d1").css({top: 180, left: 10});
			$("#valm_num3d2").show();
			$("#valm_num3d2").css({top: 140, left: 150});
			$("#valm_num3d3").show();
			$("#valm_num3d3").css({top: 210, left: 150});
		}
		if (antaltilbygninger==2) {
			$("#valm_num1").show();
			$("#valm_num1").css({top: -15, left: 100});
			$("#valm_num2").show();
			$("#valm_num2").css({top: 80, left: 15});
			$("#valm_num3").show();
			$("#valm_num3").css({top: 45, left: 190});
			$("#valm_num4").show();
			$("#valm_num4").css({top: 130, left: 100});

			$("#valm_num3d1").show();
			$("#valm_num3d1").css({top: 160, left: 40});
			$("#valm_num3d2").show();
			$("#valm_num3d2").css({top: 210, left: 20});
			$("#valm_num3d3").show();
			$("#valm_num3d3").css({top: 140, left: 180});
			$("#valm_num3d4").show();
			$("#valm_num3d4").css({top: 190, left: 190});
		}
	}
	if (hoved==2) {
		if (antalVinkler==1) {
			$("#valm_num1").show();
			$("#valm_num1").css({top: 25, left: 10});
			$("#valm_num2").show();
			$("#valm_num2").css({top: 25, left: 150});
			$("#valm_num3").show();
			$("#valm_num3").css({top: 110, left: 150});

			$("#valm_num3d1").show();
			$("#valm_num3d1").css({top: 180, left: 15});
			$("#valm_num3d2").show();
			$("#valm_num3d2").css({top: 190, left: 130});
			$("#valm_num3d3").show();
			$("#valm_num3d3").css({top: 170, left: 210});
		}
		if (antalVinkler==2) {
			$("#valm_num1").show();
			$("#valm_num1").css({top: 10, left: 50});
			$("#valm_num2").show();
			$("#valm_num2").css({top: 100, left: 0});
			$("#valm_num3").show();
			$("#valm_num3").css({top: 10, left: 190});
			$("#valm_num4").show();
			$("#valm_num4").css({top: 100, left: 140});

			$("#valm_num3d1").show();
			$("#valm_num3d1").css({top: 170, left: 20});
			$("#valm_num3d2").show();
			$("#valm_num3d2").css({top: 210, left: 20});
			$("#valm_num3d3").show();
			$("#valm_num3d3").css({top: 160, left: 170});
			$("#valm_num3d4").show();
			$("#valm_num3d4").css({top: 200, left: 170});
		}
	}
	
	if (hoved==5) {
		if (antalVinkler==1) {
			$("#valm_num1").show();
			$("#valm_num1").css({top: 60, left: 20});
			$("#valm_num2").show();
			$("#valm_num2").css({top: 30, left: 200});

			$("#valm_num3d1").show();
			$("#valm_num3d1").css({top: 140, left: 100});
			$("#valm_num3d2").show();
			$("#valm_num3d2").css({top: 140, left: 200});
		}
		if (antalVinkler==2) {
			$("#valm_num2").show();
			$("#valm_num2").css({top: 160, left: 50});
			$("#valm_num1").show();
			$("#valm_num1").css({top: 30, left: 200});

			$("#valm_num3d1").show();
			$("#valm_num3d1").css({top: 150, left: 210});
			$("#valm_num3d2").show();
			$("#valm_num3d2").css({top: 90, left: 210});
		}
	}
	if (hoved==7) {
		$("#valm_num1").show();
		$("#valm_num2").show();
		$("#valm_num2").css({left: 120});
		$("#valm_num3").show();
		$("#valm_num3").css({top: 120});
		$("#valm_num4").show();
		$("#valm_num4").css({top: 120, left:120});

		$("#valm_num3d1").show();
		$("#valm_num3d1").css({top: 160, left: 20});
		$("#valm_num3d2").show();
		$("#valm_num3d2").css({top: 140, left: 120});
		$("#valm_num3d3").show();
		$("#valm_num3d3").css({top: 210, left: 80});
		$("#valm_num3d4").show();
		$("#valm_num3d4").css({top: 160, left: 170});
	}
}