<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Admin extends CI_Controller { 
	public function varer() {
		$query = $this->db->get ( 'varer' );
		$data ['res'] = $query->result ();
		 
		$this->load->view ( 'admin/admin_header' );
		$this->load->view ( 'admin/admin_home', $data );
	}
	public function vare() {
		$query = $this->db->get ( 'vare' );
		$data ['res'] = $query->result ();
		
		$this->load->view ( 'admin/admin_header' );
		$this->load->view ( 'admin/admin_home', $data );
	}
	public function __construct(){
	parent::__construct();
// 	if (!$this->tank_auth->is_logged_in ()) { // logged in
// 	redirect ( 'auth/login' );
// 	}
	}
	
	public function index()  {
// 	if ($this->tank_auth->is_logged_in ()) { // logged in
		$this->addFarve();
// 	} else {
// 	redirect ( 'auth/login' );
// 	}
	}
	public function addFarve() {
		$data ['farve'] = false;
		$this->load->view ( 'admin/admin_header' );
		$this->load->view ( 'admin/add_farve', $data );
	}
	public function addFarveTbl() {
		$data ['farve'] = $this->input->post ( 'newfarve' );
		$data ['type'] = $this->input->post ( 'type' );
		$this->db->select ( 'IBF, Tun, Navn,Enh,Standart,Tilbehor,vu,ut,Rkf,Rt,varetype,right(varetype,length(varetype)-2) as type' );
		$query = $this->db->get_where ( 'vare', array (
				'farveid' => '2',
				$data ['type'] => '1'  
		) );
		$data ['res'] = $query->result ();
		$data ['farve'] = $this->input->post ( 'newfarve' );
		
		//gather replacement data
		$query = $this->db->query('SELECT *,right(varetype,length(varetype)-2) as type FROM `vare` WHERE left(varetype,1) = 2 group by Tun order by ABS(type)');
		$data ['replacedata'] = $query->result ();
		
		//---
		
		$this->load->view ( 'admin/admin_header' );
		$this->load->view ( 'admin/add_farve_tbl', $data );
	}
	public function get_varer() {
		$this->output->enable_profiler ( TRUE );
		foreach ( $_POST as $key => $value ) {
			echo $key . ' = ' . $value . '<br>';
			$this->db->where ( 'IBF', $key );
			$this->db->update ( 'varer', array (
					'varetype' => $value 
			) );
			;
		}
	}
}