<script>
$( document ).ready(function() {
	retning = (<?php echo json_encode($retning)?>);
	load_farvevalg(retning);
	});
</script>
<?php

$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'class' => 'form-control next',
		'type' => 'submit',
		'content' => 'Frem ->' 
);
$back = "'" . base_url ( 'home/kunde' ) . "/" . $stenvalgtnr . "'";
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $back,
		'content' => '<-- Tilbage' 
);
?>

<div class="container">
	<div class=row>
		<h3 class="col-md-5">Vælg tagets farve</h3>
	</div>
	<div class=row>
		<div class=col-md-3>
			<?php
			// ///// FORM ///////

			echo form_open ( 'getdata/farvevalg', array (
					'id' => 'farvevalg' 
			) );

			// prints list of radio buttons
			$i=0;
			foreach ( $colors as $obj ) {
				$radio = array (
						'name' => 'color',
						'id' => 'r'.$obj->id,
						'value' => $obj->id,
						'onclick' => "show_image2('$obj->id')" 
				);
				if ($i==0)
					$radio['checked']=1;
				$i++;
				echo '<br>' . form_radio ( $radio );
				echo '<label for="r'.$obj->id.'"></label>';
				$colorname = str_replace ( "ų", "ø", $obj->name );
				?><label class=radiotext><?php echo $colorname?></label><?php
			}
			?>
		<!-- /col-md-6 -->
		</div>
		<div class="col-md-4">
			<?php foreach ( $colors as $obj ) { ?>
	
					<img alt="" class=roof_type_img src="<?php echo base_url('/assets/img/'.$obj->image)?>" id="roof_type_img<?php echo $obj->id?>" width=100>

			<?php } ?>
		</div>
	</div>


	<!-- /col-md-6 -->
	<div class=row style="margin-bottom:200px">
		<div class="col-md-7">
			<div style="float:right">
			<img src="<?php echo base_url('assets')?>/img/left.png" onclick="window.location.href=<?php echo $back?>" style="cursor:pointer;">
			<img src="<?php echo base_url('assets')?>/img/right.png" onclick = 'farvevalg.submit();' style="cursor:pointer;">
			</div>
		</div>
	<div>   
</div>
<!-- /row -->

<?php
echo form_close ();
// // end of form///
?>