<style>
#per {
	margin-top: 40px;
}

#imgcont3d {
	margin-top: 50px;
	margin-bottom: 100px;
}
</style>
<script>
$( document ).ready(function() {
	var stenvalgt = <?php echo '"'.$stenvalgt.'"'; ?>;
	antalvinkler = <?php echo '"'.$antalvinkler.'"'; ?>;
 	vinkel45(stenvalgt,antalvinkler);
 	dynValmType = <?php echo $dynValmType; ?>;
	baseurl = <?php echo '"'.base_url('assets/illustration').'"'; ?>;
	antalVinklerVinkel = <?php echo '"'.$antalvinklervinkel.'"'; ?>;
	antalVinkler = <?php echo '"'.$antalvinkler.'"'; ?>;
	antalTilbygninger = <?php echo '"'.$antaltilbygninger.'"'; ?>;
	antalKnaster = <?php echo '"'.$antalknaster.'"'; ?>;
	tag = <?php echo '"'.$tag.'"'; ?>;
	drawillustration('vinkel_45');

	retning = (<?php echo json_encode($retning)?>);
	load_vinkel_45(retning);
});
</script>
<?php
$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'val_vinkel_45()',
		'content' => 'Frem ->' 
);
$back = "'" . base_url ( 'home/vinkel' ) . "'";
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $back,
		'content' => '<-- Tilbage' 
);
$under_capt = array (
		'undertag' => 'Undertag',
		'understrygning' => 'Understrygning' 
);
$valg_af_capt = array (
		'False' => 'Alm. rygningssten',
		'True' => 'Faconrygningssten' 
);

?>
<?php

echo form_open ( 'getdata/vinkel_45', array (
		'id' => 'vinkel_45' 
) );
?>
<div class="container">
	<div class="row">
		<h4 class="col-md-8">Data for hovedhus</h4>
		<div class=col-md-8 id="a">
			<div class=row>
				<div class=col-md-12>
					<div class="row input-group">
						<div class="textfieldtext col-md-7">
						a:  Længde på hus incl. udhæng:
						</div>
						<div class="col-md-3 ">
						<?php
						$data = array (
								'name' => 'hvhus2_l',
								'id' => 'hvhus2_l',
								'class' => 'textfield textfieldwithspan num' 
						);
						echo form_input ( $data );
						?>
						<span class="textfieldspan">m</span> 
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class=col-md-8 id="b">
			<div class=row>
				<div class=col-md-12>
					<div class="row input-group">
						<div class="textfieldtext col-md-7">
						b:  Bredde på hus incl. udhæng:
						</div>
						<div class="col-md-3 ">
						<?php
						$data = array (
								'name' => 'hvhus2_b',
								'id' => 'hvhus2_b',
								'class' => 'textfield textfieldwithspan num' 
						);
						echo form_input ( $data );
						?>
						<span class="textfieldspan">m</span> 
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class=col-md-8 id="c">
			<div class=row>
				<div class=col-md-12>
					<div class="row input-group">
						<div class="textfieldtext col-md-7">
						c:  Taghældning:
						</div>
						<div class="col-md-3 ">
						<?php
						$data = array (
								'name' => 'hvhus2_v',
								'id' => 'hvhus2_v',
								'class' => 'textfield textfieldwithspan num' 
						);
						echo form_input ( $data );
						?>
						<span class="textfieldspan">°</span> 
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="col-md-12" id="imgcont" style="margin-top:-100px">	</div>
			<div class="col-md-12" id="imgcont3d">	</div>
		</div>
	</div>

		<h4 style="col-md-8">Data for vinkel 1</h4>
		<div class=col-md-8 id="d">
			<div class=row>
				<div class=col-md-12>
					<div class="row input-group">
						<div class="textfieldtext col-md-7">
						d: Længde på hus incl. udhæng:
						</div>
						<div class="col-md-3 ">
						<?php
						$data = array (
								'name' => 'hvhus2_l2',
								'id' => 'hvhus2_l2',
								'class' => 'textfield textfieldwithspan num' 
						);
						echo form_input ( $data );
						?>
						<span class="textfieldspan">m</span> 
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class=col-md-8 id="e">
			<div class=row>
				<div class=col-md-12>
					<div class="row input-group">
						<div class="textfieldtext col-md-7">
						e: Bredde på hus incl. udhæng:
						</div>
						<div class="col-md-3 ">
						<?php
						$data = array (
								'name' => 'hvhus2_b2',
								'id' => 'hvhus2_b2',
								'class' => 'textfield textfieldwithspan num' 
						);
						echo form_input ( $data );
						?>
						<span class="textfieldspan">m</span> 
						</div>
					</div>
				</div>
			</div>
		</div>

		<h4 class="col-md-8 datavinkel2">Data for vinkel 2</h4>
		<div class="col-md-8 datavinkel2" id="g">
			<div class=row>
				<div class=col-md-12>
					<div class="row input-group">
						<div class="textfieldtext col-md-7">
						f:  Længde på hus incl. udhæng:
						</div>
						<div class="col-md-3 ">
						<?php
						$data = array (
								'name' => 'hvhus2_l3',
								'id' => 'hvhus2_l3',
								'class' => 'textfield textfieldwithspan num' 
						);
						echo form_input ( $data );
						?>
						<span class="textfieldspan">m</span> 
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-8 datavinkel2" id="h">
			<div class=row>
				<div class=col-md-12>
					<div class="row input-group">
						<div class="textfieldtext col-md-7">
						g:  Bredde på hus incl. udhæng:
						</div>
						<div class="col-md-3 ">
						<?php
						$data = array (
								'name' => 'hvhus2_b3',
								'id' => 'hvhus2_b3',
								'class' => 'textfield textfieldwithspan num' 
						);
						echo form_input ( $data );
						?>
						<span class="textfieldspan">m</span> 
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class=col-md-12>
			<div class="row">
				<div class="col-md-6">
					<h4>Undertag/Understrygning</h4>
					<?php
					foreach ( $under_capt as $value => $capt ) :
					$radio_under = array (
							'name' => 'under',
							'value' => $value,
							'id' => $value 
					);
					?>
							<div class=col-md-12>

				  <?php echo form_radio($radio_under)?><label for="<?php echo $radio_under['id']?>"></label><span class=radiotext><?php echo $capt?></span>
				  
						</div>	
				<?php endforeach; ?>
				</div>
				<div class="col-md-6" style="display:none">
					<h4>Valg af rygningssten</h4>
					<?php  

					foreach ( $valg_af_capt as $value => $capt ) :
						$radio_valg_af = array (
								'name' => 'valg_af',
								'value' => $value,
								'id' => $value 
						);
						?>
						<div class=col-md-12>		

					  <?php echo form_radio($radio_valg_af)?><label for="<?php echo $radio_valg_af['id']?>"></label><span class=radiotext><?php echo $capt?></span>
					  </div>
								
					<?php endforeach; ?>
				</div>
			</div>
		</div>

	
	<div class=row style="margin-bottom:200px">
		<div class="col-md-12">
			<div style="float:right">
			<img src="<?php echo base_url('assets')?>/img/left.png" onclick="window.location.href=<?php echo $back?>" style="cursor:pointer;">
			<img src="<?php echo base_url('assets')?>/img/right.png" onclick = 'val_vinkel_45()' style="cursor:pointer;">
			</div>
		</div>
	<div>
</div>
<span id="a"></span>
<span id="b"></span>
<span id="c"></span>
<span id="d"></span>
<span id="e"></span>
<span id="g"></span>
<span id="h"></span>
<!--
<div class="row">
	<div class="col-md-6">
		<h4>Data for hovedhus</h4>
		<ul class="list-group">

			<li class="list-group-item" id="a">


				<div class="input-group">
		a:  Længde på hus incl. udhæng:
		<?php
		$data = array (
				'name' => 'hvhus2_l',
				'id' => 'hvhus2_l',
				'class' => 'form-control incolright num num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>

			<li class="list-group-item" id="b">


				<div class="input-group">
		b:  Bredde på hus incl. udhæng:
		<?php
		$data = array (
				'name' => 'hvhus2_b',
				'id' => 'hvhus2_b',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>

			<li class="list-group-item" id="c">


				<div class="input-group">
		c:  Taghældning:
		<?php
		$data = array (
				'name' => 'hvhus2_v',
				'id' => 'hvhus2_v',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">°</span>
				</div>
			</li>
		</ul>
		<h4>Data for vinkel 1</h4>
		<ul class="list-group">

			<li class="list-group-item" id="d">


				<div class="input-group">
					<span id="labeld">d: Længde på hus incl. udhæng:</span>
		<?php
		$data = array (
				'name' => 'hvhus2_l2',
				'id' => 'hvhus2_l2',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>

			<li class="list-group-item" id="e">


				<div class="input-group">
					<span id="labele">e: Bredde på hus incl. udhæng:</span>
		<?php
		$data = array (
				'name' => 'hvhus2_b2',
				'id' => 'hvhus2_b2',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>


		</ul>
		<h4 class="datavinkel2">Data for vinkel 2</h4>
		<ul class="list-group datavinkel2">

			<li class="list-group-item" id="g">


				<div class="input-group">
		f:  Længde på hus incl. udhæng:
		<?php
		$data = array (
				'name' => 'hvhus2_l3',
				'id' => 'hvhus2_l3',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>

			<li class="list-group-item" id="h">


				<div class="input-group">
		g:  Bredde på hus incl. udhæng:
		<?php
		$data = array (
				'name' => 'hvhus2_b3',
				'id' => 'hvhus2_b3',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?> 
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>


		</ul>
	</div>
	<div class="col-md-6" id="imgcont">	</div>
	<div class="col-md-6" id="imgcont3d">	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<!-- 	Undertag/Understrygning 
		<h4>Undertag/Understrygning</h4>
		<ul class="list-group">
			
<?php

foreach ( $under_capt as $value => $capt ) :
	$radio_under = array (
			'name' => 'under',
			'value' => $value,
			'id' => $value 
	);
	?>
			<li class="list-group-item">

  <?php echo form_radio($radio_under).$capt?>
  
  			</li>
<?php endforeach; ?>
		</ul>
		<!-- 		Valg af rygningssten 
	</div>
	<div class="col-md-6" id="valgaf">
		<h4>Valg af rygningssten</h4>
		<ul class="list-group">
			
<?php

foreach ( $valg_af_capt as $value => $capt ) :
	$radio_valg_af = array (
			'name' => 'valg_af',
			'value' => $value,
			'id' => $value 
	);
	?>
			<li class="list-group-item">

  <?php echo form_radio($radio_valg_af).$capt?>
  
  			</li>
<?php endforeach; ?>
		</ul>
	</div>
</div>

<?php
echo form_button ( $buttonNext );
echo form_button ( $buttonBack );
echo form_close ();

?>-->