<script>
$( document ).ready(function() {
	baseurl = <?php echo '"'.base_url('assets/illustration').'"'; ?>;
	tagtype(baseurl);
	retning = <?php echo json_encode($retning)?>;
	load_tagtype(retning);

	});
</script>
<style>
#per{
	margin-top:40px;
}
#imgcont3d{
	margin-top:50px;
	margin-bottom:100px;
	overflow:hidden;
}

#imgcont{
	margin-top:50px;
	margin-bottom:100px;
	overflow:hidden; 
}
</style>
<?php
$hovedhus_capt = array (
		'1' => 'Længehus',
		'2' => 'Vinkelhus',
		'3' => 'Tag med ensidig hældning (frit)',
		'4' => 'Tag med ensidig hældning (ved mur)',
		'5' => 'Vinkelhus 45°',
		'6' => 'Pyramidetag',
		'7' => 'H - Hus' 
);
$tilb_capt = array (
		'1' => 'Ingen tilbygning',
		'2' => 'Tilbygning',
		'3' => 'Knast' 
);
$valm_capt = array (
		'1' => 'Gavl',
		'2' => 'Helvalm',
		'3' => 'Halvvalm' 
);
$kvist_capt = array (
		'1' => 'Ingen kvist',
		'2' => 'Kvist med gavltag',
		'3' => 'Kvist med helvalm',
		'4' => 'Kvist med halvvalm' 
);
$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'tagtypesubm()',
		'content' => 'Frem ->' 
);
$back = base_url ( 'home/farvevalg' );
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $back,
		'content' => '<-- Tilbage' 
);
?>



<div class="container">
	<h4>Vælg ønsket tag, ved at markere felterne</h4>
	<div class="row">
		
	</div>
</div>

<div class="container">
	<div class=row>
		<?php

echo form_open ( 'getdata/tagtype', array (
		'id' => 'tagtype' 
) );
?>
		<div class=col-md-4>
		<div class=row>
			<div class="col-md-12">
				<h4>Valg af hovedhus</h4> 
			</div>
			<?php

			foreach ( $hovedhus_capt as $num => $capt ) :
				$radio_hovedhus = array (
						'name' => 'hovedhus',
						'id' => 'hovedhus' . $num,
						'value' => $num,
						'onclick' => 'tagtype()' 
				);
			?>

			
			<div class=col-md-12 style="align-vertical:middle;">
				
				
					
						<?php echo form_radio($radio_hovedhus)?><label for="<?php echo $radio_hovedhus['id']?>"></label><span class=radiotext><?php echo $capt?></span>
						
					
					
					<?php if ($num==2):?>

						<div style="float:right">
						<select name="antalvinklervinkel" id="vinklervinkelhus" style="float:right"
							onchange="tagtype()">
							<option value="1vinkel">1 Vinkel</option>
							<option value="2vinkler">2 Vinkler</option>
						</select><div style="pointer-events:none;display:block;" ></div>
						</div>
						





					
					<?php elseif ($num==5):?>
					<font class=text-left>
						<div style="float:right">
						<select name="antalvinkler" id="vinkel45grader" onchange="tagtype()" style="float:right">
								<option value="1vinkel">1 Vinkel</option>
								<option value="2vinkler">2 Vinkler</option>
						</select>
						</div>
					</font>
					<?php endif;?>
					
			</div>
			<?php endforeach; ?>
		
	
			<?php

			foreach ( $tilb_capt as $num => $capt ) :
				$radio_tilb = array (
						'name' => 'tilb',
						'id' => 'tilb' . $num,
						'value' => $num,
						'onclick' => 'tagtype()' 
				);
				?>
				<div class=col-md-12>
					
							<?php echo form_radio($radio_tilb)?><label for="<?php echo $radio_tilb['id']?>"></label><span class=radiotext><?php echo $capt?></span>
						
						<?php if ($num==2):?>
							<font class=text-left>
								<div style="float:right">
								<select name="antaltilbygninger" id="tilbygninger" onchange="tagtype()" style="float:right">
									<option value="1tilbygning">1 Tilbygning</option>
									<option value="2tilbygninger">2 Tilbygninger</option>
								</select><div style="pointer-events:none;display:block; align:right;" ></div>
								</div>
							</font>
						<?php elseif ($num==3):?>
							<font class=text-left>
								<div style="float:right">
								<select name="antalknaster" id="knaster" onchange="tagtype()" style="float:right">
									<option value="1knast">1 Knast</option>
									<option value="2knaster">2 Knaster</option>
								</select><div style="pointer-events:none;display:block;" ></div>
								</div>
							</font>
						<?php endif;?>
					
				</div>
			<?php endforeach; ?>
				
		</div>
		</div>
		
		<div class=col-md-8>
			<div class=row>
				<div class=col-md-6>
					<div class="col-md-12">
						<h4>Valg af valme</h4>
					</div>
					<?php

					foreach ( $valm_capt as $num => $capt ) :
						$radio_valm = array (
								'name' => 'valm',
								'id' => 'valm' . $num,
								'value' => $num,
								'onclick' => 'tagtype()' 
						);
						?>
					<div  class=col-md-12>
						<?php echo form_radio($radio_valm)?><label for="<?php echo $radio_valm['id']?>"></label><span class=radiotext><?php echo $capt?></span>
						</div>
					<?php
					endforeach
					;
					?>
				</div>
				<div class=col-md-6>
					<div class="col-md-12">
						<h4>Valg af kviste</h4>
					</div>
					<div  class=col-md-12 style="height:45px;">
							<span class="radiotext" style="margin-top:10px;">Antal kviste</span>
						

							<?php 
							// $data = array (
									// 'name' => 'antalkviste',
									// 'id' => 'antalkviste',
									// 'value' => '0',
									// 'class' => 'form-control',
									// 'onchange' => 'tagtype()' 
							// );
							// echo form_input ( $data );
							?>
							<div style="float:right">
							<select id="antalkviste" name="antalkviste" onchange="tagtype()">
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
							</select>
							</div>

					</div>
					<?php

					foreach ( $kvist_capt as $num => $capt ) :
						$radio_kvist = array (
								'name' => 'kvist',
								'id' => 'kvist' . $num,
								'value' => $num,
								'onclick' => 'tagtype()' 
						);
						?>
						<div  class=col-md-12>

					  <?php echo form_radio($radio_kvist)?><label for="<?php echo $radio_kvist['id']?>"></label><span class=radiotext><?php echo $capt?></span>
					  </div>
					<?php endforeach; ?>
				</div>
			</div>
			<div class=row>
				<div class=col-md-12>
						<div class="col-md-4 col-md-offset-2">
							<div id="imgcont" style=""></div>
						</div>
						<div class=col-md-6>
							<div id="imgcont3d" style=""></div>
						</div>
						<!--<a href=# onclick='    $(".illustration").css({
        position: "relative"
    }).show(); return false;'>deconstruct</a><BR>
						<a href=# onclick='    $(".illustration").css({
        position: "absolute"
    }).show(); return false;'>construct</a>-->
				</div>
			</div>
		</div>
	</div>
	<div class=row style="margin-bottom:200px">
		<div class="col-md-12">
			<div style="float:right">
			<img src="<?php echo base_url('assets')?>/img/left.png" onclick="window.location.href='<?php echo $back?>'" style="cursor:pointer;">
			<img src="<?php echo base_url('assets')?>/img/right.png" onclick = 'tagtypesubm();' style="cursor:pointer;">
			</div>
		</div>
	<div>  
</div>
<!--
<div class="row">
	<div class="col-md-6">
		<!-- Valg of hofedus 
		<h4>Valg af hovedhus</h4>
		<ul class="list-group">	
	
<?php

foreach ( $tilb_capt as $num => $capt ) :
	$radio_tilb = array (
			'name' => 'tilb',
			'id' => 'tilb' . $num,
			'value' => $num,
			'onclick' => 'tagtype()' 
	);
	?>
	
 <?php if ($num==2 || $num==3):?>
  <li class="list-group-item withdropdown">
  <?php else:?>
  
			
			
			
			<li class="list-group-item">
  <?php endif;?>
  <?php echo form_radio($radio_tilb).$capt?>
  <?php if ($num==2):?>
  <select name="antaltilbygninger" class="form-control" id="tilbygninger" onchange="tagtype()">
					<option value="1tilbygning">1 Tilbygning</option>
					<option value="2tilbygninger">2 Tilbygninger</option>
			</select>
  <?php elseif ($num==3):?>
  <select name="antalknaster" class="form-control" id="knaster" onchange="tagtype()">
					<option value="1knast">1 Knast</option>
					<option value="2knaster">2 Knaster</option>
			</select>
  <?php endif;?>
  </li>
<?php endforeach; ?>
	</ul>
		<!-- 	End of Valg of hofedus
		<br>
		<!-- 		Beggining of Valg af valme 
		<h4>Valg af valme</h4>
		<ul class="list-group">	
	
<?php

foreach ( $valm_capt as $num => $capt ) :
	$radio_valm = array (
			'name' => 'valm',
			'id' => 'valm' . $num,
			'value' => $num,
			'onclick' => 'tagtype()' 
	);
	?>
<li class="list-group-item">
<?php echo form_radio($radio_valm).$capt;?>
</li>
<?php
endforeach
;
?>
	</ul>
	</div>
	<!-- /.col-md-6 
	<div class="col-md-6">
		<h4>Valg af kviste</h4>
		<ul class="list-group">
			<li class="list-group-item">
		Antal kviste
		<?php
		$data = array (
				'name' => 'antalkviste',
				'id' => 'antalkviste',
				'value' => '0',
				'class' => 'form-control incol',
				'onchange' => 'tagtype()' 
		);
		echo form_input ( $data );
		?>
		</li>
<?php

foreach ( $kvist_capt as $num => $capt ) :
	$radio_kvist = array (
			'name' => 'kvist',
			'id' => 'kvist' . $num,
			'value' => $num,
			'onclick' => 'tagtype()' 
	);
	?>
	<li class="list-group-item">

  <?php echo form_radio($radio_kvist).$capt?>
  
  </li>
<?php endforeach; ?>
</ul>
<?php
echo form_button ( $buttonNext );
echo form_button ( $buttonBack );
echo form_close ();
?>
	</div>
	<!-- /.col-md-6 
</div>
/.row -->
<script type="text/javascript" src="../assets/js/jquery.selectbox-0.2.js"></script>

<script>
	$(function () {
		$("#vinklervinkelhus").selectbox();
		$("#vinkel45grader").selectbox();
		$("#knaster").selectbox();

		$("#antalkviste").selectbox({
			classHolder:'sbHolderS', 
			classOptions:'sbOptionsS',
			classSelector:'sbSelectorS'
		});		
		$("#tilbygninger").selectbox({
			classHolder:'sbHolderL', 
			classOptions:'sbOptionsL',
			classSelector:'sbSelectorL'
		});
		
	});
</script>

