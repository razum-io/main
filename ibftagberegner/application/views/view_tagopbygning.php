
<?php
$back = "'" . base_url ( 'home/beregn' ) . "'";
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $back,
		'content' => '<-- Tilbage' 
);
?>

<div class="row">
	<div class="col-md-8">
		<div class="well">
			<h3>Tagopbygning</h3>
			<table class="table">
				<tr>
					<th></th>
					<th><?php echo $label1;?></th>
					<th><?php echo $label7;?></th>
					<th><?php echo $label8;?></th>
					<th><?php echo $label9;?></th>
				</tr>
				<tr>
					<th><?php echo $label2;?></th>
					<td><?php echo $lengde_hh;?></td>
					<td><?php echo $lengde_tb;?></td>
					<td><?php echo $lengde_v;?></td>
					<td><?php echo $lengde_k;?></td>
				</tr>
				<tr>
					<th><?php echo $label3;?></th>
					<td><?php echo $bredde_hh;?></td>
					<td><?php echo $bredde_tb;?></td>
					<td><?php echo $bredde_v;?></td>
					<td><?php echo $bredde_k;?></td>
				</tr>
				<tr>
					<th><?php echo $label4;?></th>
					<td><?php echo $tagh_hh;?></td>
					<td><?php echo $tagh_tb;?></td>
					<td><?php echo $tagh_v;?></td>
					<td><?php echo $tagh_k;?></td>
				</tr>
				<tr>
					<th><?php echo $label5;?></th>
					<td><?php echo $kol_hh;?></td>
					<td><?php echo $kol_tb;?></td>
					<td><?php echo $kol_v;?></td>
					<td><?php echo $kol_k;?></td>
				</tr>
				<tr>
					<th><?php echo $label6;?></th>
					<td><?php echo $rek_hh;?></td>
					<td><?php echo $rek_tb;?></td>
					<td><?php echo $rek_v;?></td>
					<td><?php echo $rek_k;?></td>
				</tr>
			</table>
		</div>
		<br>
		<div class="row">
			<div class="col-md-5">
				<div class="well">
					<table class="table nonbordered">
						<tr>
							<th>Rygning :</th>
							<td><?php echo round($rygning_meter,3);?> Meter</td>

						</tr>
						<tr>
							<th>Grater :</th>
							<td><?php echo round($grater_meter,3);?> Meter</td>
						</tr>
						<tr>
							<th>Klemliste :</th>
							<td><?php echo round($klemliste,3);?> Meter</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

	</div>
	<div class="col-md-4">
		<div class="well">
			<h3>Lægteafstand</h3>
			<img style="width:100%;"src="<?php echo base_url('assets/css/images/legteafstand.bmp')?>">
			<table class="table">
				<tr>
					<th></th>
					<th><?php echo $label17;?></th>
					<th><?php echo $label19;?></th>
					<th><?php echo $label18;?></th>
				</tr>
				<tr>
					<th><?php echo $label20;?></th>
					<td><?php echo $text1;?></td>
					<td><?php echo $til_h;?></td>
					<td><?php echo $vinkel_v;?></td>
				</tr>
				<tr>
					<th><?php echo $label22;?></th>
					<td><?php echo $text2;?></td>
					<td><?php echo $tilbyg_c;?></td>
					<td><?php echo $vinkel_c;?></td>
				</tr>
				<tr>
					<th><?php echo $label21;?></th>
					<td><?php echo $text3;?></td>
					<td><?php echo $tilbyg_b;?></td>
					<td><?php echo $vinkel_b;?></td>
				</tr>
			</table>
		</div>
	</div>
</div>




<?php
echo form_button ( $buttonBack );
echo form_close ();

?>
