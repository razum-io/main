<script>
$(document).ready(function(){
	$(".clickableRow").click(function() {
		window.document.location = $(this).attr("href");
	});
})
</script>
<div class="row">
			<div class="col-md-12 ">
				<div class="panel panel-primary">
					<div class="panel-heading">Colors <a href="<?php echo base_url('color/add_color/')?>" style="color:white; float:right" class=panelLink>Add Color</a></div>
					<table class="table">
						<tr>
							<th>Name</th>
							<th>Dobbelt-S</th>
							<th>Vinge Okonomi</th>
							<th>Vinge Okonomi plus</th>
							<th></th>
						</tr>
						
						<?php foreach($data as $row){ ?>
						<tr>
							<td><?php echo $row->name?>
							<?php if ($row->hidden) echo "<BR>(hidden)";?>
							</td>
							<td><?php if(($row->dobbelt)){ ?><img src="<?php echo base_url('/assets/img/'.$row->dobbelt_file)?>"/><?php } ?></td>
							<td><?php if(($row->vinge)){ ?><img src="<?php echo base_url('/assets/img/'.$row->vinge_file)?>"/><?php } ?></td>
							<td><?php if(($row->vinge_plus)){ ?><img src="<?php echo base_url('/assets/img/'.$row->vinge_plus_file)?>"/><?php } ?></td>
							<td>
								<?php // if($row->code==""){ ?>
								<?php // } ?>
								<a class="btn" href="<?php echo base_url('color/update_color/'.$row->id)?>"
									>
									[Edit]
								</a>
								<a class="btn" href="<?php echo base_url('color/copy_color/'.$row->id)?>"
									>
									[Copy]
								</a>
								<?php if ($row->hidden) { ?>
								<a class="btn" href="<?php echo base_url('color/show/'.$row->id)?>">
									[Show]
								</a>
								<?php } else { ?>
								<a class="btn" href="<?php echo base_url('color/hide/'.$row->id)?>">
									[Hide]
								</a>
								<?php } ?>
								<a class="btn" href="<?php echo base_url('color/delete/'.$row->id)?>"
									onclick="return confirm('Are you sure?');">
									[Delete]
								</a>
							</td>
						</tr>
						<?php }?>

					</table>
				</div>
				<center>

			</div>
		</div>