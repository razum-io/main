<script>
$(document).ready(function(){
	$(".clickableRow").click(function() {
		window.document.location = $(this).attr("href");
	});
})
</script>
<div class="row">
			<div class="col-md-12 ">
				<div class="panel panel-primary">
					<div class="panel-heading"></div>
					<form action="<?php echo base_url('/color/color_parts_update/'.$id)?>" method="POST">
					<table class="table">
						<tr>
							<th>IBF</th>
							<th>TUN</th>
							<th>Navn</th>
							<th>IBF</th>
							<th>TUN</th>
							<th>Navn</th>
						</tr>
						
						<?php foreach($data as $obj){ ?>
						<tr>
							<input type="hidden" name="id[]" value="<?php echo $obj->id?>">
							<td><input type="text"  size="5" value="<?php echo $obj->ibf1?>" readonly></td>
							<td><input type="text" size="5" value="<?php echo $obj->tun1?>" readonly></td>
							<td><input type="text" size="50" value="<?php echo replace_danish($obj->navn1)?>" readonly></td>
							<td><input type="text" size="5" name="ibf2_<?php echo $obj->id?>" value="<?php echo $obj->ibf2?>"></td>
							<td><input type="text" size="5" name="tun2_<?php echo $obj->id?>" value="<?php echo $obj->tun2?>"></td>
							<td><input type="text" size="50" name="navn2_<?php echo $obj->id?>" value="<?php echo replace_danish($obj->navn2)?>"></td>
						</tr>
						<?php }?>
						<tr>
							<td colspan=6> 
								Replace <input type=text size="10" name=replace_what> with <input type=text size="10" name=replace_with> (press Save to replace)
								<div class="pull-right"><button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo base_url()?>color'">Back</button>
								<button type="submit" class="btn btn-success">Save</button></div>
							</td>
						</tr>
					</table>
						
							</form>
				</div>
				<center>

			</div>
		</div>
	
<?php
function replace_danish($s)
{	
	$s = str_replace("ų","ø",$s);
	$s = str_replace("Ų","Ø",$s);
	$s = str_replace("ę","æ",$s);
	$s = str_replace("Ę","Æ",$s);
	return $s;
}
?>