
<style>
#per {
	margin-top: 40px;
}

#imgcont3d {
	margin-top: 150px;
}
</style>
<script>
hoved = <?php echo '"'.$hoved.'"'; ?>;
$( document ).ready(function() {
stenvalgt = <?php echo '"'.$stenvalgt.'"'; ?>;
hvhus1(stenvalgt);

dynValmType = <?php echo $dynValmType; ?>;
baseurl = <?php echo '"'.base_url('assets/illustration').'"'; ?>;
antalVinklerVinkel = <?php echo '"'.$antalvinklervinkel.'"'; ?>;
antalVinkler = <?php echo '"'.$antalvinkler.'"'; ?>;
antalTilbygninger = <?php echo '"'.$antaltilbygninger.'"'; ?>;
antalKnaster = <?php echo '"'.$antalknaster.'"'; ?>;
tag = <?php echo '"'.$tag.'"'; ?>;
drawillustration('hvhus1'); 
retning = (<?php echo json_encode($retning)?>);
load_hvhus1(retning);
});
</script>

<?php
$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'val_hvhus1()',
		'content' => 'Frem ->' 
);
$back = "'" . base_url ( 'home/vinkel' ) . "'";
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $back,
		'content' => '<-- Tilbage' 
);
$under_capt = array (
		'undertag' => 'Undertag',
		'understrygning' => 'Understrygning' 
);
$valg_af_capt = array (
		'False' => 'Alm. rygningssten',
		'True' => 'Faconrygningssten' 
);
?>
<?php

echo form_open ( 'getdata/hvhus1', array (
		'id' => 'hvhus1' 
) );
?>
<div class="row">
	<div class="col-md-6">
		<ul class="list-group">
			<li class="list-group-item">


				<div class="input-group">
		a:  Længde på hus incl. udhæng:
		<?php
		$data = array (
				'name' => 'hvhus1_l',
				'id' => 'hvhus1_l',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>

			<li class="list-group-item">


				<div class="input-group">
		b:  Bredde på hus incl. udhæng:
		<?php
		$data = array (
				'name' => 'hvhus1_b',
				'id' => 'hvhus1_b',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>

			<li class="list-group-item">


				<div class="input-group">
		c:  Taghældning:
		<?php
		$data = array (
				'name' => 'hvhus1_v',
				'id' => 'hvhus1_v',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">°</span>
				</div>
			</li>
		</ul>
	</div>
	<div class="col-md-6" id="per"></div>
</div>
<div class="row">
	<div class="col-md-6">
		<!-- 	Undertag/Understrygning -->
		<h4>Undertag/Understrygning</h4>
		<ul class="list-group">
			
<?php

foreach ( $under_capt as $value => $capt ) :
	$radio_under = array (
			'name' => 'under',
			'value' => $value,
			'id' => $value 
	);
	?>
			<li class="list-group-item">

  <?php echo form_radio($radio_under).$capt?>
  
  			</li>
<?php endforeach; ?>
		</ul>
		<!-- 		Valg af rygningssten -->
		<div id="valgaf">
			<h4>Valg af rygningssten</h4>
			<ul class="list-group">
			
<?php  

foreach ( $valg_af_capt as $value => $capt ) :
	$radio_valg_af = array (
			'name' => 'valg_af',
			'value' => $value,
			'id' => $value 
	);
	?>
			<li class="list-group-item">

  <?php echo form_radio($radio_valg_af).$capt?>
  
  			</li>
<?php endforeach; ?>
		</ul>
		</div>
	</div>
</div>

<?php
echo form_button ( $buttonNext );
echo form_button ( $buttonBack );
echo form_close ();

?>