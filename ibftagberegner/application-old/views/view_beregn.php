<style>
#imgcont {
	width:200px;
	height:120px;
	display: inline-block;
}

#imgcont3d {
	//float:right;
	width:200px;
	height:120px;
	display: inline-block;
}
</style>
<script>
$("#navia").append('<li><a href="<?php echo base_url('home/index/beregn');?>">Skift stentype</a></li>');
$("#navia").append('<li><a href="<?php echo base_url('home/tagopbygning');?>">Tagopbygning</a></li>');
$("#navia").append('<li><a href="#" onclick="window.print();">Print</a></li>');

$( document ).ready(function() {
	dynValmType = <?php echo $dynValmType; ?>;
	baseurl = <?php echo '"'.base_url('assets/illustration').'"'; ?>;
	antalVinklerVinkel = <?php echo '"'.$antalvinklervinkel.'"'; ?>;
	antalVinkler = <?php echo '"'.$antalvinkler.'"'; ?>;
	antalTilbygninger = <?php echo '"'.$antaltilbygninger.'"'; ?>;
	antalKnaster = <?php echo '"'.$antalknaster.'"'; ?>;
	tag = <?php echo '"'.$tag.'"'; ?>;
	drawillustration('beregn');
	if (hoved == 5){
		$('#imgcont3d').css('margin-top','100px'); 
	}
});
function changeantal(elem){
	 var antal = prompt("", elem.innerHTML);
	 if (antal != null) {
		 elem.innerHTML = antal;	    
		 }
	 
}
</script>
<?php
$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'class' => 'form-control next',
		'type' => 'submit',
		'content' => 'Frem ->' 
);
$backlbl = "'" . $back . "'";
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $backlbl,
		'content' => '<-- Tilbage' 
);
?>
<?php

echo form_open ( 'getdata/tagmetode', array (
		'id' => 'beregn' 
) );
?>

<div class="row">
	<div class="col-md-4">
		<h3>Kunde</h3>
		<table class="table">
			<tr>
				<th>Navn :</th>
				<td><?php echo $Navn;?></td>
			</tr>
			<tr>
				<th>Adresse :</th>
				<td><?php echo $adresse;?></td>
			</tr>
			<tr>
				<th>By :</th>
				<td><?php echo $by;?></td>
			</tr>
			<tr>
				<th>Telefon :</th>
				<td><?php echo $telefon;?></td>
			</tr>
			<tr>
				<th>Tilbudsnummer :</th>
				<td><?php echo $tilbudsnummer;?></td>
			</tr>
			<tr>
				<th>Beregningstidspunkt :</th>
				<td><?php echo $beregningstidspunkt;?></td>
			</tr>
		</table>
	</div>
	<div class="col-md-4">
		<h3>Taget</h3>
		<table class="table">
			<tr>
				<th>Tagflade :</th>
				<td><?php echo round($frmflade,3);?><?php if (round($frmflade,3)!==0):?>
				m<sup>2</sup>
				<?php endif;?> </td>
			</tr>
			<tr>
				<th>Meter Lægte :</th>
				<td><?php echo round($frmlegte,3);?> m</td>
			</tr>
			<tr>
				<th>Meter Tagrende :</th>
				<td><?php echo round($frmtagrende,3);?> m</td>
			</tr>

		</table>
	</div>
</div>
<BR>
	<div id="imgcont"></div>
	<div id="imgcont3d"></div>
<br>
<div class="row">
	<div class="col-md-12">


		<table class="table table-hover">
			<tr>
				<th>Varenr:</th>
				<th>DBnr:</th>
				<th>Varebeskrivelse:</th>
				<th>Mængde:</th>
				<th>Enhed:</th>
			</tr>
			 <?php
				$search = array (
						'Ų',
						'Ę' 
				);
				$replace = array (
						'Ø',
						'Æ' 
				);
				foreach ( $tilgTable_stand as $obj ) :
					if (( int ) $obj->Antal > 0) :
						?>
						<tr>
				<td><?php echo $obj->IBF?></td>
				<td><?php echo $obj->TUN?></td>
				<td><?php echo str_replace($search, $replace,$obj->Navn);?></td>
				<td onclick="changeantal(this)"><?php echo $obj->Antal?></td>
				<td><?php echo $obj->Enhed?></td>

			</tr>
					
					
					
					
					
					
					
					
					
					
					
					
					<?php
endif;
				endforeach
				;
				
				?>
			 

		</table>
	</div>

</div>




<?php
echo form_button ( $buttonBack );
echo form_close ();

?>
