<script>
$( document ).ready(function() {
	retning = (<?php echo json_encode($retning)?>);
	load_farvevalg(retning);
	});
</script>
<?php

$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'class' => 'form-control next',
		'type' => 'submit',
		'content' => 'Frem ->' 
);
$back = "'" . base_url ( 'home/kunde' ) . "/" . $stenvalgtnr . "'";
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $back,
		'content' => '<-- Tilbage' 
);
?>

<div class="row">
	<div class="col-md-6">
		<h4>Vælg tagets farve</h4>
<?php
// ///// FORM ///////

echo form_open ( 'getdata/farvevalg', array (
		'id' => 'farvevalg' 
) );

// prints list of radio buttons
foreach ( $colors as $obj ) {
	$radio = array (
			'name' => 'color',
			'id' => 'radio',
			'value' => $obj->id,
			'onclick' => "show_image2('$obj->image')" 
	);
	
	echo '<br>' . form_radio ( $radio );
	$colorname = str_replace ( "ų", "ø", $obj->name );
	echo $colorname;
}
?>
</div>
	<!-- /col-md-6 -->
	<div class="col-md-6">

		<img alt="" src="" id="farveimage">

	</div>
	<!-- /col-md-6 -->
</div>
<!-- /row -->
<div class="row">      
<?php
echo form_button ( $buttonNext );
echo form_button ( $buttonBack );
echo form_close ();
// // end of form///
?>
</div>