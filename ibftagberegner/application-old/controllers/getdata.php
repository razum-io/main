<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Getdata extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		$this->output->enable_profiler ( TRUE );
	}
	public function index($stenvalgtnr) {
		$this->session->set_userdata ( array (
				'stenvalgtnr' => $stenvalgtnr,
				'genberegningAfIndlaes' => 'false' 
		) ); 
		if ($this->session->userdata ( 'sentfrberegn' ) == 'YES') {
			redirect ( 'home/farvevalg' );
		}
		redirect ( 'home/kunde' );
	}
	public function kunde() {
		// get form data
		$this->form_validation->set_rules ( 'kundenavn', 'Kunde navn', 'xss_clean' );
		$this->form_validation->set_rules ( 'adresse', 'Adresse', 'xss_clean' );
		$this->form_validation->set_rules ( 'by', 'By', 'xss_clean' );
		$this->form_validation->set_rules ( 'telefon', 'Telefon', 'xss_clean' );
		$this->form_validation->set_rules ( 'tilbudsnr', 'Tilbudsnummer', 'xss_clean' );
		$this->form_validation->set_rules ( 'udfort', 'Udf�rt', 'xss_clean' );
		$this->form_validation->set_rules ( 'beregningstidspunkt', 'Beregningstidspunkt', 'xss_clean' );
		$this->form_validation->run ();
		// put it in variables 
		$kundenavn = set_value ( 'kundenavn' );
		$adresse = set_value ( 'adresse' );
		$by = set_value ( 'by' );
		$telefon = set_value ( 'telefon' );
		$tilbudsnr = set_value ( 'tilbudsnr' );
		$udfort = set_value ( 'udfort' );
		$beregningstidspunkt = set_value ( 'beregningstidspunkt' );
		$uniqid = uniqid ();
		// update session data
		$sesset = array (
				'kundenavn' => $kundenavn,
				'adresse' => $adresse,
				'by' => $by,
				'telefon' => $telefon,
				'tilbudsnr' => $tilbudsnr,
				'udfort' => $udfort,
				'beregningstidspunkt' => $beregningstidspunkt,
				'sessuser_id' => $uniqid 
		);
		$this->session->set_userdata ( $sesset );
		// test for debuging purposes:
		// print_r ( $this->session->all_userdata () );
		redirect ( 'home/farvevalg' );
	}
	public function farvevalg() {
		$color = $this->input->post ( 'color' );
		$nr = $color;
		
		$query = $this->db->query("select * from color where id = $color");
		$newcolor = $query->result();
		
		$this->session->set_userdata('custom_color',0);
			
		if (!$newcolor[0]->code)
		{
			// new custom color
			$this->session->set_userdata('custom_color',$newcolor[0]->id);
		}
		
		$query = $this->db->get_where ( 'farvetabel', array (
				'handle' => $newcolor[0]->field
		) );
		$color = $query->result ();
		// add variable to session
		$sesset = array (
				'farvevalgnr' => $color [0]->Farvekode,
				'farvevalg' => $color [0]->handle,
				'farveid' => $color [0]->id 
		);
		$this->session->set_userdata ( $sesset );
		
		// if change roof type and color
		if ($this->session->userdata ( 'sentfrberegn' ) == 'YES') {
			$this->session->unset_userdata('repeat6');
			$this->session->unset_userdata ( 'sentfrberegn' );
			$this->session->unset_userdata ( 'metode' );
			$this->session->unset_userdata ( 'tilgengelig' );
			$this->session->unset_userdata ( 'resmet' );
			$this->session->unset_userdata ( 'tmpnum' );
			$this->session->unset_userdata ( 'mal' );
			$this->model_ibf->deleteberegn ();
			$this->model_ibf->husnummer ();
		}
		// delete temporary session data
		// $sess = $this->session->all_userdata ();
		// foreach ( $sess as $key => $value ) {
		// if (substr ( $key, 0, 4 ) == 'res_') {
		// $this->session->unset_userdata ( $key );
		// }
		// }
		redirect ( 'home/tagtype' );
	}
	public function tagtype() {
		$this->form_validation->set_rules ( 'antalkviste', 'Antal Kviste', 'trim|xss_clean' );
		$this->form_validation->run ();
		$antalkviste = set_value ( 'antalkviste' );
		// check if is with value and if isnt set value to 0
		if ($this->input->post ( 'antalVinklerVinkel' )) {
			$antalVinklerVinkel = $this->input->post ( 'antalVinklerVinkel' )[0];
		} else {
			$antalVinklerVinkel = 0;
		}
		echo $antalVinklerVinkel;
		if ($this->input->post ( 'antalVinkler' )) {
			$antalVinkler = $this->input->post ( 'antalVinkler' )[0];
		} else {
			$antalVinkler = 0;
		}
		if ($this->input->post ( 'antalTilbygninger' )) {
			$antalTilbygninger = $this->input->post ( 'antalTilbygninger' )[0];
		} else {
			$antalTilbygninger = 0;
		}
		
		if ($this->input->post ( 'antalKnaster' )) {
			$antalKnaster = $this->input->post ( 'antalKnaster' )[0];
		} else {
			$antalKnaster = 0;
		}
		
		$dynValmType = $this->input->post ( 'dynValmType' );
		$dynValmPlacering = $this->input->post ( 'dynValmPlacering' );
		
		$laengehus = $this->input->post ( 'laengehus' );
		$tag = $this->input->post ( 'tag' );
		$hoved = $this->input->post ( 'hovedhus' );
		$tilbyg = $this->input->post ( 'tilb' );
		$valm = $this->input->post ( 'valm' );
		$kvist = $this->input->post ( 'kvist' );
		$globalAntalValgfrie = $this->input->post ( 'globalAntalValgfrie' );
		$setdata = array (
				'dynValmType' => $dynValmType,
				'dynValmPlacering' => $dynValmPlacering,
				'knast_met' => '0', // false
				'halve' => '0', // false
				'tilfoej_halv_fjern_hel' => '0', // false
				'facon' => '0', // false
				'tag' => $tag,
				'laengehus' => $laengehus,
				'laengehusbckup' => $laengehus,
				'antalVinklerVinkel' => $antalVinklerVinkel,
				'antalVinkler' => $antalVinkler,
				'antalTilbygninger' => $antalTilbygninger,
				'antalKnaster' => $antalKnaster,
				'hoved' => $hoved,
				'tilbyg' => $tilbyg,
				'valm' => $valm,
				'antalkviste' => $antalkviste,
				'kvist' => $kvist,
				'globalAntalValgfrie' => $globalAntalValgfrie 
		);
		$this->session->set_userdata ( $setdata );
		
		// redirect
		$data ['laengehus'] = $laengehus;
		if ($hoved == 2 || $hoved == 1 || $hoved == 5 || ($hoved == 6 && ( int ) $tilbyg !== 1) || $hoved == 7) {
			redirect ( 'home/valm' );
		} else {
			redirect ( 'home/vinkel' );
		}
	}
	public function valm() {
		$laengehus = $this->input->post ( 'laengehus' );
		$jsonType = $this->input->post ( 'dynValmType' );
		$jsonPlacering = $this->input->post ( 'dynValmPlacering' );
		// add variable to session
		$sesset = array (
				'laengehus' => $laengehus,
				'dynValmPlacering' => $jsonPlacering,
				'dynValmType' => $jsonType,
				'valm_retning' => 'TRUE' 
		);
		$this->session->set_userdata ( $sesset );
		redirect ( 'home/vinkel' );
	}
	public function vinkel() {
		$udluftningshaetter = $this->input->post ( 'udluftningshaetter' );
		// add variable to session
		echo $udluftningshaetter;
		$dynValmVinkel = array ();
		$dynValmTop = array ();
		
		$sesset = array (
				'udluftningshaetter' => $udluftningshaetter,
				'dynValmVinkel' => json_encode ( $dynValmVinkel ),
				'dynValmTop' => json_encode ( $dynValmTop ) 
		);
		$this->session->set_userdata ( $sesset );
		// redirect
		$hoved = $this->session->userdata ( 'hoved' );
		$tilbyg = $this->session->userdata ( 'tilbyg' );
		switch ($hoved) {
			case 1 :
				if ($tilbyg == 1) {
					redirect ( 'home/hvhus1' );
				} else {
					redirect ( 'home/hvhus2' );
				}
				break;
			case 2 :
				redirect ( 'home/hvhus2' );
				break;
			case 3 :
			case 4 :
				redirect ( 'home/hvhus3' );
				break;
			case 5 :
				redirect ( 'home/vinkel_45' );
				break;
			case 6 :
				if ($tilbyg == 1) {
					redirect ( 'home/hvhus1' );
				} else {
					redirect ( 'home/hvhus2' );
				}
				break;
			case 7 :
				redirect ( 'home/h_hus' );
				break;
		}
	}
	public function hvhus1() {
		$this->model_ibf->deleteberegn ();
		$gather ['tagunder'] = $this->model_ibf->checkPost ( 'under', false );
		$gather ['facon'] = $this->model_ibf->checkPost ( 'valg_af', false );
		$gather ['hl'] = $this->model_ibf->checkPost ( 'hvhus1_l', true );
		$gather ['hb'] = $this->model_ibf->checkPost ( 'hvhus1_b', true );
		$gather ['hv'] = $this->model_ibf->checkPost ( 'hvhus1_v', false );
		
		$gather ['valm_v1'] = $this->model_ibf->checkPost ( 'hvhus1_v', false );
		
		$hl_backup = $gather ['hl'];
		foreach ( $gather as $var => $value ) {
			if ($value !== 'nope') {
				$sesset [$var] = $value;
			}
		}
		$this->session->set_userdata ( $sesset );
		
		$valm = $this->session->userdata ( 'valm' );
		$kvist = $this->session->userdata ( 'kvist' );
		$tag = $this->session->userdata ( 'tag' );
		
		// redirect
		$dynValmType = ( array ) json_decode ( $this->session->userdata ( 'dynValmType' ) );
		$globalAntalValgfrie = $this->session->userdata ( 'globalAntalValgfrie' );
		$helValm = 0;
		$halvValm = 0;
		for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
			if ($dynValmType [$i] == 2) {
				$helValm = true;
			} else if ($dynValmType [$i] == 3) {
				$halvValm = true;
			}
		}
		if ($helValm == true) {
			$globalValmTypeMaalsaet = 2;
			$this->session->set_userdata ( array (
					'globalValmTypeMaalsaet' => $globalValmTypeMaalsaet 
			) );
			redirect ( 'home/maalsaet' );
		} else if ($halvValm == true) {
			$globalValmTypeMaalsaet = 3;
			$this->session->set_userdata ( array (
					'globalValmTypeMaalsaet' => $globalValmTypeMaalsaet 
			) );
			redirect ( 'home/maalsaet' );
		} 

		else if ($kvist > 1) {
			redirect ( 'home/kvist' );
		} else {
			$this->model_ibf->husnummer ();
		}
	}
	public function hvhus2() {
		$this->model_ibf->deleteberegn ();
		$hoved = $this->session->userdata ( 'hoved' );
		$tilbyg = $this->session->userdata ( 'tilbyg' );
		$antalTilbygninger = $this->session->userdata ( 'antalTilbygninger' );
		$antalKnaster = $this->session->userdata ( 'antalKnaster' );
		
		$gather ['tagunder'] = $this->model_ibf->checkPost ( 'under', false );
		$gather ['facon'] = $this->model_ibf->checkPost ( 'valg_af', false );
		
		$gather ['hl_t'] = $this->model_ibf->checkPost ( 'hvhus2_l3', true );
		$gather ['hb_t'] = $this->model_ibf->checkPost ( 'hvhus2_b3', true );
		$gather ['hv_t'] = $this->model_ibf->checkPost ( 'hvhus2_v3', false );
		$gather ['hl'] = $this->model_ibf->checkPost ( 'hvhus2_l', true );
		$gather ['hb'] = $this->model_ibf->checkPost ( 'hvhus2_b', true );
		$gather ['hv'] = $this->model_ibf->checkPost ( 'hvhus2_v', false );
		
		if ($hoved == 1 && $antalTilbygninger == 2) {
			$gather ['hb_t'] = $this->model_ibf->checkPost ( 'hvhus2_b2', true );
			$gather ['hv_t'] = $this->model_ibf->checkPost ( 'hvhus2_v2', false );
			$gather ['hl_t'] = $this->model_ibf->checkPost ( 'hvhus2_l2', true );
			
			$gather ['hl_v'] = $this->model_ibf->checkPost ( 'hvhus2_l3', true );
			$gather ['hb_v'] = $this->model_ibf->checkPost ( 'hvhus2_b3', true );
			$gather ['hv_V'] = $this->model_ibf->checkPost ( 'hvhus2_v3', false );
		}
		if ($hoved == 2) {
			$gather ['hl_v'] = $this->model_ibf->checkPost ( 'hvhus2_l2', true );
			$gather ['hb_v'] = $this->model_ibf->checkPost ( 'hvhus2_b2', true );
			$gather ['hv_V'] = $this->model_ibf->checkPost ( 'hvhus2_v2', false );
		}
		if ($tilbyg == 3) {
			$gather ['knhl'] = $this->model_ibf->checkPost ( 'hvhus2_l3', true );
			$gather ['knhb'] = $this->model_ibf->checkPost ( 'hvhus2_b3', true );
			$knhl_backup = $gather ['knhb'];
			if ($antalKnaster == 2) {
				$gather ['knhl2'] = $this->model_ibf->checkPost ( 'hvhus2_l2', true );
				$gather ['knhb2'] = $this->model_ibf->checkPost ( 'hvhus2_b2', true );
				$knhl2_backup = $gather ['knhb2'];
			}
		}
		if ($hoved == 6) {
			$sesset ['valm_v1'] = $gather ['hv'];
			$sesset ['valm_v3'] = $gather ['hv_t'];
		}
		
		foreach ( $gather as $var => $value ) {
			if ($value !== 'nope') {
				$sesset [$var] = $value;
			}
		}
		$this->session->set_userdata ( $sesset );
		// redirect
		$dynValmType = ( array ) json_decode ( $this->session->userdata ( 'dynValmType' ) );
		$globalAntalValgfrie = $this->session->userdata ( 'globalAntalValgfrie' );
		$kvist = $this->session->userdata ( 'kvist' );
		$tag = $this->session->userdata ( 'tag' );
		$helValm = 0;
		$halvValm = 0;
		for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
			if ($dynValmType [$i] == 2) {
				$helValm = true;
			} else if ($dynValmType [$i] == 3) {
				$halvValm = true;
			}
		}
		if ($helValm == true) {
			$globalValmTypeMaalsaet = 2;
			$this->session->set_userdata ( array (
					'globalValmTypeMaalsaet' => $globalValmTypeMaalsaet 
			) );
			redirect ( 'home/maalsaet' );
		} else if ($halvValm == true) {
			$globalValmTypeMaalsaet = 3;
			$this->session->set_userdata ( array (
					'globalValmTypeMaalsaet' => $globalValmTypeMaalsaet 
			) );
			redirect ( 'home/maalsaet' );
		} 

		else if ($kvist > 1) {
			redirect ( 'home/kvist' );
		} else {
			$this->model_ibf->husnummer ();
		}
	}
	public function hvhus3() {
		$this->model_ibf->deleteberegn ();
		$gather ['tagunder'] = $this->model_ibf->checkPost ( 'under', false );
		$gather ['hl'] = $this->model_ibf->checkPost ( 'hvhus3_l', true );
		$gather ['hb'] = $this->model_ibf->checkPost ( 'hvhus3_b', true );
		$gather ['hv'] = $this->model_ibf->checkPost ( 'hvhus3_v', false );
		
		$hl_backup = $gather ['hl'];
		foreach ( $gather as $var => $value ) {
			if ($value !== 'nope') {
				$sesset [$var] = $value;
			}
		}
		
		$this->session->set_userdata ( $sesset );
		// redirect
		$dynValmType = ( array ) json_decode ( $this->session->userdata ( 'dynValmType' ) );
		$globalAntalValgfrie = $this->session->userdata ( 'globalAntalValgfrie' );
		$kvist = $this->session->userdata ( 'kvist' );
		$tag = $this->session->userdata ( 'tag' );
		$helValm = 0;
		$halvValm = 0;
		for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
			if ($dynValmType [$i] == 2) {
				$helValm = true;
			} else if ($dynValmType [$i] == 3) {
				$halvValm = true;
			}
		}
		if ($helValm == true) {
			$globalValmTypeMaalsaet = 2;
			$this->session->set_userdata ( array (
					'globalValmTypeMaalsaet' => $globalValmTypeMaalsaet 
			) );
			redirect ( 'home/maalsaet' );
		} else if ($halvValm == true) {
			$globalValmTypeMaalsaet = 3;
			$this->session->set_userdata ( array (
					'globalValmTypeMaalsaet' => $globalValmTypeMaalsaet 
			) );
			redirect ( 'home/maalsaet' );
		} 

		else if ($kvist > 1) {
			redirect ( 'home/kvist' );
		} else {
			$this->model_ibf->husnummer ();
		}
	}
	public function vinkel_45() {
		$this->model_ibf->deleteberegn ();
		$gather ['tagunder'] = $this->model_ibf->checkPost ( 'under', false );
		$gather ['facon'] = $this->model_ibf->checkPost ( 'valg_af', false );
		$gather ['hl'] = $this->model_ibf->checkPost ( 'hvhus2_l', true );
		$gather ['hb'] = $this->model_ibf->checkPost ( 'hvhus2_b', true );
		$gather ['hv'] = $this->model_ibf->checkPost ( 'hvhus2_v', false );
		
		$gather ['hl_v'] = $this->model_ibf->checkPost ( 'hvhus2_l2', true );
		$gather ['hb_v'] = $this->model_ibf->checkPost ( 'hvhus2_b2', true );
		$gather ['hv_V'] = $gather ['hv'];
		
		if ($this->model_ibf->checkPost ( 'hvhus2_l3', true ) !== '') {
			$gather ['hl_t'] = $this->model_ibf->checkPost ( 'hvhus2_l3', true );
		} else {
			$gather ['hl_t'] = '0';
		}
		if ($this->model_ibf->checkPost ( 'hvhus2_b3', true ) !== '') {
			$gather ['hb_t'] = $this->model_ibf->checkPost ( 'hvhus2_b3', true );
		} else {
			$gather ['hb_t'] = '0';
		}
		$gather ['hv_t'] = $gather ['hv'];
		foreach ( $gather as $var => $value ) {
			if ($value !== 'nope') {
				$sesset [$var] = $value;
			}
		}
		$this->session->set_userdata ( $sesset );
		// redirect
		$dynValmType = ( array ) json_decode ( $this->session->userdata ( 'dynValmType' ) );
		$globalAntalValgfrie = $this->session->userdata ( 'globalAntalValgfrie' );
		$kvist = $this->session->userdata ( 'kvist' );
		$tag = $this->session->userdata ( 'tag' );
		$helValm = 0;
		$halvValm = 0;
		for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
			if ($dynValmType [$i] == 2) {
				$helValm = true;
			} else if ($dynValmType [$i] == 3) {
				$halvValm = true;
			}
		}
		if ($helValm == true) {
			$globalValmTypeMaalsaet = 2;
			$this->session->set_userdata ( array (
					'globalValmTypeMaalsaet' => $globalValmTypeMaalsaet 
			) );
			redirect ( 'home/maalsaet' );
		} else if ($halvValm == true) {
			$globalValmTypeMaalsaet = 3;
			$this->session->set_userdata ( array (
					'globalValmTypeMaalsaet' => $globalValmTypeMaalsaet 
			) );
			redirect ( 'home/maalsaet' );
		} 

		else if ($kvist > 1) {
			redirect ( 'home/kvist' );
		} else {
			$this->model_ibf->husnummer ();
		}
	}
	public function h_hus() {
		$this->model_ibf->deleteberegn ();
		$gather ['tagunder'] = $this->model_ibf->checkPost ( 'under', false );
		$gather ['facon'] = $this->model_ibf->checkPost ( 'valg_af', false );
		$gather ['hb_t'] = $this->model_ibf->checkPost ( 'hvhus2_b2', true );
		$gather ['hv_t'] = $this->model_ibf->checkPost ( 'hvhus2_v2', false );
		$gather ['hl_t'] = $this->model_ibf->checkPost ( 'hvhus2_l2', true );
		$gather ['hl_v'] = $this->model_ibf->checkPost ( 'hvhus2_l3', true );
		$gather ['hb_v'] = $this->model_ibf->checkPost ( 'hvhus2_b3', true );
		$gather ['hv_V'] = $this->model_ibf->checkPost ( 'hvhus2_v3', false );
		$gather ['hl'] = $this->model_ibf->checkPost ( 'hvhus2_l', true );
		$gather ['hb'] = $this->model_ibf->checkPost ( 'hvhus2_b', true );
		$gather ['hv'] = $this->model_ibf->checkPost ( 'hvhus2_v', false );
		
		foreach ( $gather as $var => $value ) {
			if ($value !== 'nope') {
				$sesset [$var] = $value;
			}
		}
		$this->session->set_userdata ( $sesset );
		// redirect
		$dynValmType = ( array ) json_decode ( $this->session->userdata ( 'dynValmType' ) );
		$globalAntalValgfrie = $this->session->userdata ( 'globalAntalValgfrie' );
		$kvist = $this->session->userdata ( 'kvist' );
		$tag = $this->session->userdata ( 'tag' );
		$helValm = 0;
		$halvValm = 0;
		for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
			if ($dynValmType [$i] == 2) {
				$helValm = true;
			} else if ($dynValmType [$i] == 3) {
				$halvValm = true;
			}
		}
		if ($helValm == true) {
			$globalValmTypeMaalsaet = 2;
			$this->session->set_userdata ( array (
					'globalValmTypeMaalsaet' => $globalValmTypeMaalsaet 
			) );
			redirect ( 'home/maalsaet' );
		} else if ($halvValm == true) {
			$globalValmTypeMaalsaet = 3;
			$this->session->set_userdata ( array (
					'globalValmTypeMaalsaet' => $globalValmTypeMaalsaet 
			) );
			redirect ( 'home/maalsaet' );
		} 

		else if ($kvist > 1) {
			redirect ( 'home/kvist' );
		} else {
			$this->model_ibf->husnummer ();
		}
	}
	public function tagmetode() {
		if ($this->input->post ( 'metodevalg' ) == 1) {
			$metode = 3;
		} else {
			$metode = 2;
		}
		$sesset ['metode'] = $metode;
		$this->session->set_userdata ( $sesset );
		$this->model_ibf->husnummer ( 1 );
	}
	public function maalsaet() {
		$dynValmType = ( array ) json_decode ( $this->session->userdata ( 'dynValmType' ) );
		$dynValmVinkel = ( array ) json_decode ( $this->session->userdata ( 'dynValmVinkel' ) );
		$valmId = json_decode ( $_POST ['valmId'], true );
		
		for($i = 1; $i <= 6; $i ++) {
			if (isset ( $_POST ['txtValmVinkel'] [$i] )) {
				$tmpId = $valmId [$i];
				$dynValmVinkel [$tmpId] = $_POST ['txtValmVinkel'] [$i];
			}
			if (isset ( $_POST ['txtValmTop'] [$i] )) {
				$tmpId = $valmId [$i];
				$dynValmTop [$tmpId] = $_POST ['txtValmTop'] [$i];
			}
		}
		if (isset ( $dynValmVinkel )) {
			$jsonVinkel = json_encode ( $dynValmVinkel );
			$this->session->set_userdata ( array (
					'dynValmVinkel' => $jsonVinkel 
			) );
		}
		if (isset ( $dynValmTop )) {
			$jsonTop = json_encode ( $dynValmTop ); 
			$this->session->set_userdata ( array (
					'dynValmTop' => $jsonTop 
			) );
		}
		$data ['globalValmTypeMaalsaet'] = $this->session->userdata ( 'globalValmTypeMaalsaet' );
		$globalAntalValgfrie = $this->session->userdata ( 'globalAntalValgfrie' );
		$dynValmType = ( array ) json_decode ( $this->session->userdata ( 'dynValmType' ) );
		if ($data ['globalValmTypeMaalsaet'] == 2) {
			for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
				if ($dynValmType [$i] == 3) {
					$this->session->set_userdata ( array (
							'globalValmTypeMaalsaet' => 3 
					) );
					redirect ( 'home/maalsaet' );
				}
			}
		}
		$kvist = $this->session->userdata ( 'kvist' );
		if ($kvist > 1) {
			redirect ( 'home/kvist' ); 
		}
		$this->model_ibf->husnummer ();
	}
	public function kvist() {
		$khl = (float)str_replace ( ",", ".",$this->input->post ( 'kvistl' ));
		$khb = (float)str_replace ( ",", ".",$this->input->post ( 'kvistb' ));
		$khv = $this->input->post ( 'kvistv' );
		$khl_backup = $khl;
		$sesset = array (
				'khl' => $khl*1000,
				'khb' => $khb*1000,
				'khv' => $khv 
		);
		
		if ($this->input->post ( 'kvistv_v' )) {
			$khvv = $this->input->post ( 'kvistv_v' );
			$sesset ['khvv'] = $khvv;
		}
		if ($this->input->post ( 'kvistv_b' )) {
			$khvb = $this->input->post ( 'kvistv_b' );
			$sesset ['khvb'] = $khvb;
		}
		// add gathered data to session
		$this->session->set_userdata ( $sesset );
		
		$this->model_ibf->husnummer ();
	}
	public function met2q() {
		$option = $this->input->post ( 'option1' );
		$fra_til = ( array ) json_decode ( $this->input->post ( 'fra_til' ) );
		$metq3_opt1_value = $this->input->post ( 'metq3_opt1_value' );
		$metq3_opt2_value = $this->input->post ( 'metq3_opt2_value' );
		$halve = $this->session->userdata ( 'halve' );
		$tilfoej_halv_fjern_hel = false;
		
		if ($option == 0) {
			if ($metq3_opt1_value == 2) {
				$resmet = $fra_til [2];
			}
			
			if ($metq3_opt1_value == 3) {
				$resmet = $fra_til [3];
				$tilfoej_halv_fjern_hel = true;
				$halve = true;
			}
			
			if ($metq3_opt1_value == 4) {
				$resmet = $fra_til [4];
				$halve = true;
			}
			
			if ($metq3_opt1_value == 5) {
				$resmet = $fra_til [5];
			}
		}
		
		if ($option == 1) {
			if ($metq3_opt2_value == 2) {
				$resmet = $fra_til [2];
			}
			
			if ($metq3_opt2_value == 3) {
				$resmet = $fra_til [3];
				$tilfoej_halv_fjern_hel = true;
				$halve = true;
			}
			
			if ($metq3_opt2_value == 4) {
				$resmet = $fra_til [4];
				$halve = true;
			}
			
			if ($metq3_opt2_value == 5) {
				$resmet = $fra_til [5];
			}
		}
		$sess = $this->session->all_userdata ();
		foreach ( $sess as $key => $value ) {
			if (substr ( $key, 0, 4 ) == 'tmp_') {
				${substr ( $key, 4 )} = $value;
			}
		}
		$knast_nr = ( int ) $this->session->userdata ( 'tmp_knast_nr' );
		$resmet = $resmet * - 1;
		if ($knast_nr == 1) {
			$sesset = array (
					'1_tilfoej_halv_fjern_hel' => $tilfoej_halv_fjern_hel,
					'1_halve' => $halve,
					'1_resmet' => $resmet 
			);
		} else if ($knast_nr == 2) {
			$sesset = array (
					'2_tilfoej_halv_fjern_hel' => $tilfoej_halv_fjern_hel,
					'2_halve' => $halve,
					'2_resmet' => $resmet 
			);
		} else if ($knast_nr == 3) {
			$sesset = array (
					'3_tilfoej_halv_fjern_hel' => $tilfoej_halv_fjern_hel,
					'3_halve' => $halve,
					'3_resmet' => $resmet 
			);
		} else {
			$sesset = array (
					'tilfoej_halv_fjern_hel' => $tilfoej_halv_fjern_hel,
					'halve' => $halve,
					'resmet' => $resmet 
			);
		}
		$this->session->set_userdata ( $sesset );
		// call husnummer, parameters { first -> skips tagmetode, second -> skips tagopbygning
		$knast_rpt = $this->session->userdata ( 'knast_rpt' );
		$antalKnaster = ( int ) $this->session->userdata ( 'antalKnaster' );
		if ($knast_rpt != '1') {
			$this->model_ibf->husnummer ( 1, 1 );
		} else {
			
			if ($mtcnt == '1') {
				$this->model_ibf->husnummer ( 1, 1 );
			} else if ($mtcnt == 'k1') {
				if ($antalKnaster == 2) {
					$this->model_ibf->husnummer ( 1, 1, '10' );
				} else {
					$this->model_ibf->husnummer ( 1, 1, '11' );
				}
			} else if ($mtcnt == 'k2') {
				$this->model_ibf->husnummer ( 1, 1, '11' );
			}
		}
		// delete temporary session data
		// $sess = $this->session->all_userdata ();
		// foreach ( $sess as $key => $value ) {
		// if (substr ( $key, 0, 4 ) == 'tmp_') {
		// $this->session->unset_userdata ( $key );
		// }
		// }
	}
	public function met3q() {
		$option = $this->input->post ( 'option1' );
		$fra_til = ( array ) json_decode ( $this->input->post ( 'fra_til' ) );
		$metq3_opt1_value = $this->input->post ( 'metq3_opt1_value' );
		$metq3_opt2_value = $this->input->post ( 'metq3_opt2_value' );
		$halve = $this->session->userdata ( 'halve' );
		$tilfoej_halv_fjern_hel = false;
		
		if ($option == 0) {
			if ($metq3_opt1_value == 2) {
				$resmet = $fra_til [2];
			}
			
			if ($metq3_opt1_value == 3) {
				$resmet = $fra_til [3];
				$tilfoej_halv_fjern_hel = true;
				$halve = true;
			}
			
			if ($metq3_opt1_value == 4) {
				$resmet = $fra_til [4];
				$halve = true;
			}
			
			if ($metq3_opt1_value == 5) {
				$resmet = $fra_til [5];
			}
		}
		
		if ($option == 1) {
			if ($metq3_opt2_value == 2) {
				$resmet = $fra_til [2];
			}
			
			if ($metq3_opt2_value == 3) {
				$resmet = $fra_til [3];
				$tilfoej_halv_fjern_hel = true;
				$halve = true;
			}
			
			if ($metq3_opt2_value == 4) {
				$resmet = $fra_til [4];
				$halve = true;
			}
			
			if ($metq3_opt2_value == 5) {
				$resmet = $fra_til [5];
			}
		}
		$sess = $this->session->all_userdata ();
		foreach ( $sess as $key => $value ) {
			if (substr ( $key, 0, 4 ) == 'tmp_') {
				${substr ( $key, 4 )} = $value;
			}
		}
		$knast_nr = ( int ) $this->session->userdata ( 'tmp_knast_nr' );
		$resmet = $resmet * - 1;
		if ($knast_nr == 1) {
			$sesset = array (
					'1_tilfoej_halv_fjern_hel' => $tilfoej_halv_fjern_hel,
					'1_halve' => $halve,
					'1_resmet' => $resmet 
			);
		} else if ($knast_nr == 2) {
			$sesset = array (
					'2_tilfoej_halv_fjern_hel' => $tilfoej_halv_fjern_hel,
					'2_halve' => $halve,
					'2_resmet' => $resmet 
			);
		} else if ($knast_nr == 3) {
			$sesset = array (
					'3_tilfoej_halv_fjern_hel' => $tilfoej_halv_fjern_hel,
					'3_halve' => $halve,
					'3_resmet' => $resmet 
			);
		} else {
			$sesset = array (
					'tilfoej_halv_fjern_hel' => $tilfoej_halv_fjern_hel,
					'halve' => $halve,
					'resmet' => $resmet 
			);
		}
		$this->session->set_userdata ( $sesset );
		$knast_rpt = $this->session->userdata ( 'knast_rpt' );
		
		// // delete temporary session data
		// $sess = $this->session->all_userdata ();
		// foreach ( $sess as $key => $value ) {
		// if (substr ( $key, 0, 4 ) == 'tmp_') {
		// $this->session->unset_userdata ( $key );
		// }
		// }
		// call husnummer, parameters { first -> skips tagmetode, second -> skips tagopbygning, third skips knast metq
		$antalKnaster = ( int ) $this->session->userdata ( 'antalKnaster' );
		
		if ($knast_rpt != '1') {
			$this->model_ibf->husnummer ( 1, 1 );
		} else {
			
			if ($mtcnt == '1') {
				$this->model_ibf->husnummer ( 1, 1 );
			} else if ($mtcnt == 'k1') {
				if ($antalKnaster == 2) {
					$this->model_ibf->husnummer ( 1, 1, '10' );
				} else {
					$this->model_ibf->husnummer ( 1, 1, '11' );
				}
			} else if ($mtcnt == 'k2') {
				$this->model_ibf->husnummer ( 1, 1, '11' );
			}
		}
	}
}
