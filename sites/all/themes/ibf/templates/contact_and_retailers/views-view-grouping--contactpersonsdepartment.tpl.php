<?php

/**
 * @file
 * This template is used to print a single grouping in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $grouping: The grouping instruction.
 * - $grouping_level: Integer indicating the hierarchical level of the grouping.
 * - $rows: The rows contained in this grouping.
 * - $title: The title of this grouping.
 * - $content: The processed content output that will normally be used.
 */
?>


<div class="afdeling<?=$grouping_level?>">

  <div class="view-grouping">
  
    <?php if (empty($title)) : ?>
      <div class="afdeling<?=$grouping_level-1?>">
    <?php endif; ?>
 
    <?php if (!empty($title)) : ?>
      <div class="view-grouping-header"><h2 class="togglebar"><?php print $title; ?></h2></div>
	<?php endif; ?>

	<div class="view-grouping-content">
      <?php print $content; ?>
    </div>	
 	
    <?php if (empty($title)) : ?>
	  </div>
      <div class="afdeling<?=$grouping_level-1?>-slut"></div>
    <?php endif; ?>	
	
  </div>

</div>

<div class="afdeling<?=$grouping_level?>-slut">
</div>
