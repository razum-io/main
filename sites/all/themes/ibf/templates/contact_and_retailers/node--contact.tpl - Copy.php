<?php 
/**
* Selve kontakt siden med grundstrukturen
**/

?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print render($title_prefix); ?>
  
    <h1<?php print $title_attributes; ?> class="title"><?php print $title; ?></h1>
  
  <?php print render($title_suffix); ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
    <div id="map-canvas" style="width: 620px; height: 460px;"></div>
    
    <?php 
    $view = views_embed_view('retailers', 'default');
    echo $view;
    ?>
    
    <div class="gmap-controls infieldlabels">
      <div class="find-nearest" style="display:none;"><?php echo t("Find the closest"); ?> <span class="button black"><?php echo t("Find!"); ?></span></div>
      <div class="find-by-zip form-item">
      	<label for="find-by-zip-input"><?php echo t("Or search by postal code..."); ?></label>
        <input type="text" value="" class="form-text" name="find-by-zip-input" id="find-by-zip-input"/><a class="submit button black" href="javascript:;"><?php echo t("Search"); ?></a>
      </div>
      <h3><?php echo t("Choose from the lists"); ?></h3>
      
      <div id="retaz100">
		<div class="retailers">
			<div class="current-retailer"><?php echo t("Dealer list..."); ?></div>
			<ul class="retailer-list"></ul>
		</div>
      </div>
      
      <div id="deptz1">
			<div class="departments">
				<div class="current-department"><?php echo t("IBF Branches..."); ?></div>
				<ul class="department-list"></ul>
			</div>
		</div>
    </div>
    
    <?php 
      $view = views_embed_view('contactpersons', 'default');
      echo $view;
    ?>
    
    
  </div>

</div>