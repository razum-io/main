<?php
/**
* Bruges til Kontakt personerne nederst på siden med billede

 * @file views-view-table.tpl.php
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
?>

<div <?php if ($classes) { print 'class="'. $classes . '" '; } ?><?php print $attributes; ?>>
	
  <?php if (!empty($title)) : ?>
    <h2 class="togglebar"><?php print $title; ?></h2>
  <?php endif; ?>


  
  <div class="contentbody contactpersons">
    <?php foreach ($rows as $row_count => $row): ?>
	<div class="vcard">
        <div class="photo"><?php echo $row['field_employee_image']; ?></div>
        <div class="org"><?php echo $row['field_employee_department']; ?></div>
        <div><?php echo $row['field_areacovered']; ?></div>
        <div class="fn"><?php echo $row['field_employee_name']; ?></div>
        <div class="title"><?php echo $row['field_jobtitle']; ?></div>

		<?php if (!empty($row['field_mobile_phone'])) : ?>
			<div class="tel"><span class="type"><?php print t('Mobile:'); ?></span> <a href="tel:<?php echo $row['field_mobile_phone']; ?>"><?php echo $row['field_mobile_phone']; ?></a></div>
		<?php endif; ?>
		
		<?php if (!empty($row['field_office_phone'])) : ?>
			<div class="tel"><span class="type"><?php print t('Office:'); ?></span> <a href="tel:<?php echo $row['field_office_phone']; ?>"><?php echo $row['field_office_phone']; ?></a></div>
		<?php endif; ?>

		<?php
		/** 
		<div class="tel"><span class="type"><?php print t('Fax:'); ?></span> <a href="tel:<?php echo $row['field_fax']; ?>"><?php echo $row['field_fax']; ?></a></div> 
		**/
		?>
		
		<div class="email"><span class="type"><?php print t('Email:'); ?></span> <a href="mailto:<?php echo $row['field_email']; ?>" class="email"><?php echo $row['field_email']; ?></a></div>
	</div>
      <?php // print_r(array_keys($row));?>
    <?php endforeach; ?>
  </div>
  
</div>
 
