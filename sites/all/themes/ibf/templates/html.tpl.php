<?php

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://opengraphprotocol.org/schema/" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>

<head profile="<?php print $grddl_profile; ?>">
  <?php print $head; ?>
	<title><?php print $head_title; ?></title>
	<meta name="google-site-verification" content="KUSdU2G6P2TJHkhIvqHug2RnCZ12vP_oAf4173UZFzs" />
	<meta property="fb:app_id" content="319527764772307"/>
  <?php print $styles; ?>
  <?php print $scripts; ?>
	<!--[if !IE]>
	<style type="text/css">
		body {font-size:16px;}
	</style>
	<![endif]-->
<!--[if lt IE 9]>
<link type="text/css" rel="stylesheet" href="/sites/all/themes/ibf/ie.css?lwnmnr" media="all" />
<![endif]-->

<!-- Start of mailchimp -->
<?php 
//Henter URL (IBF.dk/da/side / IBF.dk/pl/side)
$sproglag = explode("/",$_SERVER['REQUEST_URI']);

//Tildeler sproglag /da/ /pl/ fra URL'en. 
$sproglag = $sproglag[1]; 

//Checker om sproglag er da. 
if($sproglag == 'da'){?>
	<script type="text/javascript" src="//s3.amazonaws.com/downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script>
	<script type="text/javascript">require(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us4.list-manage.com","uuid":"d7320011442bd44fa36155fa9","lid":"bed093ee83"}) })</script>
<?php 
} 
?>
<!-- End of mailchimp -->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '579437352235316');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=579437352235316&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!--
----------------------------------------------------------------------------------------------
Tracking Code: Standard (Synchronous) "Retargeting TP"
----------------------------------------------------------------------------------------------
-->
<!-- Adform Tracking Code BEGIN -->
<script type="text/javascript" src="https://track.adform.net/serving/scripts/trackpoint/"></script>
<script type="text/javascript">
    adf.Params.PageName = encodeURIComponent('IBF.dk|Retargeting TP');
    adf.Params.Divider = encodeURIComponent('|');
    adf.track(606705);
</script>
<noscript>
    <p style="margin:0;padding:0;border:0;">
        <img src="https://track.adform.net/Serving/TrackPoint/?pm=606705&ADFPageName=Retargeting%20TP&ADFdivider=|" width="1" height="1" alt="" />
    </p>
</noscript>
<!-- Adform Tracking Code END -->
<!--
**************Cookie**************
RETARGETING COOKIE
-->
<script type="text/javascript">
document.write('<scr'+'ipt language="javascript1.1" src=" http://adserver.adtech.de/bind?ckey1=ibf_retarg;cvalue1=ibf_retarg;expiresDays=90;adct=text/html;misc=123"></scri'+'pt>');
</script>

<!--
**********************************

Afledt / salg 
-->
<!-- Postclick Tracking Tag Start -->
<!--<img src="http://adserver.adtech.de/pcsale/3.0/858/0/0/0/BeaconId=28580;rettype=img;subnid=1;SalesValue=1;;custom1=[sale]" width="1" height"1">
<!-- Postclick Tracking Tag End -->

</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
	<div id="container-wrapper">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCav4Pos1tnCDhrpB9TDuUmVN7owoNqvm8&sensor=true" type="text/javascript"></script>
<div id="fb-root"></div>

<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/da_DK/all.js#xfbml=1&appId=319527764772307";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
	</div>
</body>
</html>
