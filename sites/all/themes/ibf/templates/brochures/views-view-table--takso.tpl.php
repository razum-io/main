<?php
/**
 * @file views-view-table.tpl.php
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
?>
<div <?php if ($classes) { print 'class="'. $classes . '" '; } ?><?php print $attributes; ?>>
  <?php if (!empty($title)) : ?>
    <h2 class="togglebar"><?php print $title; ?></h2>
  <?php endif; ?>

  <ul class="contentbody">
    
    <?php foreach ($rows as $row_count => $row): ?>
      <li class="pdflisting views-row <?php print implode(' ', $row_classes[$row_count]); ?>">
        <?php 
        //print_r(array_keys($row));
        $content = $row['title'];
        $field = 'title';
        ?>
        
        <div <?php if ($field_classes[$field][$row_count]) { print 'class="'. $field_classes[$field][$row_count] . '" '; } ?><?php print drupal_attributes($field_attributes[$field][$row_count]); ?>>
          <?php print $content; ?>
          <input type="hidden" value="<?php echo $row['nid']; ?>" class="nid" />
          <ul class="links">
            <li class="first"><?php if (isset($row['field_pdf_url']) && ($row['field_pdf_url'])!='') : ?><a href="<?php echo $row['field_pdf_url']; ?>" target="_blank"><?php echo t("Læs"); ?></a><?php endif; ?></li>
            <li class="pdf"><?php if (isset($row['field_pdf_url']) && ($row['field_pdf_url'])!='') : ?><a href="<?php echo $row['field_pdf_url']; ?>ViewPDF.ashx" target="_blank"><?php echo t("PDF"); ?></a></li><?php else: ?><a href="<?php echo $row['field_pdffile']; ?>" target="_blank"><?php echo t("PDF"); ?></a></li><?php endif; ?>
            <li class="last"><?php if ($row['field_ordernotpossible'] != 'nej')  : ?><a href="javascript:;" class="order"><?php echo t("Bestil"); ?></a><?php endif; ?></li>
          </ul>
        </div>
              
      </li>
    <?php endforeach; ?>
  </ul>
</div>
