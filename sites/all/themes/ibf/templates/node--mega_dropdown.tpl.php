<?php
  // We hide the comments and links now so that we can render them later.
  hide($content['field_column1']);
  hide($content['field_column2']);
  hide($content['field_column3']);
  hide($content['field_products']);
  hide($content['field_backgroundimage']);
  hide($content['field_link_toarticle']);
//  print_r($node);
?>
<div class="megabox" id="megadrop<?php print($node->nid); ?>">
<?php print render($title_prefix); ?>
	<div class="title">
		<div class="tabmenu" title="<?php print t('Vælg et menupunkt herunder'); ?>"><?php print $title; ?></div>
	</div>
	<?php print render($title_suffix); ?>

  <div class="megadrop" style="display:none; background-image:url('<?php if (isset($node->field_backgroundimage[LANGUAGE_NONE]['0']['uri'])) { print (file_create_url($node->field_backgroundimage[LANGUAGE_NONE]['0']['uri'])); }; ?>');">
  	<div class="collums1 collums">
	  <?php print render($content['field_column1']); ?>
	</div>
  	<div class="collums2 collums">
	  <?php print render($content['field_column2']); ?>
	</div>
  <?php if (isset($node->field_backgroundimage) && count($node->field_backgroundimage)> 0) : ?>
	<?php if ((isset($node->field_link_toarticle) && count($node->field_link_toarticle)> 0) or ($node->field_alt_link)!="") : ?><a href="<?php 
	if(render($content['field_alt_link'])==""){
	print render($content['field_link_toarticle']); 
	}
	else{
	print render($content['field_alt_link']);
	}
	?>" class="collums3 collums"></a><?php endif; ?>
	<?php else: ?>
  	<div class="collums3 collums">
	  <?php print render($content['field_column3']); ?>
	</div>
	<?php endif; ?>



  <?php if (isset($node->field_products) && count($node->field_products)> 0) : ?>
	<div class="relatedproducts">
		<h2><?php echo t("Popular IBF Products"); ?></h2>
		<div class="products"><?php print render($content['field_products']); ?></div>
		<div class="clear"></div>
	</div>
  <?php endif; ?>

    <div class="clear"></div>
  </div>
	


</div>
