(function ($) {
  $(document).ready(function(){
    
	var $checklevel2=0;
	var $checklevel1=0;
	var $checklevel0=0;
	
	$('#find-by-zip-input').keydown(function(e) {
    if (e.keyCode == 13) {
          var zipcode = $(this).parents(".find-by-zip").find("input:first").val().toLowerCase();
          retailer_view.find(".views-row").hide();
          retailer_view.find(".node").each(function(index, value) {
            var item_zipcode = $(this).find(".postal-code").text();
            if (item_zipcode == parseInt(zipcode) ) {
				$(this).parents(".views-row").show();
			}
			else
			{
				item_zipcode = $(this).find(".locality").text().toLowerCase();
				//alert(item_zipcode+" ShouldMatch "+zipcode);
				if (item_zipcode.match(zipcode) )  {
					//alert('Hej');
					$(this).parents(".views-row").show();
				}
            }
        });
	}
	});
	
    /*
     * googlemaps integration magic for contact page
     */
    if ($(".node-contact #map-canvas").length > 0) {
      var default_start = new google.maps.LatLng(56.212814, 10.667725); 
      if ($(".i18n-de").length > 0) default_start = new google.maps.LatLng(54.1581, 9.84396); 
      
      var gmap_markers = [];
      var gmap_options = {
        zoom: 7,
        center: default_start,
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      var gmap = new google.maps.Map(document.getElementById("map-canvas"), gmap_options);
      
      // contact persons 
      if ($(".view-id-contactpersons").length > 0) { 
        $(".view-id-contactpersons h2.togglebar").each(function() {
          $(this).parents("div.views-table:first").find(".contentbody").hide();
        });

        // $(".view-id-contactpersons .views-table:first h2.togglebar").addClass("active");
        // $(".view-id-contactpersons .views-table:first .contentbody").show();
        
        $(".view-id-contactpersons .views-table h2.togglebar").click(function() {
          
          $(".view-id-contactpersons .views-table .contentbody").hide();
          $(".view-id-contactpersons .views-table h2.togglebar").removeClass("active");
          
          $(this).parents("div.views-table:first").find(".contentbody").show();
          $(this).addClass("active");
        });
		} 

      // contact personsdepartment
      if ($(".view-id-contactpersonsdepartment").length > 0) {

		$("h2.togglebar").click(function() {
			//alert("start");
			
			if ($(this).hasClass("active")==false) {
				//Niveau 2 click
				if ($(this).parent().parent().parent().hasClass("afdeling2")) {
					// Luk alle niveau'er
					$("h2.togglebar").removeClass("active");
					$(".afdeling1").hide();
					$(".afdeling0").hide();
					$(".view-grouping-content").hide();
					$(".contactpersons").hide();
					
					$checklevel2=1;
					$checklevel1=0;
					$checklevel0=0;
					
					// Åben Niveau 2
					$(this).parent().parent().children(".view-grouping-content").show();
					$(this).parent().parent().children(".view-grouping-content").children(".afdeling1").show();
					$(this).parent().parent().children(".view-grouping-content").children(".afdeling1").children(".view-grouping").children(".afdeling0").show();
					$(this).parent().parent().children(".view-grouping-content").children(".afdeling1").children(".view-grouping").children(".afdeling0").children(".view-grouping-content").show();
					$(this).parent().parent().children(".view-grouping-content").children().children("h2.togglebar").addClass("active");
					$(this).parent().parent().children(".view-grouping-content").children(".afdeling1").children(".view-grouping").children(".view-grouping-content").hide();
				}	
				
				//if ($checklevel1==0) {
					if ($(this).parent().parent().parent().hasClass("afdeling1")) {
						// Åben Niveau 1
						$checklevel1=1;
						$(this).parent().parent().children(".view-grouping-content").show();
						
						// Luk Niveau 0
						$checklevel0=0;
						$(this).parent().parent().parent().parent().children(".afdeling1").children(".view-grouping").children(".afdeling0").children(".view-grouping-header").children().children("h2.togglebar").removeClass("active");
						$(this).parent().parent().parent().parent().children(".afdeling1").children(".view-grouping").children(".afdeling0").children(".view-grouping-header").children().children(".contactpersons").hide();
						$(this).parent().parent().parent().parent().children(".afdeling1").children(".view-grouping").children(".afdeling0").children(".view-grouping-content").children().children("h2.togglebar").removeClass("active");
						$(this).parent().parent().parent().parent().children(".afdeling1").children(".view-grouping").children(".afdeling0").children(".view-grouping-content").children().children(".contactpersons").hide();
					}				
				//}

					if ($(this).parent().parent().parent().parent().hasClass("afdeling1")) {
						// Åben Niveau 1
						$checklevel1=1;
						$(this).parent().parent().children(".view-grouping-content").show();

						$(this).parent().next().children(".contactpersons").hide();
						$(this).parent().next().children("h2.togglebar").removeClass("active");
						$(this).parent().next().next().children(".contactpersons").hide();
						$(this).parent().next().next().children("h2.togglebar").removeClass("active");
						$(this).parent().next().next().next().children(".contactpersons").hide();
						$(this).parent().next().next().next().children("h2.togglebar").removeClass("active");
						
						$(this).parent().prev().children(".contactpersons").hide();
						$(this).parent().prev().children("h2.togglebar").removeClass("active");
						$(this).parent().prev().prev().children(".contactpersons").hide();
						$(this).parent().prev().prev().children("h2.togglebar").removeClass("active");
						$(this).parent().prev().prev().prev().children(".contactpersons").hide();
						$(this).parent().prev().prev().prev().children("h2.togglebar").removeClass("active");
						
						$checklevel0=1;
						$checklevel1=0;
						$(this).parent().children(".contactpersons").show();			
					}				
				
				
				
				//if ($checklevel0==0) {
					if ($(this).parent().parent().parent().hasClass("afdeling0")) {
						// Åben Niveau 0
						$(this).parent().next().children(".contactpersons").hide();
						$(this).parent().next().children("h2.togglebar").removeClass("active");
						
						$(this).parent().prev().children(".contactpersons").hide();
						$(this).parent().prev().children("h2.togglebar").removeClass("active");
						
						$checklevel0=1;
						$checklevel1=0;
						$(this).parent().children(".contactpersons").show();
						
						// Luk Niveau 1
						$(this).parent().parent().parent().parent().parent().parent().children(".afdeling1").children(".view-grouping").children(".view-grouping-content").children().children("h2.togglebar").removeClass("active");
						$(this).parent().parent().parent().parent().parent().parent().children(".afdeling1").children(".view-grouping").children(".view-grouping-content").children().children(".contactpersons").hide();
						$(this).parent().parent().parent().parent().parent().parent().next().children(".afdeling1").children(".view-grouping").children(".view-grouping-content").children().children("h2.togglebar").removeClass("active");
						$(this).parent().parent().parent().parent().parent().parent().next().children(".afdeling1").children(".view-grouping").children(".view-grouping-content").children().children(".contactpersons").hide();
						$(this).parent().parent().parent().parent().parent().parent().children(".afdeling1").children(".view-grouping").children(".view-grouping-header").children("h2.togglebar").removeClass("active");
						$(this).parent().parent().parent().parent().parent().parent().children(".afdeling1").children(".view-grouping").children(".view-grouping-content").hide();
					}				
				//}	

				$(this).addClass("active");
				$('html,body').animate({
					scrollTop: $(this).offset().top},
					'slow');
				return false;
			}
			else
			{
				$(this).removeClass("active");
				$(this).next().hide();
				$(this).parent().next().hide();
			}		
		})
		
		

		}

		
      // retailers list
      if ($(".node-contact .view-retailers").length > 0) {
        var retailer_list = $(".node-contact .retailer-list");
        var department_list = $(".node-contact .department-list");
        retailer_list.hide();
        department_list.hide();
        var retailer_view = $(".node-contact .view-retailers");
        retailer_view.find(".views-row").hide();
        var retailer,lat,lon,pos,item_list;
        
        // populate retailer and department select list
        $(".node-contact .view-retailers .views-row .node").each(function(index, value) {
          retailer = "<li><a href='javascript:;' class='"+$(this).attr("id")+"'>"+$(this).find(".title").text()+"</a></li>";
          if ($(this).hasClass("node-retailer")) {
            retailer_list.append(retailer);
          }
          else {
            department_list.append(retailer);
          }
          
          lat = $(this).attr("data-lat");
          lon = $(this).attr("data-lon");
          pos = new google.maps.LatLng(lat, lon);
          addMarker(pos, $(this).attr("id"));
          
        });
        
        // open retailer select list
        $(".node-contact .current-retailer").live("click", function() {
          if(retailer_list.is(":visible")) {
            retailer_list.hide();
          }
          else {
            retailer_list.show();
          }
        });
        
        // open department select list
        $(".node-contact .current-department").live("click", function() {
          if(department_list.is(":visible")) {
            department_list.hide();
          }
          else {
            department_list.show();
          }
        });
        
        // "show on map" button
        retailer_view.find(".node .show-on-map").live("click", function() {
          lat = $(this).parents(".node:first").attr("data-lat");
          lon = $(this).parents(".node:first").attr("data-lon");
          pos = new google.maps.LatLng(lat, lon);
          showOnMap(pos);
        });
          
        // click on retailer list item
        $(".node-contact .retailer-list a, .node-contact .department-list a").live("click", function() {
          selectRetailerNode($(".node-contact .view-retailers #"+$(this).attr("class")));
        });
        
        // find items by zipcode
        $(".node-contact .find-by-zip .submit").click(function() {
          var zipcode = $(this).parents(".find-by-zip").find("input:first").val().toLowerCase();
          retailer_view.find(".views-row").hide();
          retailer_view.find(".node").each(function(index, value) {
            var item_zipcode = $(this).find(".postal-code").text();
            if (item_zipcode == parseInt(zipcode) ) {
				$(this).parents(".views-row").show();
			}
			else
			{
				item_zipcode = $(this).find(".locality").text().toLowerCase();
				//alert(item_zipcode+" ShouldMatch "+zipcode);
				if (item_zipcode.match(zipcode) )  {
					//alert('Hej');
					$(this).parents(".views-row").show();
				}
            }
          });
        });
        
        if (navigator.geolocation != undefined) {
          navigator.geolocation.getCurrentPosition(locationInitHandler, noLocation, {"timeout": 10000});
          
          $(".gmap-controls .find-nearest").click(function() {
            //navigator.geolocation.getCurrentPosition(foundLocation, noLocation, {"timeout": 10000});
            var node = $("#map-canvas").attr("data-nearest-node");
            if (node != undefined && node != "") {
              selectRetailerNode(retailer_view.find("#"+node));
            }
            
          });
        }
        // do some mobile testing
        if(navigator.userAgent.match(/Android/i) ||
          navigator.userAgent.match(/webOS/i) ||
          navigator.userAgent.match(/iPhone/i) ||
          navigator.userAgent.match(/iPod/i) ||
          navigator.userAgent.match(/iPad/i) ||
          navigator.userAgent.match(/BlackBerry/)) {
          $(".node-contact #map-canvas").hide();
          
        }
        else {
          $(".node-contact .show-on-map").each(function() {
            // remove links on non-mobile devices
            $(this).text($(this).text());
          });
        }
        
      }
      
    } // google maps
    
    // Add a marker to the map and push to the array.
    function addMarker(location, node_id) {
      marker = new google.maps.Marker({
        "position": location,
        "map": gmap
      });
      google.maps.event.addListener(marker, 'click', function() {
        //$(".node-contact").remove()
        selectRetailerNode($("#"+node_id));
        gmap.setCenter(location);
        gmap.setZoom(12);
      });
      
      gmap_markers.push(marker);
    }

    // Sets the map on all markers in the array.
    function setAllMap(map) {
      for (var i = 0; i < gmap_markers.length; i++) {
        gmap_markers[i].setMap(map);
      }
    }

    // Removes the overlays from the map, but keeps them in the array.
    function clearOverlays() {
      setAllMap(null);
    }
    
    function showOnMap(pos) {
      gmap.setCenter(pos);
      gmap.setZoom(12);
    }
    
    function selectRetailerNode(node) {
      var views_row = node.parents(".views-row:first");
      var retailer_list = node.parents(".node-contact:first").find(".retailer-list");
      var department_list = node.parents(".node-contact:first").find(".department-list");
      var retailer_view = node.parents(".node-contact:first").find(".view-retailers");
      
      // show select item
      retailer_view.find(".views-row").hide();
      views_row.show();
      
      // set map position
      /*lat = views_row.find(".node").attr("data-lat");
      lon = views_row.find(".node").attr("data-lon");
      pos = new google.maps.LatLng(lat, lon);
      gmap.setCenter(pos);
      gmap.setZoom(10);
      */
      // set marker
      //addMarker(pos);
      
      // set current retailer text
      if (node.hasClass("node-retailer")) {
        node.parents(".node-contact:first").find(".current-retailer").text(node.find(".title:first").text());
      }
      else {
        node.parents(".node-contact:first").find(".current-department").text(node.find(".title:first").text());
      }
            
      // hide the list
      department_list.hide();
      retailer_list.hide();
    }
    
    function locationInitHandler(position) {
      var lat,lon,dist,loc,nid;
      var current_lat = position.coords.latitude;
      var current_lon = position.coords.longitude;
      var current_loc = new LatLon(current_lat, current_lon);
      var view = $(".node-contact .view-retailers");
      var map_canvas = $(".node-contact #map-canvas");
      map_canvas.attr("data-nearest-dist", "");
      map_canvas.attr("data-nearest-lat", "");
      map_canvas.attr("data-nearest-lon", "");
      
      // enable find nearest button
      $(".gmap-controls .find-nearest").show();
      
      // zoom to current position
      var my_pos = new google.maps.LatLng(current_lat, current_lon);  
      gmap.setCenter(my_pos);
      gmap.setZoom(12);
      
      view.find(".views-row .node").each(function(index, value) {
        lat = $(this).attr("data-lat");
        lon = $(this).attr("data-lon");
        nid = $(this).attr("id");
        if (lat != undefined && lon != undefined) {
          loc = new LatLon(lat,lon);
          dist = current_loc.distanceTo(loc);
          $(this).attr("data-dist", dist);
          // check if we should update data attributes on #map-canvas
          if (map_canvas.attr("data-nearest-dist") == "" || (map_canvas.attr("data-nearest-dist") != "" && (parseFloat(dist) < parseFloat(map_canvas.attr("data-nearest-dist"))))) {
            map_canvas.attr("data-nearest-dist", dist);
            map_canvas.attr("data-nearest-lat", lat);
            map_canvas.attr("data-nearest-lon", lon);
            map_canvas.attr("data-nearest-node", nid);
          }
        }
      });
      
    }
    /*
    function foundLocation(position) {
      var lat = position.coords.latitude;
      var lon = position.coords.longitude;
      
      alert("lat" + lat);
      
    }*/
    
    function noLocation() {
      $(".gmap-controls .find-nearest").hide();
    }
    
    
    
  }); // document ready end

})(jQuery);