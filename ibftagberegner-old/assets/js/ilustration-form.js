var LineArx = [ 0, 0 ];
var LineAry = [ 0, 0 ];
var p2 = 0.017453292;
var p1 = 3.141592654;
var gemX1;
var gemX2;
var gemY1;
var gemY2;
var gemXbogstav;
var gemYbogstav;
var vink45x1;
var vink45y1;
var vink45x2;
var vink45y2;
var vink45a;
var vink45b;
var vinkel45Grader;
var bredde;
var hoejde;

var strRygning;
var sltRygning;
var strRygningY;
var sltRygningY;
var midtRygning;
var vinkelHoejde;
var wrkFltNum;
var tegnStregen;
var tmpTilbygning;
var hoejesteY;
var tegnKasser;
var calcVariabel3;

var tempVink45RygA;
var tempVink45RygB;
var tempVink45RygC;

var tmpY;
var tmpX;

function tegnHuset(tegn2D, tegn3D, tegnVinkler, TegnMaal, filnavn, hustype,
		laengehus) {

	// if (kaldFraUdskrift == true) {
	// tegnKasser = true;
	// kaldFraUdskrift = false;
	//
	// }

	hoejesteY = 0;

	tmpTilbygning = new tilbygning;

	if (hustype == 3) {

		if (tegn2D == true && tegn3D == true) {
			if (TegnMaal == true) {
				drawimage(
						"http://augsa.lv/ibf/assets/css/images/genererilustration/imgMaalsaes3.bmp",
						0, 0);
			} else {

				drawimage(
						"http://augsa.lv/ibf/assets/css/images/genererilustration/img3.bmp",
						0, 0);
			}
		} else {
			if (tegn2D == true && TegnMaal == true) {

				drawimage(
						"http://augsa.lv/ibf/assets/css/images/genererilustration/img3Med.bmp",
						0, 0);
			}
			if (tegn3D == true && TegnMaal == false) {

				drawimage(
						"http://augsa.lv/ibf/assets/css/images/genererilustration/img3Uden.bmp",
						0, 0);
			}
		}

		tegn2D = false;
		tegn3D = false;
	}
	if (hustype == 4) {
		if (tegn2D == true && tegn3D == true) {
			if (TegnMaal == true) {

				drawimage(
						"http://augsa.lv/ibf/assets/css/images/genererilustration/imgMaalsaes4.bmp",
						0, 0);
			} else {

				drawimage(
						"http://augsa.lv/ibf/assets/css/images/genererilustration/img4.bmp",
						0, 0);
			}
		} else {
			if (tegn2D == true && TegnMaal == true) {

				drawimage(
						"http://augsa.lv/ibf/assets/css/images/genererilustration/img4Med.bmp",
						0, 0);
			}
			if (tegn3D == true && TegnMaal == false) {

				drawimage(
						"http://augsa.lv/ibf/assets/css/images/genererilustration/img4Uden.bmp",
						0, 0);
			}
		}
		tegn2D = false;
		tegn3D = false;
	}
	if (tegn2D == true) {
		TegnBygning(tmpTilbygning, laengehus, 0, 0, 0, hoejesteY, TegnMaal);
	} else {
		hoejesteY = 23;
	}

	if (hoved == 50000) {
		hoejesteY = 59;
	}

	if (tegn3D == true) {
		// TegnBygning3D(tmpTilbygning, laengehus, 0, hoejesteY, 0, 0, 0, 0, 0,
		// hustype, tegnVinkler);
	}
}
function TegnBygning(tilbBeskrivelse, oprindeligBygning, strPosX, strPosY,
		sidePlacering, hoejesteY, maalsaetning) {

	// sidePlacering:
	// /// 1
	// // ____
	// 2 |////| 3
	// //|____|
	// ////4
	// Hvis side Placering 0 så er der det fųrste kald af sub-rutinen
	// oprindeligBygning indeholder ved dette kald den bygning der skal tegnes.
	//

	bredde = oprindeligBygning.bredde;
	hoejde = oprindeligBygning.hoejde;

	if (sidePlacering !== 0) {
		tmpBygning = tilbBeskrivelse.bygning;
	} else {
		tmpBygning = oprindeligBygning;
	}

	if (sidePlacering !== 0) {
		// Ved fųrste kald er der ingen tilbygning, og X/Y er udfyldt
		if (sidePlacering == 1 || sidePlacering == 4) {
			switch (tilbBeskrivelse.placering) {
			case 0: // Venstre side
				tmpBygning.x = strPosX;
				break;
			case 1: // Til venstre for midten
				tmpBygning.x = strPosX + (tmpBygning.bredde / 2);
				break;
			case 2: // Midten
				tmpBygning.x = strPosX + (bredde / 2) - (tmpBygning.bredde / 2);
				break;
			case 3: // Til hųjre for midten
				tmpBygning.x = strPosX + bredde - (tmpBygning.bredde * 1.5);
				break;
			case 4: // Hųjre
				tmpBygning.x = strPosX + bredde - tmpBygning.bredde;
				break;
			}
		} else {
			switch (tilbBeskrivelse.placering) {
			case 0: // Venstre side
				tmpBygning.y = strPosY;
				break;
			case 1: // Til venstre for midten
				tmpBygning.y = strPosY + (tmpBygning.hoejde / 2);
				break;
			case 2: // Midten
				tmpBygning.y = strPosY + (hoejde / 2) - (tmpBygning.hoejde / 2);
				break;
			case 3: // Til hųjre for midten
				tmpBygning.y = strPosY + hoejde - (tmpBygning.hoejde * 1.5);
				break;
			case 4: // Hųjre
				tmpBygning.y = strPosY + hoejde - tmpBygning.hoejde;
				break;
			}
		}
	}

	// ' Fųrst tegnes den sorte ramme om huset
	// '--------------------------------------

	// CurColor = RGB(0, 0, 0)
	var TxtHeight;
	var TxtWidth;
	switch (sidePlacering) {
	case 0:

		// Side 1:
		if (tmpBygning.side1KnastAktiv == true) {

			tmpBygning.y = tmpBygning.y + (hoejde * 0.3);

			retVal = DrawLineF(tmpBygning.x, tmpBygning.y, tmpBygning.x,
					(tmpBygning.y - (tmpBygning.hoejde * 0.3)));
			retVal = DrawLineF(tmpBygning.x,
					(tmpBygning.y - (tmpBygning.hoejde * 0.3)),
					(tmpBygning.x + (tmpBygning.bredde * 0.3)),
					(tmpBygning.y - (tmpBygning.hoejde * 0.3)));
			retVal = DrawLineF((tmpBygning.x + (tmpBygning.bredde * 0.3)),
					(tmpBygning.y - (tmpBygning.hoejde * 0.3)),
					(tmpBygning.x + (tmpBygning.bredde * 0.3)), tmpBygning.y);
			retVal = DrawLineF((tmpBygning.x + (tmpBygning.bredde * 0.3)),
					tmpBygning.y, tmpBygning.x + tmpBygning.bredde,
					tmpBygning.y);

			if (tmpBygning.side1KnastSide3Bogstav !== ""
					&& maalsaetning == true) {
				TxtHeight = TextHeight(tmpBygning.side1KnastSide3Bogstav);
				CurrentX = tmpBygning.x + (tmpBygning.bredde * 0.3) + 8;
				CurrentY = tmpBygning.y - ((tmpBygning.hoejde * 0.3) / 2)
						- (TxtHeight / 2) - 2;
				Printtext(tmpBygning.side1KnastSide3Bogstav, CurrentX, CurrentY);

				retVal = DrawLineF(
						tmpBygning.x + (tmpBygning.bredde * 0.3) + 5,
						tmpBygning.y, tmpBygning.x + (tmpBygning.bredde * 0.3)
								+ 5, tmpBygning.y - (tmpBygning.hoejde * 0.3));
				retVal = DrawLineF(
						tmpBygning.x + (tmpBygning.bredde * 0.3) + 7,
						tmpBygning.y, tmpBygning.x + (tmpBygning.bredde * 0.3)
								+ 2, tmpBygning.y);
				retVal = DrawLineF(
						tmpBygning.x + (tmpBygning.bredde * 0.3) + 7,
						tmpBygning.y - (tmpBygning.hoejde * 0.3), tmpBygning.x
								+ (tmpBygning.bredde * 0.3) + 2, tmpBygning.y
								- (tmpBygning.hoejde * 0.3));

			}

			if (tmpBygning.side1KnastSide1Bogstav !== ""
					&& maalsaetning == true) {

				TxtWidth = TextWidth(tmpBygning.side1KnastSide1Bogstav);
				TxtHeight = TextHeight(tmpBygning.side1KnastSide1Bogstav);
				CurrentX = tmpBygning.x + ((tmpBygning.bredde * 0.3) / 2)
						- (TxtWidth / 2);
				CurrentY = tmpBygning.y - (tmpBygning.hoejde * 0.3) - TxtHeight
						- 3;
				Printtext(tmpBygning.side1KnastSide1Bogstav, CurrentX, CurrentY);

				retVal = DrawLineF(tmpBygning.x, tmpBygning.y
						- (tmpBygning.hoejde * 0.3) - 5,
						(tmpBygning.x + (tmpBygning.bredde * 0.3)),
						tmpBygning.y - (tmpBygning.hoejde * 0.3) - 5);
				retVal = DrawLineF(tmpBygning.x, tmpBygning.y
						- (tmpBygning.hoejde * 0.3) - 7, tmpBygning.x,
						tmpBygning.y - (tmpBygning.hoejde * 0.3) - 2);
				retVal = DrawLineF((tmpBygning.x + (tmpBygning.bredde * 0.3)),
						tmpBygning.y - (tmpBygning.hoejde * 0.3) - 7,
						(tmpBygning.x + (tmpBygning.bredde * 0.3)),
						tmpBygning.y - (tmpBygning.hoejde * 0.3) - 2);
			}

			if (tmpBygning.side1Bogstav !== "" && maalsaetning == true) {

				TxtWidth = TextWidth(tmpBygning.side1Bogstav);
				CurrentX = tmpBygning.x + (tmpBygning.bredde / 2)
						- (TxtWidth / 2);
				CurrentY = tmpBygning.y - (tmpBygning.hoejde * 0.3) - 20;
				Printtext(tmpBygning.side1Bogstav, CurrentX, CurrentY);

				retVal = DrawLineF(tmpBygning.x, tmpBygning.y
						- (tmpBygning.hoejde * 0.3) - 13 - 5,
						(tmpBygning.x + tmpBygning.bredde), tmpBygning.y
								- (tmpBygning.hoejde * 0.3) - 13 - 5);
				retVal = DrawLineF(tmpBygning.x, tmpBygning.y
						- (tmpBygning.hoejde * 0.3) - 13 - 7, tmpBygning.x,
						tmpBygning.y - (tmpBygning.hoejde * 0.3) - 13 - 2);
				retVal = DrawLineF((tmpBygning.x + tmpBygning.bredde),
						tmpBygning.y - (tmpBygning.hoejde * 0.3) - 13 - 7,
						(tmpBygning.x + tmpBygning.bredde), tmpBygning.y
								- (tmpBygning.hoejde * 0.3) - 13 - 2);
			}

		} else {

			retVal = DrawLineF(tmpBygning.x, tmpBygning.y,
					(tmpBygning.x + tmpBygning.bredde), tmpBygning.y);
			if (tmpBygning.side1Bogstav !== "" && maalsaetning == true) {

				TxtWidth = TextWidth(tmpBygning.side1Bogstav);
				CurrentX = tmpBygning.x + (tmpBygning.bredde / 2)
						- (TxtWidth / 2);
				CurrentY = tmpBygning.y - 20;
				Printtext(tmpBygning.side1Bogstav, CurrentX, CurrentY);

				retVal = DrawLineF(tmpBygning.x, tmpBygning.y - 5,
						(tmpBygning.x + tmpBygning.bredde), tmpBygning.y - 5);
				retVal = DrawLineF(tmpBygning.x, tmpBygning.y - 7,
						tmpBygning.x, tmpBygning.y - 2);
				retVal = DrawLineF((tmpBygning.x + tmpBygning.bredde),
						tmpBygning.y - 7, (tmpBygning.x + tmpBygning.bredde),
						tmpBygning.y - 2);
			}

		}
		// Side 2:
		retVal = DrawLineF(tmpBygning.x, tmpBygning.y, tmpBygning.x,
				(tmpBygning.y + tmpBygning.hoejde));
		if (tmpBygning.side2Bogstav !== "" && maalsaetning == true) {

			if (tmpBygning.side2TilbygningAktiv = true) {
				// tmpBygning.side2Tilbygning.bygning.hoejde

				TxtHeight = TextHeight(tmpBygning.side2Bogstav);
				CurrentX = tmpBygning.x - 14
						- tmpBygning.side2Tilbygning.bygning.hoejde - 20;
				CurrentY = tmpBygning.y + (tmpBygning.hoejde / 2)
						- (TxtHeight / 2);
				Printtext(tmpBygning.side2Bogstav, CurrentX, CurrentY);

				retVal = DrawLineF(tmpBygning.x - 5
						- tmpBygning.side2Tilbygning.bygning.hoejde - 20,
						tmpBygning.y, tmpBygning.x - 5
								- tmpBygning.side2Tilbygning.bygning.hoejde
								- 20, tmpBygning.y + tmpBygning.hoejde);
				retVal = DrawLineF(tmpBygning.x - 7
						- tmpBygning.side2Tilbygning.bygning.hoejde - 20,
						tmpBygning.y, tmpBygning.x - 2
								- tmpBygning.side2Tilbygning.bygning.hoejde
								- 20, tmpBygning.y);
				retVal = DrawLineF(tmpBygning.x - 7
						- tmpBygning.side2Tilbygning.bygning.hoejde - 20,
						tmpBygning.y + tmpBygning.hoejde, tmpBygning.x - 2
								- tmpBygning.side2Tilbygning.bygning.hoejde
								- 20, tmpBygning.y + tmpBygning.hoejde);

			} else {

				TxtHeight = TextHeight(tmpBygning.side2Bogstav);
				CurrentX = tmpBygning.x - 14;
				CurrentY = tmpBygning.y + (tmpBygning.hoejde / 2)
						- (TxtHeight / 2);

				Printtext(tmpBygning.side2Bogstav, CurrentX, CurrentY);

				retVal = DrawLineF(tmpBygning.x - 5, tmpBygning.y,
						tmpBygning.x - 5, tmpBygning.y + tmpBygning.hoejde);
				retVal = DrawLineF(tmpBygning.x - 7, tmpBygning.y,
						tmpBygning.x - 2, tmpBygning.y);
				retVal = DrawLineF(tmpBygning.x - 7, tmpBygning.y
						+ tmpBygning.hoejde, tmpBygning.x - 2, tmpBygning.y
						+ tmpBygning.hoejde);
			}
		}
		// -------------------------------
		// ----->>>> KNAST <<<------
		//
		// Vi tegner en evt. knast her!
		// -----------------------------
		if (tmpBygning.side4KnastAktiv == true) {

			retVal = DrawLineF(
					tmpBygning.x,
					(tmpBygning.y + tmpBygning.hoejde),
					tmpBygning.x,
					((tmpBygning.y + tmpBygning.hoejde) + (tmpBygning.hoejde * 0.3)));
			retVal = DrawLineF(
					tmpBygning.x,
					((tmpBygning.y + tmpBygning.hoejde) + (tmpBygning.hoejde * 0.3)),
					(tmpBygning.x + (tmpBygning.bredde * 0.3)),
					((tmpBygning.y + tmpBygning.hoejde) + (tmpBygning.hoejde * 0.3)));
			retVal = DrawLineF(
					(tmpBygning.x + (tmpBygning.bredde * 0.3)),
					((tmpBygning.y + tmpBygning.hoejde) + (tmpBygning.hoejde * 0.3)),
					(tmpBygning.x + (tmpBygning.bredde * 0.3)),
					(tmpBygning.y + tmpBygning.hoejde));

			retVal = DrawLineF((tmpBygning.x + (tmpBygning.bredde * 0.3)),
					(tmpBygning.y + tmpBygning.hoejde),
					(tmpBygning.x + tmpBygning.bredde),
					(tmpBygning.y + tmpBygning.hoejde));

			if (tmpBygning.side4KnastSide3Bogstav !== ""
					&& maalsaetning == true) {
				TxtHeight = TextHeight(tmpBygning.side4KnastSide3Bogstav);
				CurrentX = tmpBygning.x + (tmpBygning.bredde * 0.3) + 8;
				CurrentY = tmpBygning.y + tmpBygning.hoejde
						+ ((tmpBygning.hoejde * 0.3) / 2) - (TxtHeight / 2);

				Printtext(tmpBygning.side4KnastSide3Bogstav, CurrentX, CurrentY);

				retVal = DrawLineF(
						tmpBygning.x + (tmpBygning.bredde * 0.3) + 5,
						tmpBygning.y + tmpBygning.hoejde, tmpBygning.x
								+ (tmpBygning.bredde * 0.3) + 5, tmpBygning.y
								+ tmpBygning.hoejde + (tmpBygning.hoejde * 0.3));
				retVal = DrawLineF(
						tmpBygning.x + (tmpBygning.bredde * 0.3) + 7,
						tmpBygning.y + tmpBygning.hoejde, tmpBygning.x
								+ (tmpBygning.bredde * 0.3) + 2, tmpBygning.y
								+ tmpBygning.hoejde);
				retVal = DrawLineF(
						tmpBygning.x + (tmpBygning.bredde * 0.3) + 7,
						tmpBygning.y + tmpBygning.hoejde
								+ (tmpBygning.hoejde * 0.3), tmpBygning.x
								+ (tmpBygning.bredde * 0.3) + 2, tmpBygning.y
								+ tmpBygning.hoejde + (tmpBygning.hoejde * 0.3));

			}

			if (tmpBygning.side4KnastSide4Bogstav !== ""
					&& maalsaetning == true) {

				TxtWidth = TextWidth(tmpBygning.side4KnastSide4Bogstav);
				CurrentX = tmpBygning.x + ((tmpBygning.bredde * 0.3) / 2)
						- (TxtWidth / 2);
				CurrentY = tmpBygning.y + tmpBygning.hoejde
						+ (tmpBygning.hoejde * 0.3) + 2;
				Printtext(tmpBygning.side4KnastSide4Bogstav, CurrentX, CurrentY);

				retVal = DrawLineF(tmpBygning.x, tmpBygning.y
						+ tmpBygning.hoejde + (tmpBygning.hoejde * 0.3) + 5,
						(tmpBygning.x + (tmpBygning.bredde * 0.3)),
						tmpBygning.y + tmpBygning.hoejde
								+ (tmpBygning.hoejde * 0.3) + 5);
				retVal = DrawLineF(tmpBygning.x, tmpBygning.y
						+ tmpBygning.hoejde + (tmpBygning.hoejde * 0.3) + 7,
						tmpBygning.x, tmpBygning.y + tmpBygning.hoejde
								+ (tmpBygning.hoejde * 0.3) + 2);
				retVal = DrawLineF((tmpBygning.x + (tmpBygning.bredde * 0.3)),
						tmpBygning.y + tmpBygning.hoejde
								+ (tmpBygning.hoejde * 0.3) + 7,
						(tmpBygning.x + (tmpBygning.bredde * 0.3)),
						tmpBygning.y + tmpBygning.hoejde
								+ (tmpBygning.hoejde * 0.3) + 2);
			}

		} else {

			// Side 4:
			retVal = DrawLineF(tmpBygning.x,
					(tmpBygning.y + tmpBygning.hoejde),
					(tmpBygning.x + tmpBygning.bredde),
					(tmpBygning.y + tmpBygning.hoejde));

			if (tmpBygning.side4Bogstav !== "" && maalsaetning == true) {

				TxtWidth = TextWidth(tmpBygning.side4Bogstav);
				CurrentX = tmpBygning.x + (tmpBygning.bredde / 2)
						- (TxtWidth / 2);
				CurrentY = tmpBygning.y + tmpBygning.hoejde + 2;
				Printtext(tmpBygning.side4Bogstav, CurrentX, CurrentY);
				retVal = DrawLineF(tmpBygning.x, tmpBygning.y
						+ tmpBygning.hoejde + 5,
						(tmpBygning.x + tmpBygning.bredde), tmpBygning.y
								+ tmpBygning.hoejde + 5);
				retVal = DrawLineF(tmpBygning.x, tmpBygning.y
						+ tmpBygning.hoejde + 7, tmpBygning.x, tmpBygning.y
						+ tmpBygning.hoejde + 2);
				retVal = DrawLineF((tmpBygning.x + tmpBygning.bredde),
						tmpBygning.y + tmpBygning.hoejde + 7,
						(tmpBygning.x + tmpBygning.bredde), tmpBygning.y
								+ tmpBygning.hoejde + 2);
			}
		}
		// Side 3:
		retVal = DrawLineF((tmpBygning.x + tmpBygning.bredde), tmpBygning.y,
				(tmpBygning.x + tmpBygning.bredde),
				(tmpBygning.y + tmpBygning.hoejde));

		if (tmpBygning.side3Bogstav !== "" && maalsaetning == true) {
			TxtHeight = TextHeight(tmpBygning.side3Bogstav);
			CurrentX = tmpBygning.x + tmpBygning.bredde + 8;
			CurrentY = tmpBygning.y + (tmpBygning.hoejde / 2) - (TxtHeight / 2);

			Printtext(tmpBygning.side3Bogstav, CurrentX, CurrentY);

			retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde + 5,
					tmpBygning.y, tmpBygning.x + tmpBygning.bredde + 5,
					tmpBygning.y + tmpBygning.hoejde);
			retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde + 7,
					tmpBygning.y, tmpBygning.x + tmpBygning.bredde + 2,
					tmpBygning.y);
			retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde + 7,
					tmpBygning.y + tmpBygning.hoejde, tmpBygning.x
							+ tmpBygning.bredde + 2, tmpBygning.y
							+ tmpBygning.hoejde);

		}
		// --------------------------------------------------------------------------
		// ---->>>> Kvist <<<<----
		// --------------------------------------------------------------------------
		if (tmpBygning.kvistAktiv == true) {
			var kvistX;
			var kvistY;
			var kvistY1;
			var kvistY11;
			var kvistX1;
			var kvistX11;
			var kvistX22;
			var kvistY22;

			if (hoved == 60000 || antalTilbygninger == 2
					|| antalVinklerVinkel == 2) {

				kvistX = tmpBygning.x + 16.3 + 4.5; // ((.bredde / 5) * 2)
				if (hoved == 60000) {
					kvistY = tmpBygning.y + (tmpBygning.hoejde / 2) + 4.5;
				} else {
					kvistY = tmpBygning.y + (tmpBygning.hoejde / 3) + 4.5;

				}

				kvistX1 = kvistX - 16.3;
				kvistY1 = kvistY - 8.14;
				kvistY11 = kvistY + 8.14;
				retVal = DrawLineF(kvistX1, kvistY1, kvistX1, kvistY11);

				retVal = DrawLineF(kvistX1, kvistY1, kvistX1 + 6, kvistY1);
				retVal = DrawLineF(kvistX1, kvistY11, kvistX1 + 6, kvistY11);

				retVal = DrawLineF(kvistX1 + 6, kvistY11, kvistX, kvistY);

				retVal = DrawLineF(kvistX1 + 6, kvistY1, kvistX, kvistY);

				FFcolor = "#FF0000";

				retVal = ExtFloodFill(kvistX, kvistY1 - 6, FFcolor);

				if (tmpBygning.kvistType == 1) {
					retVal = DrawLineF(kvistX, kvistY, kvistX1, kvistY);

				}

				if (tmpBygning.kvistType == 2) {
					kvistX22 = kvistX - ((kvistX - kvistX1) / 3);

					retVal = DrawLineF(kvistX, kvistY, kvistX22, kvistY);

					retVal = DrawLineF(kvistX1, kvistY1, kvistX22, kvistY);
					retVal = DrawLineF(kvistX1, kvistY11, kvistX22, kvistY);
				}

				if (tmpBygning.kvistType == 3) {
					// Ikke muligt at vęlge ved Pyramidetag
					// with pyram kvist4 will never be active, form control
					// disables it

				}

			} else {
				kvistX = tmpBygning.x + ((tmpBygning.bredde / 5) * 2);
				kvistY = tmpBygning.y + (tmpBygning.hoejde / 2) + 4.5;

				kvistY1 = kvistY + 16.3;
				kvistX1 = kvistX - 8.14;
				kvistX11 = kvistX + 8.14;
				retVal = DrawLineF(kvistX1, kvistY1, kvistX11, kvistY1);

				retVal = DrawLineF(kvistX1, kvistY1, kvistX1, kvistY1 - 6);
				retVal = DrawLineF(kvistX11, kvistY1, kvistX11, kvistY1 - 6);

				retVal = DrawLineF(kvistX11, kvistY1 - 6, kvistX, kvistY);
				retVal = DrawLineF(kvistX1, kvistY1 - 6, kvistX, kvistY);

				FFcolor = "#FF0000";

				retVal = ExtFloodFill(kvistX, kvistY1 - 6, FFcolor);

				// Midter stregen:
				if (tmpBygning.kvistType == 1) {
					retVal = DrawLineF(kvistX, kvistY, kvistX, kvistY1);

				}

				if (tmpBygning.kvistType == 2) {
					kvistY22 = kvistY + ((kvistY1 - kvistY) / 3);
					retVal = DrawLineF(kvistX, kvistY, kvistX, kvistY22 + 1);

					retVal = DrawLineF(kvistX1, kvistY1, kvistX, kvistY22);
					retVal = DrawLineF(kvistX11, kvistY1, kvistX, kvistY22);
				}

				if (tmpBygning.kvistType == 3) {
					kvistY22 = kvistY + (((kvistY1 - kvistY) / 3.5) * 2.5);
					retVal = DrawLineF(kvistX, kvistY, kvistX, kvistY22 + 1);

					retVal = DrawLineF(kvistX1 + 4.44, kvistY1, kvistX,
							kvistY22);
					retVal = DrawLineF(kvistX11 - 4.44, kvistY1, kvistX,
							kvistY22);
				}
			}
		}
		hoejesteY = tmpBygning.y + tmpBygning.hoejde;
		break;
	case 1:
		hoejesteY = tmpBygning.y + tmpBygning.hoejde;
		tmpBygning.y = strPosY;
		retVal = DrawLineF(tmpBygning.x, tmpBygning.y, tmpBygning.x,
				tmpBygning.y - tmpBygning.hoejde);
		retVal = DrawLineF(tmpBygning.x, tmpBygning.y - tmpBygning.hoejde,
				tmpBygning.x + tmpBygning.bredde, tmpBygning.y
						- tmpBygning.hoejde);
		retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde, tmpBygning.y,
				tmpBygning.x + tmpBygning.bredde, tmpBygning.y
						- tmpBygning.hoejde);

		// Samling med &&en bygning!

		retVal = DrawLineF(LineARx[0], LineARy[0], LineARx[1], LineARy[1]);
		retVal = DrawLineF(tmpBygning.x + 1, tmpBygning.y,
				(tmpBygning.x + tmpBygning.bredde), tmpBygning.y);

		hoejesteY = hoejesteY + tmpBygning.hoejde;
		break;
	// other cases

	case 2:

		// Tjek om vi er ved at tegne et vinkelhus 45 rader her!
		// ----------------!!!!!!!!!!!!!!1
		// oprindeligbygning.
		if (hoved == 50000) {

			vinkel45Grader = (oprindeligBygning.hoejde / (Tan(p2 * 65)));
			vink45x1 = oprindeligBygning.x - vinkel45Grader;
			vink45y1 = oprindeligBygning.y;
			//        
			retVal = DrawLineF(oprindeligBygning.x, oprindeligBygning.y,
					vink45x1, vink45y1);

			retVal = DrawLineF(oprindeligBygning.x, oprindeligBygning.y + 1,
					oprindeligBygning.x, oprindeligBygning.y
							+ oprindeligBygning.hoejde, "#FF0000", 4);
			// 4 for safari
			retVal = DrawLineF(vink45x1, vink45y1, oprindeligBygning.x,
					oprindeligBygning.y + oprindeligBygning.hoejde);
			tmpBygning.hoejde = Sqr((vinkel45Grader * vinkel45Grader)
					+ (oprindeligBygning.hoejde * oprindeligBygning.hoejde));
			// oprindeligbygning.x

			vink45a = Sin(p2 * 45) * tmpBygning.bredde;
			vink45b = Cos(p2 * 45) * tmpBygning.bredde;
			// Nederste
			retVal = DrawLineF(oprindeligBygning.x,
					(oprindeligBygning.y + oprindeligBygning.hoejde),
					oprindeligBygning.x - vink45b,
					(oprindeligBygning.y + oprindeligBygning.hoejde) + vink45a);

			if (tmpBygning.side3Bogstav !== "" && maalsaetning == true) {
				TxtWidth = TextWidth(tmpBygning.side3Bogstav);
				CurrentX = oprindeligBygning.x
						- ((oprindeligBygning.x - (oprindeligBygning.x - vink45b)) / 2)
						+ TxtWidth;
				CurrentY = (oprindeligBygning.y + oprindeligBygning.hoejde)
						+ (((((oprindeligBygning.y + oprindeligBygning.hoejde) + vink45a)) - (oprindeligBygning.y + oprindeligBygning.hoejde)) / 2);
				Printtext(tmpBygning.side3Bogstav, CurrentX, CurrentY);
				retVal = DrawLineF(oprindeligBygning.x,
						(oprindeligBygning.y + oprindeligBygning.hoejde) + 5,
						oprindeligBygning.x - vink45b + 5,
						(oprindeligBygning.y + oprindeligBygning.hoejde)
								+ vink45a);
				// delete previous line
				retVal = DrawLineF(oprindeligBygning.x,
						(oprindeligBygning.y + oprindeligBygning.hoejde) + 4,
						oprindeligBygning.x,
						(oprindeligBygning.y + oprindeligBygning.hoejde) + 9);
				// draw new one so it doesnt overlay
				retVal = DrawLineF(oprindeligBygning.x - vink45b + 4,
						(oprindeligBygning.y + oprindeligBygning.hoejde)
								+ vink45a, oprindeligBygning.x - vink45b + 9,
						(oprindeligBygning.y + oprindeligBygning.hoejde)
								+ vink45a);
			}
			vink45x2 = (vink45x1 - vink45b)
					- (Cos(p2 * 45) * (tmpBygning.hoejde * Tan(p2 * 20)));
			vink45y2 = vink45y1 + vink45a
					+ (Sin(p2 * 45) * (tmpBygning.hoejde * Tan(p2 * 20)));

			if (tmpBygning.side4TilbygningAktiv == true) {

				if (tmpBygning.side4Bogstav !== "" && maalsaetning == true) {

					TxtWidth = TextWidth(tmpBygning.side4Bogstav);

					gemXbogstav = vink45x2
							+ ((oprindeligBygning.x - vink45b + 1 - vink45x2))
							- TxtWidth;
					gemYbogstav = vink45y2;

					gemX1 = vink45x2 + 4;
					gemX2 = oprindeligBygning.x - vink45b + 1 + 4;
					gemY1 = vink45y2 - 4;
					gemY2 = (oprindeligBygning.y + oprindeligBygning.hoejde)
							+ vink45a + 1 - 4;

					// retVal = DrawLineF(vink45x2 + 4, vink45y2 - 4,
					// oprindeligBygning.x - vink45b + 1 + 4,
					// (oprindeligBygning.y +
					// oprindeligBygning.hoejde) + vink45a + 1 - 4)
				}
				tmpBygning.side4Tilbygning.bygning = new tilbygning;
				tmpBygning.side4Tilbygning.bygning.bredde = (oprindeligBygning.x - vink45b)
						- vink45x2;
				// Der skal lidt mere hęldning på sammenkoblingen!
				var nyVinkelB;
				var nyVinkelA;
				var nyVinkelC;
				var calcVariabel1;
				var calcVariabel2;

				calcVariabel1 = (oprindeligBygning.x - vink45b) - vink45x2;
				calcVariabel2 = ((oprindeligBygning.y + oprindeligBygning.hoejde) + vink45a)
						- vink45y2;
				nyVinkelB = Sqr((calcVariabel1 * calcVariabel1)
						+ (calcVariabel2 * calcVariabel2));
				nyVinkelA = Tan(p2 * 20) * nyVinkelB;
				nyVinkelC = nyVinkelA;
				nyVinkelA = Sin(p2 * 45) * nyVinkelC;
				nyVinkelB = Cos(p2 * 45) * nyVinkelC;
				vink45x2 = vink45x2 - nyVinkelB;
				vink45y2 = vink45y2 + nyVinkelA;
				// tmpBygning.side4Tilbygning.bygning.hoejde = .hoejde +
				// nyVinkelA
				// tmpBygning.side4Tilbygning.bygning.bredde = .bredde +
				// nyVinkelB
				// Ųverste stykke
				// retVal = DrawLineF(vink45x1, vink45y1, vink45x1 - vink45b,
				// vink45y1 + vink45a)
				retVal = DrawLineF(vink45x1, vink45y1, vink45x2, vink45y2);
				// Gavl
				retVal = DrawLineF(vink45x2, vink45y2, oprindeligBygning.x
						- vink45b + 1,
						(oprindeligBygning.y + oprindeligBygning.hoejde)
								+ vink45a);

				// START SLUT RYGNING!!!

				nyVinkelA = (oprindeligBygning.x - vink45b) - vink45x2;
				nyVinkelB = ((oprindeligBygning.y + oprindeligBygning.hoejde) + vink45a)
						- vink45y2;

				nyVinkelC = (Sqr((nyVinkelA * nyVinkelA)
						+ (nyVinkelB * nyVinkelB))) / 2;

				// Svarende
				// til C

				// var tempVar ;
				// tempVar = (Cos(p2 * 65) * calcVariabel1) //b
				// tempVar = (Sin(p2 * 65) * calcVariabel1)
				// tempVar = .bredde

				sltRygning = (oprindeligBygning.x - vink45b + 1)
						- (Sin(p2 * 65) * nyVinkelC);
				sltRygningY = ((oprindeligBygning.y + oprindeligBygning.hoejde)
						+ vink45a + 2)
						- (Cos(p2 * 65) * nyVinkelC);
				tmpBygning.side4Tilbygning.bygning.bredde = oprindeligBygning.x
						- vink45b - vink45x2;
				// Tjek for side 4 bogstav!!!
				if (tmpBygning.side4Bogstav !== "" && maalsaetning == true) {

					FFcolor = "#FF0000";
					retVal = ExtFloodFill(gemXbogstav, gemYbogstav, FFcolor);

					CurrentX = gemXbogstav;
					CurrentY = gemYbogstav;
					Printtext(tmpBygning.side4Bogstav, CurrentX, CurrentY);

					retVal = DrawLineF(gemX1, gemY1, gemX2, gemY2);
				}
			} else {
				// Ųverste stykke
				// retVal = DrawLineF(vink45x1, vink45y1, vink45x1 -
				// vink45b, vink45y1 + vink45a)
				retVal = DrawLineF(vink45x1, vink45y1, vink45x2, vink45y2);
				// Gavl
				retVal = DrawLineF(vink45x2, vink45y2, oprindeligBygning.x
						- vink45b + 1,
						(oprindeligBygning.y + oprindeligBygning.hoejde)
								+ vink45a + 1);

				// Tjek for side 4 bogstav!!!
				if (tmpBygning.side4Bogstav !== "" && maalsaetning == true) {
					TxtWidth = TextWidth(tmpBygning.side4Bogstav);
					CurrentX = vink45x2
							+ ((oprindeligBygning.x - vink45b + 1 - vink45x2) / 2)
							- (TxtWidth * 2);
					CurrentY = vink45y2
							+ (((oprindeligBygning.y + oprindeligBygning.hoejde)
									+ vink45a + 1 - vink45y2) / 2);
					Printtext(tmpBygning.side4Bogstav, CurrentX, CurrentY);

					retVal = DrawLineF(vink45x2 - 4, vink45y2 + 4,
							oprindeligBygning.x - vink45b + 1 - 4,
							(oprindeligBygning.y + oprindeligBygning.hoejde)
									+ vink45a + 1 + 4);
					retVal = DrawLineF(vink45x2 - 2, vink45y2 + 2,
							vink45x2 - 7, vink45y2 + 7);
					retVal = DrawLineF(oprindeligBygning.x - vink45b + 1 - 2,
							(oprindeligBygning.y + oprindeligBygning.hoejde)
									+ vink45a + 1 + 2, oprindeligBygning.x
									- vink45b + 1 - 7,
							(oprindeligBygning.y + oprindeligBygning.hoejde)
									+ vink45a + 1 + 7);
				}

			}
			tmpBygning.x = vink45x2;
			// .y = vink45y2
		} else {

			tmpBygning.x = strPosX - tmpBygning.bredde;
			// Side 1:
			retVal = DrawLineF(tmpBygning.x, tmpBygning.y, tmpBygning.x
					+ tmpBygning.bredde, tmpBygning.y);
			if (tmpBygning.side1Bogstav !== "" && maalsaetning == true) {

				TxtWidth = TextWidth(tmpBygning.side1Bogstav);
				CurrentX = tmpBygning.x + (tmpBygning.bredde / 2)
						- (TxtWidth / 2);
				TxtHeight = TextHeight(tmpBygning.side2Bogstav);
				if (TxtHeight > 6) {
					TxtHeight = 1;
				} else {
					TxtHeight = 0;
				}

				CurrentY = tmpBygning.y - 20 - TxtHeight;
				Printtext(tmpBygning.side1Bogstav, CurrentX, CurrentY);

				retVal = DrawLineF(tmpBygning.x, tmpBygning.y - 5,
						(tmpBygning.x + tmpBygning.bredde), tmpBygning.y - 5);
				retVal = DrawLineF(tmpBygning.x, tmpBygning.y - 7,
						tmpBygning.x, tmpBygning.y - 2);
				retVal = DrawLineF((tmpBygning.x + tmpBygning.bredde),
						tmpBygning.y - 7, (tmpBygning.x + tmpBygning.bredde),
						tmpBygning.y - 2);

			}
			// side 2:
			retVal = DrawLineF(tmpBygning.x, tmpBygning.y, tmpBygning.x,
					tmpBygning.y + tmpBygning.hoejde);
			if (tmpBygning.side2Bogstav !== "" && maalsaetning == true) {
				TxtHeight = TextHeight(tmpBygning.side2Bogstav);
				CurrentX = tmpBygning.x - 14;
				CurrentY = tmpBygning.y + (tmpBygning.hoejde / 2)
						- (TxtHeight / 2);
				Printtext(tmpBygning.side2Bogstav, CurrentX, CurrentY);

				retVal = DrawLineF(tmpBygning.x - 5, tmpBygning.y,
						tmpBygning.x - 5, tmpBygning.y + tmpBygning.hoejde);
				retVal = DrawLineF(tmpBygning.x - 7, tmpBygning.y,
						tmpBygning.x - 2, tmpBygning.y);
				retVal = DrawLineF(tmpBygning.x - 7, tmpBygning.y
						+ tmpBygning.hoejde, tmpBygning.x - 2, tmpBygning.y
						+ tmpBygning.hoejde);

			}
			// Side 4:
			retVal = DrawLineF(tmpBygning.x, tmpBygning.y + tmpBygning.hoejde,
					tmpBygning.x + tmpBygning.bredde, tmpBygning.y
							+ tmpBygning.hoejde);
			if (tmpBygning.side4Bogstav !== "" && maalsaetning == true) {

				TxtWidth = TextWidth(tmpBygning.side4Bogstav);
				CurrentX = tmpBygning.x + (tmpBygning.bredde / 2)
						- (TxtWidth / 2);
				CurrentY = tmpBygning.y + tmpBygning.hoejde + 2;
				Printtext(tmpBygning.side4Bogstav, CurrentX, CurrentY);
			}
		}
		break;
	case 3:
		tmpBygning.x = strPosX + oprindeligBygning.bredde;
		retVal = DrawLineF(tmpBygning.x, tmpBygning.y, tmpBygning.x
				+ tmpBygning.bredde, tmpBygning.y);
		// Side 1 bogstav
		if (tmpBygning.side1Bogstav !== "" && maalsaetning == true) {

			TxtWidth = TextWidth(tmpBygning.side1Bogstav);
			CurrentX = tmpBygning.x + (tmpBygning.bredde / 2) - (TxtWidth / 2);
			CurrentY = tmpBygning.y - 24;
			Printtext(tmpBygning.side1Bogstav, CurrentX, CurrentY);
			retVal = DrawLineF(tmpBygning.x, tmpBygning.y - 5,
					(tmpBygning.x + tmpBygning.bredde), tmpBygning.y - 5);
			retVal = DrawLineF(tmpBygning.x, tmpBygning.y - 7, tmpBygning.x,
					tmpBygning.y - 2);
			retVal = DrawLineF((tmpBygning.x + tmpBygning.bredde),
					tmpBygning.y - 7, (tmpBygning.x + tmpBygning.bredde),
					tmpBygning.y - 2);

		}

		retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde, tmpBygning.y,
				tmpBygning.x + tmpBygning.bredde, tmpBygning.y
						+ tmpBygning.hoejde);
		// side 3 bogstav
		if (tmpBygning.side3Bogstav !== "" && maalsaetning == true) {
			TxtHeight = TextHeight(tmpBygning.side3Bogstav);
			CurrentX = tmpBygning.x + tmpBygning.bredde + 10;
			CurrentY = tmpBygning.y + (tmpBygning.hoejde / 2) - (TxtHeight / 2);
			Printtext(tmpBygning.side3Bogstav, CurrentX, CurrentY);

			retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde + 5,
					tmpBygning.y, tmpBygning.x + tmpBygning.bredde + 5,
					tmpBygning.y + tmpBygning.hoejde);
			retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde + 7,
					tmpBygning.y, tmpBygning.x + tmpBygning.bredde + 2,
					tmpBygning.y);
			retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde + 7,
					tmpBygning.y + tmpBygning.hoejde, tmpBygning.x
							+ tmpBygning.bredde + 2, tmpBygning.y
							+ tmpBygning.hoejde);

		}

		retVal = DrawLineF(tmpBygning.x, tmpBygning.y + tmpBygning.hoejde,
				tmpBygning.x + tmpBygning.bredde, tmpBygning.y
						+ tmpBygning.hoejde);

		// Samling med &&en bygning!
		CurColor = RGB(255, 255, 255);
		retVal = DrawLineF(LineARx[0], LineARy[0], LineARx[1], LineARy[1]);
		retVal = DrawLineF(tmpBygning.x, tmpBygning.y + 1, tmpBygning.x,
				tmpBygning.y + tmpBygning.hoejde);
		CurColor = RGB(0, 0, 0);
		// --------------------------------------------------------------------------------
		// Tilbygning placeret på side 4
		// --------------------------------------------------------------------------------
		break;
	case 4:
		tmpBygning.y = strPosY + hoejde;
		// Side 2:
		retVal = DrawLineF(tmpBygning.x, tmpBygning.y, tmpBygning.x,
				tmpBygning.y + tmpBygning.hoejde);

		if (tmpBygning.side2Bogstav !== "" && maalsaetning == true) {
			TxtHeight = TextHeight(tmpBygning.side2Bogstav);
			CurrentX = tmpBygning.x - 14;
			CurrentY = tmpBygning.y + (tmpBygning.hoejde / 2) - (TxtHeight / 2);
			Printtext(tmpBygning.side2Bogstav, CurrentX, CurrentY);

			retVal = DrawLineF(tmpBygning.x - 5, tmpBygning.y,
					tmpBygning.x - 5, tmpBygning.y + tmpBygning.hoejde);
			retVal = DrawLineF(tmpBygning.x - 7, tmpBygning.y,
					tmpBygning.x - 2, tmpBygning.y);
			retVal = DrawLineF(tmpBygning.x - 7, tmpBygning.y
					+ tmpBygning.hoejde, tmpBygning.x - 2, tmpBygning.y
					+ tmpBygning.hoejde);

		}

		// Side 4:
		retVal = DrawLineF(tmpBygning.x, tmpBygning.y + tmpBygning.hoejde,
				tmpBygning.x + tmpBygning.bredde, tmpBygning.y
						+ tmpBygning.hoejde);
		if (tmpBygning.side4Bogstav !== "" && maalsaetning == true) {
			if (tmpBygning.side4TilbygningAktiv == true) {
			} else {
				TxtWidth = TextWidth(tmpBygning.side4Bogstav);
				CurrentX = tmpBygning.x + (tmpBygning.bredde / 2)
						- (TxtWidth / 2);
				CurrentY = tmpBygning.y + tmpBygning.hoejde + 2;
				Printtext(tmpBygning.side4Bogstav, CurrentX, CurrentY);

				retVal = DrawLineF(tmpBygning.x, tmpBygning.y
						+ tmpBygning.hoejde + 5,
						(tmpBygning.x + tmpBygning.bredde), tmpBygning.y
								+ tmpBygning.hoejde + 5);
				retVal = DrawLineF(tmpBygning.x, tmpBygning.y
						+ tmpBygning.hoejde + 7, tmpBygning.x, tmpBygning.y
						+ tmpBygning.hoejde + 2);
				retVal = DrawLineF((tmpBygning.x + tmpBygning.bredde),
						tmpBygning.y + tmpBygning.hoejde + 7,
						(tmpBygning.x + tmpBygning.bredde), tmpBygning.y
								+ tmpBygning.hoejde + 2);
			}
		}

		// Side 3:

		if (hoved == 50000 && sidePlacering == 4) {

			calcVariabel3 = tmpBygning.bredde / Tan(p2 * 65);
			retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde, tmpBygning.y
					+ calcVariabel3 - 1, tmpBygning.x + tmpBygning.bredde,
					tmpBygning.y + tmpBygning.hoejde);

			if (tmpBygning.side3Bogstav !== "" && maalsaetning == true) {
				TxtHeight = TextHeight(tmpBygning.side3Bogstav);
				CurrentX = tmpBygning.x + tmpBygning.bredde + 8;
				CurrentY = tmpBygning.y + calcVariabel3
						+ ((tmpBygning.hoejde - calcVariabel3) / 2)
						- (TxtHeight / 2);
				Printtext(tmpBygning.side3Bogstav, CurrentX, CurrentY);

				retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde + 5,
						tmpBygning.y + calcVariabel3 - 1, tmpBygning.x
								+ tmpBygning.bredde + 5, tmpBygning.y
								+ tmpBygning.hoejde);
				// retVal = DrawLineF(.x + .bredde + 7, .y + calcVariabel3 - 1,
				// .x + .bredde +
				// 2, .y + calcVariabel3 - 1)
				retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde + 7,
						tmpBygning.y + tmpBygning.hoejde, tmpBygning.x
								+ tmpBygning.bredde + 2, tmpBygning.y
								+ tmpBygning.hoejde);

			}

		} else {
			retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde, tmpBygning.y,
					tmpBygning.x + tmpBygning.bredde, tmpBygning.y
							+ tmpBygning.hoejde);
			if (tmpBygning.side3Bogstav !== "" && maalsaetning == true) {
				TxtHeight = TextHeight(tmpBygning.side3Bogstav);
				CurrentX = tmpBygning.x + tmpBygning.bredde + 8;
				CurrentY = tmpBygning.y + (tmpBygning.hoejde / 2)
						- (TxtHeight / 2);
				Printtext(tmpBygning.side3Bogstav, CurrentX, CurrentY);

				retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde + 5,
						tmpBygning.y, tmpBygning.x + tmpBygning.bredde + 5,
						tmpBygning.y + tmpBygning.hoejde);
				retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde + 7,
						tmpBygning.y, tmpBygning.x + tmpBygning.bredde + 2,
						tmpBygning.y);
				retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde + 7,
						tmpBygning.y + tmpBygning.hoejde, tmpBygning.x
								+ tmpBygning.bredde + 2, tmpBygning.y
								+ tmpBygning.hoejde);

			}
		}
		// Side 1:
		if (hoved == 70000 && tmpBygning.rygningsRetning == 0) {
			retVal = DrawLineF(tmpBygning.x, tmpBygning.y, tmpBygning.x
					+ tmpBygning.bredde, tmpBygning.y);
			//
		}

		//
		// --------------------------------------------------------------------------
		// ---->>>> Kvist <<<<----
		//
		// --------------------------------------------------------------------------
		if (tmpBygning.kvistAktiv == true) {

			kvistX = tmpBygning.x + ((tmpBygning.bredde / 4) * 2);
			kvistY = tmpBygning.y + (tmpBygning.hoejde / 2) + 4;

			kvistY1 = kvistY + 16.3;
			kvistX1 = kvistX - 8.14;
			kvistX11 = kvistX + 8.14;

			retVal = DrawLineF(kvistX1, kvistY1, kvistX11, kvistY1);

			retVal = DrawLineF(kvistX1, kvistY1, kvistX1, kvistY1 - 6);
			retVal = DrawLineF(kvistX11, kvistY1, kvistX11, kvistY1 - 6);

			retVal = DrawLineF(kvistX11, kvistY1 - 6, kvistX, kvistY);
			retVal = DrawLineF(kvistX1, kvistY1 - 6, kvistX, kvistY);

			FFcolor = "#FF0000";

			retVal = ExtFloodFill(kvistX, kvistY1 - 6, FFcolor);

			// Midter stregen:
			if (tmpBygning.kvistType == 1) {
				retVal = DrawLineF(kvistX, kvistY, kvistX, kvistY1);

			}

			if (tmpBygning.kvistType == 2) {
				kvistY22 = kvistY + ((kvistY1 - kvistY) / 3);
				retVal = DrawLineF(kvistX, kvistY, kvistX, kvistY22 + 1);

				retVal = DrawLineF(kvistX1, kvistY1, kvistX, kvistY22);
				retVal = DrawLineF(kvistX11, kvistY1, kvistX, kvistY22);
			}

			if (tmpBygning.kvistType == 3) {
				kvistY22 = kvistY + (((kvistY1 - kvistY) / 3.5) * 2.5);
				retVal = DrawLineF(kvistX, kvistY, kvistX, kvistY22 + 1);

				retVal = DrawLineF(kvistX1 + 4.44, kvistY1, kvistX, kvistY22);
				retVal = DrawLineF(kvistX11 - 4.44, kvistY1, kvistX, kvistY22);
			}

		}
		// Samling med &&en bygning!
		if (!(hoved == 50000 && sidePlacering == 4)) {
			retVal = DrawLineF(LineArx[0], LineAry[0], LineArx[1], LineAry[1]);
		}
		if (tmpBygning.rygningsRetning == 0) {
			// Dim tempxxx As Double

			// tempxxx = oprindeligBygning.x

			retVal = DrawLineF(oprindeligBygning.x + 1, tmpBygning.y,
					(oprindeligBygning.x + oprindeligBygning.bredde),
					tmpBygning.y);

		} else {
			if (!(hoved == 50000 && sidePlacering == 4)) {
				retVal = DrawLineF(tmpBygning.x + 1, tmpBygning.y,
						(tmpBygning.x + tmpBygning.bredde), tmpBygning.y);
			}
		}

		hoejesteY = hoejesteY + tmpBygning.hoejde;
		break;
	}

	// Tilbygninger

	if (tmpBygning.side1TilbygningAktiv == true) {
		TegnBygning(tmpBygning.side1Tilbygning, tmpBygning, tmpBygning.x,
				tmpBygning.y, 1, hoejesteY, maalsaetning);
	}
	if (tmpBygning.side2TilbygningAktiv == true) {
		TegnBygning(tmpBygning.side2Tilbygning, tmpBygning, tmpBygning.x,
				tmpBygning.y, 2, hoejesteY, maalsaetning);
	}
	if (tmpBygning.side3TilbygningAktiv == true) {
		TegnBygning(tmpBygning.side3Tilbygning, tmpBygning, tmpBygning.x,
				tmpBygning.y, 3, hoejesteY, maalsaetning);
	}
	if (tmpBygning.side4TilbygningAktiv == true) {
		if (hoved == 50000 && sidePlacering == 2) {

			TegnBygning(tmpBygning.side4Tilbygning, tmpBygning, vink45x2,
					vink45x2 - tmpBygning.hoejde, 4, hoejesteY, maalsaetning);

		} else {
			TegnBygning(tmpBygning.side4Tilbygning, tmpBygning, tmpBygning.x,
					tmpBygning.y, 4, hoejesteY, maalsaetning);
		}
	}

	// Tegn noget rygning!!!
	// ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	switch (tmpBygning.rygningsRetning) {

	case 0:
		// V&&ret
		strRygning = tmpBygning.x;
		sltRygning = tmpBygning.x + tmpBygning.bredde;

		strRygningY = tmpBygning.y + (tmpBygning.hoejde / 2);
		sltRygningY = strRygningY;
		break;
	case 1:
		// Lodret
		if (hoved == 50000 && sidePlacering == 2) {
			strRygning = oprindeligBygning.x
					- ((oprindeligBygning.hoejde / 2) / (Tan(p2 * 65)));
			strRygningY = oprindeligBygning.y + (oprindeligBygning.hoejde / 2);

			// Stump rygning ind til oprindelig hus, pga. speciel vinkel!
			retVal = DrawLineF(strRygning, strRygningY, oprindeligBygning.x,
					(oprindeligBygning.y + (oprindeligBygning.hoejde / 2)));

			// Ųverste stykke
			// retVal = DrawLineF(vink45x1, vink45y1, vink45x2, vink45y2)
			// 'Gavl
			// retVal = DrawLineF(vink45x2, vink45y2, oprindeligBygning.x -
			// vink45b, (oprindeligBygning.y + oprindeligBygning.hoejde) +
			// vink45a)

			tempVink45RygA = (oprindeligBygning.x - vink45b) - vink45x2;
			tempVink45RygB = ((oprindeligBygning.y + oprindeligBygning.hoejde) + vink45a)
					- vink45y2;

			tempVink45RygC = Sqr((tempVink45RygA * tempVink45RygA)
					+ (tempVink45RygB * tempVink45RygB));

			if (tmpBygning.side4TilbygningAktiv == false) {
				sltRygning = vink45x2 + (Sin(p2 * 45) * (tempVink45RygC / 2))
						+ 1;

				sltRygningY = vink45y2 + (Cos(p2 * 45) * (tempVink45RygC / 2))
						+ 1;
			}

		} else {

			strRygning = tmpBygning.x + (tmpBygning.bredde / 2);
			sltRygning = strRygning;
			if (hoved == 50000 && sidePlacering == 4) {
				strRygningY = tmpBygning.y + calcVariabel3;
				calcVariabel3 = (tmpBygning.bredde / 2) * Tan(p2 * 25);
				strRygningY = strRygningY - calcVariabel3;
			} else {
				strRygningY = tmpBygning.y;
			}
			switch (sidePlacering) {
			case 0:
				sltRygningY = tmpBygning.y + tmpBygning.hoejde;
				break;
			case 1:
				sltRygningY = tmpBygning.y - tmpBygning.hoejde;
				break;
			case 4:
				sltRygningY = tmpBygning.y + tmpBygning.hoejde;
				break;
			}
		}
		break;
	default:
		// Ingen rygning
		break;
	}

	switch (sidePlacering) {
	case 0:
		tmpX = tmpBygning.x + 2;
		tmpY = tmpBygning.y + 2;
		break;
	case 1:
		tmpX = tmpBygning.x + 2;
		tmpY = tmpBygning.y - 2;
		break;
	case 2:

		if (hoved == 50000) {
			tmpX = vink45x2 + 3;
			tmpY = vink45y2;
		} else {

			tmpX = tmpBygning.x + 2;
			tmpY = tmpBygning.y + 2;
		}
		break;
	case 3:
		tmpX = tmpBygning.x + 2;
		tmpY = tmpBygning.y + 2;
		break;
	case 4:
		tmpX = tmpBygning.x + 2;
		tmpY = tmpBygning.y + 2;
		break;
	}

	if (hoved == 50000 && sidePlacering == 2) {

		FFcolor = "#FF0000";
		retVal = ExtFloodFill(vink45x2 + 5, vink45y2, FFcolor);
		// retVal = ExtFloodFill(110, 25, FFcolor);
		// retVal = ExtFloodFill(112, 40, FFcolor);

	} else {
		FFcolor = "#FF0000";
		if (hoved == 50000) {
			retVal = ExtFloodFill(117, tmpY, FFcolor);
			retVal = ExtFloodFill(110, 25, FFcolor);
			retVal = ExtFloodFill(112, 40, FFcolor);
		} else {
			retVal = ExtFloodFill(tmpX, tmpY, FFcolor);
		}

	}

	switch (sidePlacering) {
	case 0:
		midtRygning = tmpBygning.y + (tmpBygning.hoejde / 2);
		break;
	case 1:
		midtRygning = tmpBygning.y - tmpBygning.hoejde
				- (tmpBygning.bredde / 2);
		break;
	case 2:
		midtRygning = tmpBygning.y + tmpBygning.hoejde
				- (tmpBygning.hoejde / 2);
		break;
	case 3:
		midtRygning = tmpBygning.y + tmpBygning.hoejde
				- (tmpBygning.hoejde / 2);
		break;
	case 4:
		// Vi skal tage hųjde for hvordan rygningen vender...
		if (oprindeligBygning.rygningsRetning == 1
				&& tmpBygning.rygningsRetning == 0) {
			midtRygning = tmpBygning.y + (tmpBygning.hoejde / 2);
		} else {
			midtRygning = tmpBygning.y + (tmpBygning.bredde / 2);
		}
		break;
	}
	// -----------------------------------------------------------------------------------------------------------
	// Side 1
	// -----------------------------------------------------------------------------------------------------------
	if (tmpBygning.side1GavlTypeValgfri == true) {
		tmpBygning.side1GavlTypeValgfriX = strRygning - (tmpBygning.bredde / 2)
				- 3; // strRygning
		// - 3
		tmpBygning.side1GavlTypeValgfriY = tmpBygning.y - 3;
		tmpBygning.side1GavlTypeValgfriBredde = tmpBygning.bredde + 6; // (sltRygning
		// + 3)
		// -
		// (sltRygning
		// -
		// (.hoejde
		// / 2)
		// - 3)
		tmpBygning.side1GavlTypeValgfriHoejde = (strRygning + 3)
				- (strRygning - (tmpBygning.bredde / 2) - 3); // .hoejde
		// + 6
	}

	switch (tmpBygning.side1GavlType) { // gavl2
	case 1: // Gavl
		break;
	case 2: // Helvalm

		if (tmpBygning.rygningsRetning == 0) {
			// V&&ret rygning
			// Giver umiddelbart ikke mening her!

		}
		if (tmpBygning.rygningsRetning == 1) {
			// Lodret rygning
			if (sidePlacering == 0) {
				wrkFltNum = 0;
				strRygningY = strRygningY + (tmpBygning.bredde / 2);
			}
			if (sidePlacering == 1) {
				wrkFltNum = tmpBygning.hoejde;
				sltRygningY = sltRygningY + (tmpBygning.bredde / 2);
			}
			tegnStregen = true;

			if (tmpBygning.side2TilbygningAktiv == true) {
				if (tmpBygning.side2Tilbygning.placering == 0
						&& tmpBygning.side1GavlType == 2) {
					tegnStregen = false;
				}
			}

			// if ( ! (.side2TilbygningAktiv = true &&
			// .side2Tilbygning.placering = 0
			// && .side1GavlType = 2) ) {
			if (tegnStregen == true) {
				retVal = DrawLineF(tmpBygning.x, tmpBygning.y - wrkFltNum,
						strRygning, tmpBygning.y - wrkFltNum
								+ (tmpBygning.bredde / 2));
			}

			tegnStregen = true;
			if (tmpBygning.side3TilbygningAktiv == true) {
				if (tmpBygning.side3Tilbygning.placering == 0
						&& tmpBygning.side1GavlType == 2) {
					tegnStregen = false;
				}
			}

			// ! (.side3TilbygningAktiv == true &&
			// .side3Tilbygning.placering == 0 && .side1GavlType == 2) ) {
			if (tegnStregen == true) {
				retVal = DrawLineF(strRygning, tmpBygning.y - wrkFltNum
						+ (tmpBygning.bredde / 2), tmpBygning.x
						+ tmpBygning.bredde, tmpBygning.y - wrkFltNum);
			}

			// //// if ( sidePlacering == 1 ) {
			// //// retVal = DrawLineF(.x, .y - wrkFltNum, strRygning, .y -
			// wrkFltNum +
			// (.bredde / 2))
			// //// retVal = DrawLineF(strRygning, .y - wrkFltNum + (.bredde /
			// 2), .x +
			// .bredde, .y - wrkFltNum)
			// //// }
		}

		if (tmpBygning.rygningsRetning == 2) {
			// Ingen rygning
			// strRygning = strRygning + 50
			// //// retVal = DrawLineF(.x, .y - .hoejde, .x + (bredde / 2),
			// midtRygning)
			// //// retVal = DrawLineF(.x, .y - .hoejde, .x + (.bredde / 2),
			// midtRygning)
		}
		break;
	case 3: // Halvvalm
		if (tmpBygning.rygningsRetning == 1
				&& (sidePlacering == 0 || sidePlacering == 1)) {
			if (sidePlacering == 0) {
				wrkFltNum = 0;
				strRygningY = strRygningY + (tmpBygning.bredde / 2);
			}
			if (sidePlacering == 1) {
				// Lodret tilbygning på side 1 går "opad" altså lidt
				// modsatte mål...
				wrkFltNum = tmpBygning.hoejde;
				sltRygningY = sltRygningY + (tmpBygning.bredde / 2);
			}

			retVal = DrawLineF(tmpBygning.x
					+ ((tmpBygning.bredde / 4) + (tmpBygning.bredde * 0.074)),
					tmpBygning.y - wrkFltNum, strRygning, tmpBygning.y
							- wrkFltNum + (tmpBygning.bredde / 2));
			retVal = DrawLineF(
					tmpBygning.x
							+ tmpBygning.bredde
							- (((tmpBygning.bredde / 4) + (tmpBygning.bredde * 0.074))),
					tmpBygning.y - wrkFltNum, strRygning, tmpBygning.y
							- wrkFltNum + (tmpBygning.bredde / 2));
		}
		break;
	}
	// -----------------------------------------------------------------------------------------------------------
	// Side 2
	// -----------------------------------------------------------------------------------------------------------
	if (tmpBygning.side2GavlTypeValgfri == true) {
		tmpBygning.side2GavlTypeValgfriX = strRygning - 3;
		tmpBygning.side2GavlTypeValgfriY = tmpBygning.y - 3;
		tmpBygning.side2GavlTypeValgfriBredde = (sltRygning + 3)
				- (sltRygning - (tmpBygning.hoejde / 2) - 3);
		tmpBygning.side2GavlTypeValgfriHoejde = tmpBygning.hoejde + 6;
	}

	switch (tmpBygning.side2GavlType) { // gavl2
	case 1: // Gavl
		break;
	case 2: // Helvalm

		if (tmpBygning.rygningsRetning == 0) {
			// Vandret rygning

			strRygning = strRygning + (tmpBygning.hoejde / 2); // * 0.43)

			tegnStregen = true;
			if (tmpBygning.side1TilbygningAktiv == true) {
				if (tmpBygning.side1Tilbygning.placering == 0
						&& tmpBygning.side2GavlType == 2) {
					tegnStregen = false;
				}
			}

			if (tegnStregen == true) {
				retVal = DrawLineF(tmpBygning.x, tmpBygning.y, strRygning,
						midtRygning);
			}

			tegnStregen = true;
			if (tmpBygning.side4TilbygningAktiv == true) {
				if (tmpBygning.side4Tilbygning.placering == 0
						&& tmpBygning.side2GavlType == 2) {
					tegnStregen = false;
				}
			}

			if (tegnStregen == true) {
				retVal = DrawLineF(tmpBygning.x, tmpBygning.y
						+ tmpBygning.hoejde, strRygning, midtRygning);
			}

		}
		if (tmpBygning.rygningsRetning == 1) {
			// Lodret rygning
			// Giver umiddelbart ikke mening her!?
		}

		if (tmpBygning.rygningsRetning == 2) {
			// Ingen rygning
			// strRygning = strRygning + 50
			// retVal = DrawLineF(tmpBygning.x, tmpBygning.y, tmpBygning.x
			// + (bredde / 2), midtRygning);
			// retVal = DrawLineF(tmpBygning.x, tmpBygning.y +
			// tmpBygning.hoejde,
			// tmpBygning.x + (tmpBygning.bredde / 2), midtRygning);
		}
		break;
	case 3: // Halvvalm
		if (tmpBygning.rygningsRetning == 0) {
			if ((tmpBygning.bredde * 0.16) < 20) {
				strRygning = strRygning + (tmpBygning.bredde * 0.16);
			} else {
				strRygning = strRygning + 20;
			}

			retVal = DrawLineF(tmpBygning.x, tmpBygning.y
					+ ((tmpBygning.hoejde / 4) + (tmpBygning.hoejde * 0.074)),
					strRygning, midtRygning);
			retVal = DrawLineF(tmpBygning.x, (tmpBygning.y + tmpBygning.hoejde)
					- ((tmpBygning.hoejde / 4) + (tmpBygning.hoejde * 0.074)),
					strRygning, midtRygning);
		}
		break;
	}

	// -----------------------------------------------------------------------------------------------------------
	// -----------------------------------------------------------------------------------------------------------
	// Side 3
	// -----------------------------------------------------------------------------------------------------------

	if (tmpBygning.side3GavlTypeValgfri == true) {
		tmpBygning.side3GavlTypeValgfriX = sltRygning - (tmpBygning.hoejde / 2)
				- 3;
		tmpBygning.side3GavlTypeValgfriY = tmpBygning.y - 3;
		tmpBygning.side3GavlTypeValgfriBredde = (sltRygning + 3)
				- (sltRygning - (tmpBygning.hoejde / 2) - 3);
		tmpBygning.side3GavlTypeValgfriHoejde = tmpBygning.hoejde + 6;
	}

	switch (tmpBygning.side3GavlType) { // gavl3
	case 1: // Gavl
		break;
	case 2: // Helvalm
		if (tmpBygning.rygningsRetning == 0) {
			sltRygning = sltRygning - (tmpBygning.hoejde / 2); // * 0.43)

			tegnStregen = true;
			if (tmpBygning.side1TilbygningAktiv == true) {
				if (tmpBygning.side1Tilbygning.placering == 4
						&& tmpBygning.side3GavlType == 2) {
					tegnStregen = false;
				}
			}

			// if ( ! (.side1TilbygningAktiv = true &&
			// .side1Tilbygning.placering =
			// 4
			// && .side3GavlType = 2) ) {
			if (tegnStregen == true) {
				retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde,
						tmpBygning.y, sltRygning, midtRygning);
			}

			tegnStregen = true;
			if (tmpBygning.side4TilbygningAktiv == true) {
				if (tmpBygning.side4Tilbygning.placering == 4
						&& tmpBygning.side3GavlType == 2) {
					tegnStregen = false;
				}
			}

			// if ( ! (.side4TilbygningAktiv = true &&
			// .side4Tilbygning.placering =
			// 4
			// && .side3GavlType = 2) ) {
			if (tegnStregen == true) {
				retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde,
						tmpBygning.y + tmpBygning.hoejde, sltRygning,
						midtRygning);
			}
		}
		if (tmpBygning.rygningsRetning == 1) {
			// Lodret rygning
			// Giver umiddelbart ikke mening her!?
		}
		if (tmpBygning.rygningsRetning == 2) {
			// //// retVal = DrawLineF(.x + .bredde, .y, .x + (.bredde / 2),
			// midtRygning)
			// //// retVal = DrawLineF(.x + .bredde, .y + .hoejde, .x + (.bredde
			// /
			// 2),
			// midtRygning)
		}
		break;
	case 3: // Halvvalm
		if (tmpBygning.rygningsRetning == 0) {
			if ((tmpBygning.bredde * 0.16) < 20) {
				sltRygning = sltRygning - (tmpBygning.bredde * 0.16);
			} else {
				sltRygning = sltRygning - 20;
			}
			retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde, tmpBygning.y
					+ ((tmpBygning.hoejde / 4) + (tmpBygning.hoejde * 0.074)),
					sltRygning, midtRygning);
			retVal = DrawLineF(
					tmpBygning.x + tmpBygning.bredde,
					(tmpBygning.y + tmpBygning.hoejde)
							- ((tmpBygning.hoejde / 4) + (tmpBygning.hoejde * 0.074)),
					sltRygning, midtRygning);
		}
		break;
	}

	// -----------------------------------------------------------------------------------------------------------
	// Side 4
	// -----------------------------------------------------------------------------------------------------------
	if (hoved == 50000 && sidePlacering == 2) {
		if (tmpBygning.side4GavlTypeValgfri == true) {
			tmpBygning.side4GavlTypeValgfriX = vink45x2 - 3;
			tmpBygning.side4GavlTypeValgfriY = vink45y2 - 3;
			tmpBygning.side4GavlTypeValgfriBredde = tempVink45RygA + 6;
			tmpBygning.side4GavlTypeValgfriHoejde = tempVink45RygB + 6;

		}

		switch (tmpBygning.side4GavlType) {
		case 1: // Gavl
			break;
		case 2: // Helvalm
			// tempVink45RygC
			var forskyedning;
			forskyedning = (tempVink45RygC / 2) / (Sin(p2 * 45));
			sltRygning = vink45x2 + forskyedning;
			sltRygningY = vink45y2;
			retVal = DrawLineF(vink45x2, vink45y2, sltRygning, sltRygningY);
			retVal = DrawLineF(sltRygning, sltRygningY, oprindeligBygning.x
					- vink45b + 1,
					(oprindeligBygning.y + oprindeligBygning.hoejde) + vink45a
							+ 1);

			break;
		case 3: // Halvvalm

			forskyedning = (tempVink45RygC / 2) / (Sin(p2 * 45));

			sltRygning = vink45x2 + forskyedning - (forskyedning * 0.16);
			sltRygningY = vink45y2 + (forskyedning * 0.16);

			forskyedning = (Sin(p2 * 45)) * (tempVink45RygC / 3);

			retVal = DrawLineF(vink45x2 + forskyedning,
					vink45y2 + forskyedning, sltRygning, sltRygningY);

			retVal = DrawLineF(
					sltRygning,
					sltRygningY,
					(oprindeligBygning.x - vink45b + 1) - forskyedning,
					((oprindeligBygning.y + oprindeligBygning.hoejde) + vink45a + 1)
							- forskyedning);
			break;
		}
	} else {

		if (tmpBygning.side4GavlTypeValgfri == true) {
			tmpBygning.side4GavlTypeValgfriX = strRygning
					- (tmpBygning.bredde / 2) - 3;
			tmpBygning.side4GavlTypeValgfriY = sltRygningY
					- (tmpBygning.bredde / 2) - 3;
			tmpBygning.side4GavlTypeValgfriBredde = tmpBygning.bredde + 6;
			tmpBygning.side4GavlTypeValgfriHoejde = (strRygning + 3)
					- (strRygning - (tmpBygning.bredde / 2) - 3); // .hoejde +
			// 6
		}

		switch (tmpBygning.side4GavlType) {// gavl2
		case 1: // Gavl
			break;
		case 2: // Helvalm

			if (tmpBygning.rygningsRetning == 1) {
				// Lodret rygning

				if (sidePlacering == 4 || sidePlacering == 0) {
					sltRygningY = sltRygningY - (tmpBygning.bredde / 2);
					tegnStregen = true;
					if (tmpBygning.side2TilbygningAktiv == true) {
						if (tmpBygning.side2Tilbygning.placering == 4
								&& tmpBygning.side4GavlType == 2) {
							tegnStregen = false;
						}
					}

					if (tegnStregen == true) {
						retVal = DrawLineF(tmpBygning.x, tmpBygning.y
								+ tmpBygning.hoejde, strRygning, tmpBygning.y
								+ tmpBygning.hoejde - (tmpBygning.bredde / 2));
					}

					tegnStregen = true;
					if (tmpBygning.side3TilbygningAktiv == true) {
						if (tmpBygning.side3Tilbygning.placering == 4
								&& tmpBygning.side4GavlType == 2) {
							tegnStregen = false;
						}
					}

					if (tegnStregen == true) {
						retVal = DrawLineF(strRygning, tmpBygning.y
								+ tmpBygning.hoejde - (tmpBygning.bredde / 2),
								tmpBygning.x + tmpBygning.bredde, tmpBygning.y
										+ tmpBygning.hoejde);
					}
				}
			}

			if (tmpBygning.rygningsRetning == 2) {
				// Ingen rygning
				// strRygning = strRygning + 50
				// retVal = DrawLineF(.x, .y + .hoejde, .x + (bredde / 2),
				// midtRygning)
				// retVal = DrawLineF(.x, .y + .hoejde, .x + (.bredde / 2),
				// midtRygning)
			}
			break;
		case 3: // Halvvalm
			if (tmpBygning.rygningsRetning == 1
					&& (sidePlacering == 4 || sidePlacering == 0)) {
				sltRygningY = sltRygningY - (tmpBygning.bredde / 2);
				retVal = DrawLineF(
						tmpBygning.x
								+ ((tmpBygning.bredde / 4) + (tmpBygning.bredde * 0.074)),
						tmpBygning.y + tmpBygning.hoejde, strRygning,
						tmpBygning.y + tmpBygning.hoejde
								- (tmpBygning.bredde / 2));
				retVal = DrawLineF(
						tmpBygning.x
								+ tmpBygning.bredde
								- ((tmpBygning.bredde / 4) + (tmpBygning.bredde * 0.074)),
						tmpBygning.y + tmpBygning.hoejde, strRygning,
						tmpBygning.y + tmpBygning.hoejde
								- (tmpBygning.bredde / 2));
			}
			break;
		}

	}

	// -----------------------------------------------------------------------------------------------------------
	// Her tegnes selve rygningen
	// -----------------------------------------------------------------------------------------------------------

	// sidePlacering

	// Floodfill af hele området med rųd farve
	// ---------------------------------------

	var tegnetGavl;
	if (sidePlacering == 0) {

		switch (tmpBygning.rygningsRetning) {
		case 0:
			// 0 = Vandret
			retVal = DrawLineF(strRygning, midtRygning, sltRygning, midtRygning);
			break;
		case 1:
			// 1 = Lodret
			retVal = DrawLineF(tmpBygning.x + (bredde / 2), strRygningY,
					tmpBygning.x + (tmpBygning.bredde / 2), sltRygningY);
			break;
		case 2:
			// Ingen rygning
			break;
		}

	} else {
		switch (tmpBygning.rygningsRetning) {
		case 0:
			// 0 = Vandret
			retVal = DrawLineF(strRygning, strRygningY, sltRygning, sltRygningY);

			// Her skal vi måske tegne samlingen med den anden
			// bygning???????????

			if (sidePlacering == 4 && oprindeligBygning.rygningsRetning == 1
					&& tmpBygning.rygningsRetning == 0
					&& oprindeligBygning.side4Bogstav !== ""
					&& maalsaetning == true) {
				// På et H-Hus er der lidt problemer med det ene bogstav!
				TxtWidth = TextWidth(oprindeligBygning.side4Bogstav);
				CurrentX = oprindeligBygning.x + (oprindeligBygning.bredde / 2)
						- (TxtWidth / 2);
				// CurrentY = .y + (.hoejde / 2) + 3
				CurrentY = tmpBygning.y + (tmpBygning.hoejde / 2) - 14;
				Printtext(tmpBygning.side4Bogstav, CurrentX, CurrentY);

				retVal = DrawLineF(oprindeligBygning.x, tmpBygning.y
						+ (tmpBygning.hoejde / 2) - 9,
						(oprindeligBygning.x + oprindeligBygning.bredde),
						tmpBygning.y + (tmpBygning.hoejde / 2) - 9);
				retVal = DrawLineF(oprindeligBygning.x, tmpBygning.y
						+ (tmpBygning.hoejde / 2) - 11, oprindeligBygning.x,
						tmpBygning.y + (tmpBygning.hoejde / 2) - 6);
				retVal = DrawLineF(
						(oprindeligBygning.x + oprindeligBygning.bredde),
						tmpBygning.y + (tmpBygning.hoejde / 2) - 11,
						(oprindeligBygning.x + oprindeligBygning.bredde),
						tmpBygning.y + (tmpBygning.hoejde / 2) - 6);

			}

			if (sidePlacering == 2 && oprindeligBygning.rygningsRetning == 1) {
				tegnetGavl = false;

				switch (tilbBeskrivelse.placering) {
				case 0: // Venstre side
					if (oprindeligBygning.side1GavlType == 2) { // Helvalm
						// Lidt trekants regning:
						vinkelHoejde = (tmpBygning.hoejde / 2) * Tan(p2 * 45);

						retVal = DrawLineF(
								tmpBygning.x + tmpBygning.bredde,
								tmpBygning.y + (tmpBygning.hoejde / 2),
								tmpBygning.x + tmpBygning.bredde + vinkelHoejde,
								tmpBygning.y + (tmpBygning.hoejde / 2));

						retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde
								+ vinkelHoejde, tmpBygning.y
								+ (tmpBygning.hoejde / 2), oprindeligBygning.x
								+ (oprindeligBygning.bredde / 2),
								oprindeligBygning.y
										+ (oprindeligBygning.bredde / 2));
						retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde,
								tmpBygning.y + tmpBygning.hoejde, tmpBygning.x
										+ tmpBygning.bredde + vinkelHoejde,
								tmpBygning.y + (tmpBygning.hoejde / 2));
						tegnetGavl = true;
					}
					break;
				case 4: // Hųjre
					if (oprindeligBygning.side4GavlType == 2) { // Helvalm
						// Lidt trekants regning:
						vinkelHoejde = (tmpBygning.hoejde / 2) * Tan(p2 * 45);
						retVal = DrawLineF(
								tmpBygning.x + tmpBygning.bredde,
								tmpBygning.y + (tmpBygning.hoejde / 2),
								tmpBygning.x + tmpBygning.bredde + vinkelHoejde,
								tmpBygning.y + (tmpBygning.hoejde / 2));
						retVal = DrawLineF(
								tmpBygning.x + tmpBygning.bredde + vinkelHoejde,
								tmpBygning.y + (tmpBygning.hoejde / 2),
								oprindeligBygningtmpBygning.x
										+ (oprindeligBygningtmpBygning.bredde / 2),
								oprindeligBygningtmpBygning.y
										+ oprindeligBygningtmpBygning.hoejde
										- (oprindeligBygningtmpBygning.bredde / 2));
						retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde,
								tmpBygning.y, tmpBygning.x + tmpBygning.bredde
										+ vinkelHoejde, tmpBygning.y
										+ (tmpBygning.hoejde / 2));
						tegnetGavl = true;
					}
					break;
				}

				if (tegnetGavl == false) {
					retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde,
							tmpBygning.y, tmpBygning.x + tmpBygning.bredde
									+ (oprindeligBygningtmpBygning.bredde / 2)
									- 10, tmpBygning.y
									+ (tmpBygning.hoejde / 2));
					retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde,
							tmpBygning.y + tmpBygning.hoejde, tmpBygning.x
									+ tmpBygning.bredde
									+ (oprindeligBygningtmpBygning.bredde / 2)
									- 10, tmpBygning.y
									+ (tmpBygning.hoejde / 2));
					// retVal = DrawLineF(tmpBygning.x + bredde, tmpBygning.y +
					// (tmpBygning.hoejde / 2), tmpBygning.x + tmpBygning.bredde
					// +
					// (oprindeligBygningtmpBygning.bredde / 2) - 10,
					// tmpBygning.y +
					// (tmpBygning.hoejde / 2));
					retVal = DrawLineF(sltRygning, sltRygningY, tmpBygning.x
							+ tmpBygning.bredde
							+ (oprindeligBygningtmpBygning.bredde / 2) - 10,
							tmpBygning.y + (tmpBygning.hoejde / 2));

					// sltRygningY
				}

			}

			// ////////////////////////////////////
			if (sidePlacering == 3 && oprindeligBygning.rygningsRetning == 1) {
				tegnetGavl = false;

				switch (tilbBeskrivelse.placering) {
				case 0: // Venstre side
					if (oprindeligBygning.side1GavlType == 2) { // Helvalm
						// Lidt trekants regning:
						vinkelHoejde = (tmpBygning.hoejde / 2) * Tan(p2 * 45);

						retVal = DrawLineF(tmpBygning.x, tmpBygning.y
								+ (tmpBygning.hoejde / 2), tmpBygning.x
								- vinkelHoejde, tmpBygning.y
								+ (tmpBygning.hoejde / 2));

						retVal = DrawLineF(tmpBygning.x - vinkelHoejde,
								tmpBygning.y + (tmpBygning.hoejde / 2),
								oprindeligBygning.x
										+ (oprindeligBygning.bredde / 2),
								oprindeligBygning.y
										+ (oprindeligBygning.bredde / 2));
						retVal = DrawLineF(tmpBygning.x, tmpBygning.y
								+ tmpBygning.hoejde, tmpBygning.x
								- vinkelHoejde, tmpBygning.y
								+ (tmpBygning.hoejde / 2));
						tegnetGavl = true;
					}
					break;
				case 4: // Hųjre
					if (oprindeligBygning.side4GavlType == 2) { // Helvalm
						// Lidt trekants regning:
						vinkelHoejde = (tmpBygning.hoejde / 2) * Tan(p2 * 45);
						retVal = DrawLineF(tmpBygning.x, tmpBygning.y
								+ (tmpBygning.hoejde / 2), tmpBygning.x
								- vinkelHoejde, tmpBygning.y
								+ (tmpBygning.hoejde / 2));
						retVal = DrawLineF(tmpBygning.x - vinkelHoejde,
								tmpBygning.y + (tmpBygning.hoejde / 2),
								oprindeligBygning.x
										+ (oprindeligBygning.bredde / 2),
								oprindeligBygning.y + oprindeligBygning.hoejde
										- (oprindeligBygning.bredde / 2));
						retVal = DrawLineF(tmpBygning.x, tmpBygning.y,
								tmpBygning.x - vinkelHoejde, tmpBygning.y
										+ (tmpBygning.hoejde / 2));
						tegnetGavl = true;
					}
					break;
				}

				if (tegnetGavl == false) {
					retVal = DrawLineF(tmpBygning.x, tmpBygning.y, tmpBygning.x
							- (oprindeligBygning.bredde / 2) + 10, tmpBygning.y
							+ (tmpBygning.hoejde / 2));
					retVal = DrawLineF(tmpBygning.x, tmpBygning.y
							+ tmpBygning.hoejde, tmpBygning.x
							- (oprindeligBygning.bredde / 2) + 10, tmpBygning.y
							+ (tmpBygning.hoejde / 2));
					retVal = DrawLineF(tmpBygning.x, tmpBygning.y
							+ (tmpBygning.hoejde / 2), tmpBygning.x
							- (oprindeligBygning.bredde / 2) + 10, tmpBygning.y
							+ (tmpBygning.hoejde / 2));
				}
			}

		case 1:
			// 1 = Lodret

			if (sidePlacering == 1 && oprindeligBygning.rygningsRetning == 0) {

				tegnetGavl = false;
				switch (tilbBeskrivelse.placering) {
				case 0: // Venstre side
					if (oprindeligBygning.side2GavlType == 2) { // Helvalm
						// Lidt trekants regning:

						vinkelHoejde = (tmpBygning.bredde / 2) * Tan(p2 * 45);

						retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde,
								tmpBygning.y, tmpBygning.x
										+ (tmpBygning.bredde / 2), tmpBygning.y
										+ vinkelHoejde);

						retVal = DrawLineF(tmpBygning.x
								+ (tmpBygning.bredde / 2), tmpBygning.y,
								tmpBygning.x + (tmpBygning.bredde / 2),
								tmpBygning.y + vinkelHoejde);
						retVal = DrawLineF(tmpBygning.x
								+ (tmpBygning.bredde / 2), tmpBygning.y
								+ vinkelHoejde, oprindeligBygning.x
								+ (oprindeligBygning.hoejde / 2),
								oprindeligBygning.y
										+ (oprindeligBygning.hoejde / 2));
						tegnetGavl = true;
					}
					break;
				case 4: // Hųjre
					if (oprindeligBygning.side3GavlType == 2) { // Helvalm
						// Lidt trekants regning:

						vinkelHoejde = (tmpBygning.bredde / 2) * Tan(p2 * 45);
						retVal = DrawLineF(tmpBygning.x, tmpBygning.y,
								tmpBygning.x + (tmpBygning.bredde / 2),
								tmpBygning.y + vinkelHoejde);
						retVal = DrawLineF(tmpBygning.x
								+ (tmpBygning.bredde / 2), tmpBygning.y,
								tmpBygning.x + (tmpBygning.bredde / 2),
								tmpBygning.y + vinkelHoejde);
						retVal = DrawLineF(tmpBygning.x
								+ (tmpBygning.bredde / 2), tmpBygning.y
								+ vinkelHoejde, oprindeligBygning.x
								+ oprindeligBygning.bredde
								- (oprindeligBygning.hoejde / 2),
								oprindeligBygning.y
										+ (oprindeligBygning.hoejde / 2));
						tegnetGavl = true;
					}
					break;
				}

				if (tegnetGavl == false) {
					retVal = DrawLineF(tmpBygning.x, tmpBygning.y, tmpBygning.x
							+ (tmpBygning.bredde / 2), tmpBygning.y
							+ ((oprindeligBygning.hoejde / 2) - 10));
					retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde,
							tmpBygning.y, tmpBygning.x
									+ (tmpBygning.bredde / 2), tmpBygning.y
									+ ((oprindeligBygning.hoejde / 2) - 10));
					retVal = DrawLineF(strRygning, tmpBygning.y, strRygning,
							tmpBygning.y
									+ ((oprindeligBygning.hoejde / 2) - 10));
				}
			}

			if (sidePlacering == 4 && oprindeligBygning.rygningsRetning == 0) {

				tegnetGavl = false;

				switch (tilbBeskrivelse.placering) {
				case 0: // Venstre side
					if (oprindeligBygning.side2GavlType == 2) { // Helvalm
						// Lidt trekants regning:

						vinkelHoejde = (tmpBygning.bredde / 2) * Tan(p2 * 45);

						retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde,
								tmpBygning.y, tmpBygning.x
										+ (tmpBygning.bredde / 2), tmpBygning.y
										- vinkelHoejde);

						retVal = DrawLineF(tmpBygning.x
								+ (tmpBygning.bredde / 2), tmpBygning.y,
								tmpBygning.x + (tmpBygning.bredde / 2),
								tmpBygning.y - vinkelHoejde);
						retVal = DrawLineF(tmpBygning.x
								+ (tmpBygning.bredde / 2), tmpBygning.y
								- vinkelHoejde, oprindeligBygning.x
								+ (oprindeligBygning.hoejde / 2),
								oprindeligBygning.y
										+ (oprindeligBygning.hoejde / 2));
						tegnetGavl = true;
					}
					break;
				case 4: // Hųjre
					if (oprindeligBygning.side3GavlType == 2) { // Helvalm
						// Lidt trekants regning:

						vinkelHoejde = (tmpBygning.bredde / 2) * Tan(p2 * 45);
						retVal = DrawLineF(tmpBygning.x, tmpBygning.y,
								tmpBygning.x + (tmpBygning.bredde / 2),
								tmpBygning.y - vinkelHoejde);
						retVal = DrawLineF(tmpBygning.x
								+ (tmpBygning.bredde / 2), tmpBygning.y,
								tmpBygning.x + (tmpBygning.bredde / 2),
								tmpBygning.y - vinkelHoejde);
						retVal = DrawLineF(tmpBygning.x
								+ (tmpBygning.bredde / 2), tmpBygning.y
								- vinkelHoejde, oprindeligBygning.x
								+ oprindeligBygning.bredde
								- (oprindeligBygning.hoejde / 2),
								oprindeligBygning.y
										+ (oprindeligBygning.hoejde / 2));
						tegnetGavl = true;
					}
					break;
				}

				if (tegnetGavl == false) {
					retVal = DrawLineF(tmpBygning.x, tmpBygning.y, tmpBygning.x
							+ (tmpBygning.bredde / 2), tmpBygning.y
							- ((oprindeligBygning.hoejde / 2) - 10));
					retVal = DrawLineF(tmpBygning.x + tmpBygning.bredde,
							tmpBygning.y, tmpBygning.x
									+ (tmpBygning.bredde / 2), tmpBygning.y
									- ((oprindeligBygning.hoejde / 2) - 10));
					retVal = DrawLineF(strRygning, tmpBygning.y, strRygning,
							tmpBygning.y
									- ((oprindeligBygning.hoejde / 2) - 10));
					if (hoved == 70000
							&& tmpBygning.side4TilbygningAktiv == true) {
						if (tmpBygning.side4Tilbygning.bygning.rygningsRetning == 0
								&& tmpBygning.rygningsRetning == 1) {
							retVal = DrawLineF(
									tmpBygning.x,
									tmpBygning.y + tmpBygning.hoejde,
									tmpBygning.x + (tmpBygning.bredde / 2),
									tmpBygning.y
											+ tmpBygning.hoejde
											+ ((tmpBygning.side4Tilbygning.bygning.hoejde / 2) - 10));

							retVal = DrawLineF(
									tmpBygning.x + tmpBygning.bredde,
									tmpBygning.y + tmpBygning.hoejde,
									tmpBygning.x + (tmpBygning.bredde / 2),
									tmpBygning.y
											+ tmpBygning.hoejde
											+ ((tmpBygning.side4Tilbygning.bygning.hoejde / 2) - 10));
							retVal = DrawLineF(
									sltRygning,
									tmpBygning.y + tmpBygning.hoejde,
									sltRygning,
									tmpBygning.y
											+ tmpBygning.hoejde
											+ ((tmpBygning.side4Tilbygning.bygning.hoejde / 2) - 10));

						}

					}

				}

			}

			retVal = DrawLineF(strRygning, strRygningY, sltRygning, sltRygningY);
			break;
		case 2:
			// Ingen rygning
			break;
		}

	}

}

function drawimage(url, x, y) {
	image = new Image();
	image.src = url;
	var canvas = document.getElementById("parent");
	var context = canvas.getContext("2d");
	context.drawImage(image, x, y);
}
function DrawLineF(x1, y1, x2, y2, color, linewidth) {
	var canvas = document.getElementById("parent");
	var ctx = canvas.getContext("2d");
	ctx.beginPath();
	ctx.lineJoin = "strict";
	ctx.moveTo(x1, y1);
	ctx.lineTo(x2, y2);

	if (color !== undefined) {
		ctx.strokeStyle = color;
	} else {
		ctx.strokeStyle = "rgb(1,1,1)";
	}
	if (navigator.userAgent.search("Safari") >= 0
			&& navigator.userAgent.search("Chrome") < 0) {
		if (linewidth !== undefined) {
			ctx.lineWidth = linewidth;
		} else {
			ctx.lineWidth = 3;
		}
	} else {
		ctx.lineWidth = 2;
	}

	ctx.stroke(); // Draw it
	LineArx[0] = 0;
	LineAry[0] = 0;

}
function TextHeight(elem) {
	return 16.1615;
}
function TextWidth(elem) {
	return 3.01;
}
function Printtext(text, x, y) {
	var canvas = document.getElementById("parent");
	var context = canvas.getContext("2d");
	context.fillText(text, x, y + 12);
}
function ExtFloodFill(x, y, color) {
	var canvas = document.getElementById("parent");
	var ctx = canvas.getContext("2d");
	x = Math.round(x);
	y = Math.round(y);
	floodFill(x, y, ctx, color, 1);
}
function Tan(x) {
	return Math.tan(x);
}
function Sin(x) {
	return Math.sin(x);
}
function Cos(x) {
	return Math.cos(x);
}
function Sqr(x) {
	return Math.sqrt(x);
}

$(document).ready(function() {
//	$("#canvascont").mousemove(function(event) {
//		var canvas = document.getElementById('parent');
//		var rect = canvas.getBoundingClientRect();
//		x = Math.round(event.clientX - rect.left);
//		y = Math.round(event.clientY - rect.top);
//		document.getElementById("cord").innerHTML = ("x:" + x + " y:" + y);
//	});
});
