//allow only numbers and comma
$(document).ready(function() {
	$(".num").numeric();
});
function isset(str) {
	return window[str] !== undefined;
}

// -------------------------------------------
// -------------form validation---------------
// -------------------------------------------
$(document).ready(function(){
$( "#understrygning" ).on( "click", function() {
	$('#False').click();
});
$( "#True" ).on( "click", function() {
	$('#undertag').click();
});
});
function val_kunde() {
	document.getElementById("kunde").submit();
}
function val_vinkel() {
	num = $('#udluftningshaetter').val().replace(",", ".");
	if (num == "" || isNaN(num) || num.toString().indexOf('.') != -1) {
		alert('Der mangler data i et indtastningsfelt');
	} else {
		document.getElementById("vinkel").submit();
	}
}
function val_maalsaet() {
	for ( var i = 0; i <= 6; i++) {
		if ($('#txtValmTop' + i).length) {
			num = $('#txtValmTop' + i).val().replace(",", ".");
			if (num == "" || isNaN(num)) {
				alert('Der mangler data i et indtastningsfelt');
				return;
			}
		}
		if ($('#txtValmVinkel' + i).length) {
			num = $('#txtValmVinkel' + i).val().replace(",", ".");
			if (num == "" || isNaN(num)) {
				alert('Der mangler data i et indtastningsfelt');
				return;
			}	
		}

	}
	document.getElementById("maalsaet").submit();
}
function val_hvhus1() {

	hvhus1_l = parseFloat($('#hvhus1_l').val().replace(",", "."));
	hvhus1_b = parseFloat($('#hvhus1_b').val().replace(",", "."));
	hvhus1_v = parseFloat($('#hvhus1_v').val().replace(",", "."));
	if ((hvhus1_l == "" || isNaN(hvhus1_l))
			|| (hvhus1_b == "" || isNaN(hvhus1_b))
			|| (hvhus1_v == "" || isNaN(hvhus1_v) || hvhus1_v.toString()
					.indexOf('.') != -1)) {
		alert('Der mangler data i et indtastningsfelt');
		return;
	}
	if (hvhus1_l > 100) {
		alert('Længden må maksimalt være 100m!');
		return;
	}
	if (hvhus1_b > 50) {
		alert('Bredden må maksimalt være 50m!');
		return;
	}
	if (hvhus1_l < 0.5) {
		alert('Længden skal minimum være 50 cm!');
		return;
	}
	if (hvhus1_b < 0.5) {
		alert('Bredden skal minimum være 50 cm!');
		return;
	}
	if (hvhus1_v > 89) {
		alert('Der er indtastet forkert vinkel - max 89°');
		return;
	}
	if (hoved == "6" && hvhus1_b !== hvhus1_l) {
		alert('Længde og bredde på et pyramidetag skal være ens.');
		return;
	}
	if ($("input[name=under][value=undertag]").prop("checked") == true
			&& hvhus1_v < 15) {
		alert('Der er indtastet forkert vinkel - min 15° ved undertag og 20° ved understrygning');
		return;
	}
	if ($("input[name=under][value=understrygning]").prop("checked") == true
			&& hvhus1_v < 20) {
		alert('Der er indtastet forkert vinkel - min 15° ved undertag og 20° ved understrygning');
		return;
	}
	document.getElementById("hvhus1").submit();
}
function val_hvhus2() {

	hvhus2_l = parseFloat($('#hvhus2_l').val().replace(",", "."));

	hvhus2_b = parseFloat($('#hvhus2_b').val().replace(",", "."));

	hvhus2_v = parseFloat($('#hvhus2_v').val().replace(",", "."));

	// ///////////////////////////////////
	if ($('#l2').length > 0) {
		hvhus2_l2 = parseFloat($('#hvhus2_l2').val().replace(",", "."));
	} else {
		hvhus2_l2 = 1;
	}
	if ($('#b2').length > 0) {
		hvhus2_b2 = parseFloat($('#hvhus2_b2').val().replace(",", "."));
	} else {
		hvhus2_b2 = 1;
	}
	if ($('#v2').length > 0) {
		hvhus2_v2 = parseFloat($('#hvhus2_v2').val().replace(",", "."));
	} else {
		hvhus2_v2 = 1;
	}
	if ($('#l3').length > 0) {
		hvhus2_l3 = parseFloat($('#hvhus2_l3').val().replace(",", "."));
	} else {
		hvhus2_l3 = 1;
	}
	if ($('#b3').length > 0) {
		hvhus2_b3 = parseFloat($('#hvhus2_b3').val().replace(",", "."));
	} else {
		hvhus2_b3 = 1;
	}
	if ($('#v3').length > 0) {
		hvhus2_v3 = parseFloat($('#hvhus2_v3').val().replace(",", "."));
	} else {
		hvhus2_v3 = 1;
	}
	// /////////////////////////////////////////
	if ((hvhus2_l > 100) || (hvhus2_l2 > 100) || (hvhus2_l3 > 100)) {
		alert('Længden må maksimalt være 100m!');
		return;
	}
	if ((hvhus2_b > 50) || (hvhus2_b2 > 50) || (hvhus2_b3 > 50)) {
		alert('Bredden må maksimalt være 50m!');
		return;
	}
	if ((hvhus2_l < 0.5) || (hvhus2_l2 < 0.5) || (hvhus2_l3 < 0.5)) {
		alert('Længden skal minimum være 50 cm!');
		return;
	}
	if ((hvhus2_b < 0.5) || (hvhus2_b2 < 0.5) || (hvhus2_b3 < 0.5)) {
		alert('Bredden skal minimum være 50 cm!');
		return;
	}
	if ((hvhus2_l === "" || isNaN(hvhus2_l))
			|| (hvhus2_b === "" || isNaN(hvhus2_b))
			|| (hvhus2_v === "" || isNaN(hvhus2_v) || hvhus2_v.toString()
					.indexOf('.') != -1)) {
		alert('Der mangler data i et indtastningsfelt');
		return;
	}
	if ((hvhus2_l2 === "" || isNaN(hvhus2_l2))
			|| (hvhus2_b2 === "" || isNaN(hvhus2_b2))
			|| (hvhus2_v2 === "" || isNaN(hvhus2_v2) || hvhus2_v2.toString()
					.indexOf('.') != -1)) {
		alert('Der mangler data i et indtastningsfelt');
		return;
	}
	if ((hvhus2_l3 === "" || isNaN(hvhus2_l3))
			|| (hvhus2_b3 === "" || isNaN(hvhus2_b3))
			|| (hvhus2_v3 === "" || isNaN(hvhus2_v3) || hvhus2_v3.toString()
					.indexOf('.') != -1)) {
		alert('Der mangler data i et indtastningsfelt');
		return;
	}

	if (hoved == "6" && hvhus2_b !== hvhus2_l) {
		alert('Længde og bredde på et pyramidetag skal være ens.');
		return;
	}
	if ($("input[name=under][value=undertag]").prop("checked") == true
			&& hvhus2_v < 15) {
		alert('Der er indtastet forkert vinkel - min 15° ved undertag og 20° ved understrygning');
		return;
	}
	if ($("input[name=under][value=understrygning]").prop("checked") == true
			&& hvhus2_v < 20) {
		alert('Der er indtastet forkert vinkel - min 15° ved undertag og 20° ved understrygning');
		return;
	}
	document.getElementById("hvhus2").submit();
}
function val_hvhus3() {
	hvhus3_l = parseFloat($('#hvhus3_l').val().replace(",", "."));
	hvhus3_b = parseFloat($('#hvhus3_b').val().replace(",", "."));
	hvhus3_v = parseFloat($('#hvhus3_v').val().replace(",", "."));
	if ((hvhus3_l == "" || isNaN(hvhus3_l))
			|| (hvhus3_b == "" || isNaN(hvhus3_b))
			|| (hvhus3_v == "" || isNaN(hvhus3_v) || hvhus3_v.toString()
					.indexOf('.') != -1)) {
		alert('Der mangler data i et indtastningsfelt');
		return;
	}
	if (hvhus3_l > 100) {
		alert('Længden må maksimalt være 100m!');
		return;
	}
	if (hvhus3_b > 50) {
		alert('Bredden må maksimalt være 50m!');
		return;
	}
	if (hvhus3_l < 0.5) {
		alert('Længden skal minimum være 50 cm!');
		return;
	}
	if (hvhus3_b < 0.5) {
		alert('Bredden skal minimum være 50 cm!');
		return;
	}
	document.getElementById("hvhus3").submit();
}
function val_vinkel_45() {
	hvhus2_l = parseFloat($('#hvhus2_l').val().replace(",", "."));

	hvhus2_b = parseFloat($('#hvhus2_b').val().replace(",", "."));

	hvhus2_v = parseFloat($('#hvhus2_v').val().replace(",", "."));

	// ///////////////////////////////////
	if ($('#d').length > 0) {
		hvhus2_l2 = parseFloat($('#hvhus2_l2').val().replace(",", "."));
	} else {
		hvhus2_l2 = 1;
	}
	if ($('#e').length > 0) {
		hvhus2_b2 = parseFloat($('#hvhus2_b2').val().replace(",", "."));
	} else {
		hvhus2_b2 = 1;
	}
	// /////////////////////////////////////////
	if ((hvhus2_l > 100) || (hvhus2_l2 > 100)) {
		alert('Længden må maksimalt være 100m!');
		return;
	}
	if ((hvhus2_b > 50) || (hvhus2_b2 > 50)) {
		alert('Bredden må maksimalt være 50m!');
		return;
	}
	if ((hvhus2_l < 0.5) || (hvhus2_l2 < 0.5)) {
		alert('Længden skal minimum være 50 cm!');
		return;
	}
	if ((hvhus2_b < 0.5) || (hvhus2_b2 < 0.5)) {
		alert('Bredden skal minimum være 50 cm!');
		return;
	}
	if ((hvhus2_l === "" || isNaN(hvhus2_l))
			|| (hvhus2_b === "" || isNaN(hvhus2_b))
			|| (hvhus2_v === "" || isNaN(hvhus2_v) || hvhus2_v.toString()
					.indexOf('.') != -1)) {
		alert('Der mangler data i et indtastningsfelt');
		return;
	}
	if ((hvhus2_l2 === "" || isNaN(hvhus2_l2))
			|| (hvhus2_b2 === "" || isNaN(hvhus2_b2))) {
		alert('Der mangler data i et indtastningsfelt');
		return;
	}

	if (hoved == "6" && hvhus1_b !== hvhus1_l) {
		alert('Længde og bredde på et pyramidetag skal være ens.');
		return;
	}
	if ($("input[name=under][value=undertag]").prop("checked") == true
			&& hvhus2_v < 15) {
		alert('Der er indtastet forkert vinkel - min 15° ved undertag og 20° ved understrygning');
		return;
	}
	if ($("input[name=under][value=understrygning]").prop("checked") == true
			&& hvhus2_v < 20) {
		alert('Der er indtastet forkert vinkel - min 15° ved undertag og 20° ved understrygning');
		return;
	}
	document.getElementById("vinkel_45").submit();
}
function val_h_hus() {

	hvhus2_l = parseFloat($('#hvhus2_l').val().replace(",", "."));

	hvhus2_b = parseFloat($('#hvhus2_b').val().replace(",", "."));

	hvhus2_v = parseFloat($('#hvhus2_v').val().replace(",", "."));

	// ///////////////////////////////////
	if ($('#d').length > 0) {
		hvhus2_l2 = parseFloat($('#hvhus2_l2').val().replace(",", "."));
	} else {
		hvhus2_l2 = 1;
	}
	if ($('#e').length > 0) {
		hvhus2_b2 = parseFloat($('#hvhus2_b2').val().replace(",", "."));
	} else {
		hvhus2_b2 = 1;
	}
	if ($('#v2').length > 0) {
		hvhus2_v2 = parseFloat($('#hvhus2_v2').val().replace(",", "."));
	} else {
		hvhus2_v2 = '1v';
	}
	if ($('#g').length > 0) {
		hvhus2_l3 = parseFloat($('#hvhus2_l3').val().replace(",", "."));
	} else {
		hvhus2_l3 = 1;
	}
	if ($('#h').length > 0) {
		hvhus2_b3 = parseFloat($('#hvhus2_b3').val().replace(",", "."));
	} else {
		hvhus2_b3 = 1;
	}
	if ($('#v3').length > 0) {
		hvhus2_v3 = parseFloat($('#hvhus2_v3').val().replace(",", "."));
	} else {
		hvhus2_v3 = '1v';
	}
	// /////////////////////////////////////////
	if ((hvhus2_l > 100) || (hvhus2_l2 > 100) || (hvhus2_l3 > 100)) {
		alert('Længden må maksimalt være 100m!');
		return;
	}
	if ((hvhus2_b > 50) || (hvhus2_b2 > 50) || (hvhus2_b3 > 50)) {
		alert('Bredden må maksimalt være 50m!');
		return;
	}
	if ((hvhus2_l < 0.5) || (hvhus2_l2 < 0.5) || (hvhus2_l3 < 0.5)) {
		alert('Længden skal minimum være 50 cm!');
		return;
	}
	if ((hvhus2_b < 0.5) || (hvhus2_b2 < 0.5) || (hvhus2_b3 < 0.5)) {
		alert('Bredden skal minimum være 50 cm!');
		return;
	}
	if ((hvhus2_l === "" || isNaN(hvhus2_l))
			|| (hvhus2_b === "" || isNaN(hvhus2_b))
			|| (hvhus2_v === "" || isNaN(hvhus2_v) || hvhus2_v.toString()
					.indexOf('.') != -1)) {
		alert('Der mangler data i et indtastningsfelt');
		return;
	}
	if ((hvhus2_l2 === "" || isNaN(hvhus2_l2))
			|| (hvhus2_b2 === "" || isNaN(hvhus2_b2))
			|| (hvhus2_v2 === "" || isNaN(hvhus2_v2) || hvhus2_v2.toString()
					.indexOf('.') != -1)) {
		alert('Der mangler data i et indtastningsfelt');
		return;
	}
	if ((hvhus2_l3 === "" || isNaN(hvhus2_l3))
			|| (hvhus2_b3 === "" || isNaN(hvhus2_b3))
			|| (hvhus2_v3 === "" || isNaN(hvhus2_v3) || hvhus2_v3.toString()
					.indexOf('.') != -1)) {
		alert('Der mangler data i et indtastningsfelt');
		return;
	}

	if (hoved == "6" && hvhus1_b !== hvhus1_l) {
		alert('Længde og bredde på et pyramidetag skal være ens.');
		return;
	}
	if (($("input[name=under][value=undertag]").prop("checked") == true && hvhus2_v < 15)
			|| ($("input[name=under][value=undertag]").prop("checked") == true && hvhus2_v2 < 15)
			|| ($("input[name=under][value=undertag]").prop("checked") == true && hvhus2_v3 < 15)) {
		alert('Der er indtastet forkert vinkel - min 15° ved undertag og 20° ved understrygning');
		return;
	}
	if (($("input[name=under][value=understrygning]").prop("checked") == true && hvhus2_v < 20)
			|| ($("input[name=under][value=understrygning]").prop("checked") == true && hvhus2_v2 < 20)
			|| ($("input[name=under][value=understrygning]").prop("checked") == true && hvhus2_v3 < 20)) {
		alert('Der er indtastet forkert vinkel - min 15° ved undertag og 20° ved understrygning');
		return;
	}
	document.getElementById("h_hus").submit();
}
// -------------------------------------------
// ----------------Form load------------------
// -------------------------------------------
function load_kunde(retning) {
	console.log(retning);
	if (retning['back'] == undefined) {
		$('#kundenavn').val(retning['kundenavn']);
		$('#adresse').val(retning['adresse']);
		$('#by').val(retning['by']);
		$('#telefon').val(retning['telefon']);
		$('#tilbudsnr').val(retning['tilbudsnr']);
		$('#udfort').val(retning['udfort']);
		$('#beregningstidspunkt').val(retning['beregningstidspunkt']);
	}
}
function load_farvevalg() {
	console.log(retning);
	if (retning['back'] == undefined) {
		$("input[name=color][value=" + retning['farvevalgnr'] + "]").click();

	} else {
		$('#radio').click();
	}
}
function load_tagtype() {
	console.log(retning);
	if (retning['back'] == undefined) {
		tag = retning['tag'].toString();
		hoved = parseInt(tag.slice(0, 1));
		tilb = parseInt(tag.slice(1, 2));
		valm = parseInt(tag.slice(2, 3));
		kvist = parseInt(tag.slice(4, 5));
		$('#antalkviste').val(retning['antalkviste']);
		$('#hovedhus' + hoved).prop("checked", "checked");
		$('#tilb' + tilb).prop("checked", "checked");
		$('#valm' + valm).prop("checked", "checked");
		$('#kvist' + kvist).prop("checked", "checked");

		function sel($field, $value) {
			if (parseInt(retning[$value]) == 0) {
				$($field)[0].selectedIndex = retning[$value];
			} else {
				$($field)[0].selectedIndex = retning[$value] - 1;
			}
		}
		sel('#vinklervinkelhus', 'antalVinklerVinkel');
		sel('#tilbygninger', 'antalTilbygninger');
		sel('#knaster', 'antalKnaster');
		sel('#vinkel45grader', 'antalVinkler');
	} else {
		$("#hovedhus1").prop("checked", true);
		$("#tilb1").prop("checked", true);
		$("#valm1").prop("checked", true);
		$("#kvist1").prop("checked", true);
	}
	tagtype();
}
function load_valm() {
	console.log(retning);
	if (retning['back'] == undefined) {
		retning['dynValmType'] = JSON.parse(retning['dynValmType']);
		for ( var i = 1; i <= 6; i++) {
			if (retning['dynValmType'][i] !== 0) {
				$(
						'#valmblock' + i + ' > ul > li > #valm'
								+ retning["dynValmType"][i]).click();
			}
		}
	} else {
		console.log(dynValmType);
		console.log(dynValmPlacering);

		for ( var i = 1; i <= 6; i++) {
			if (dynValmType[i] !== 0) {
				$('#valmblock' + i + ' > ul > li > #valm' + dynValmType[i])
						.prop("checked", "checked");
			}
		}
	}
	valmchange();
}
function load_vinkel() {
	console.log(retning);
	if (retning['back'] == undefined) {
		$('#udluftningshaetter').val(retning['udluftningshaetter']);
	}
}
function load_hvhus1() {
	console.log(retning);
	(retning['hl'] !== undefined) ? $('#hvhus1_l').val(
			parseFloat(retning['hl']) / 1000) : 0;
	(retning['hb'] !== undefined) ? $('#hvhus1_b').val(
			parseFloat(retning['hb']) / 1000) : 0;
	(retning['hv'] !== undefined) ? $('#hvhus1_v').val(
			parseFloat(retning['hv'])) : 0;
	(retning['tagunder'] !== undefined) ? $("#" + retning['tagunder']).prop(
			"checked", true) : 0;
	(retning['facon'] !== undefined) ? $("#" + retning['facon']).prop(
			"checked", true) : 0;
}
function load_hvhus2() {
	console.log(retning);
	(retning['hl'] !== undefined) ? $('#hvhus2_l').val(
			parseFloat(retning['hl']) / 1000) : 0;
	(retning['hb'] !== undefined) ? $('#hvhus2_b').val(
			parseFloat(retning['hb']) / 1000) : 0;
	(retning['hv'] !== undefined) ? $('#hvhus2_v').val(
			parseFloat(retning['hv'])) : 0;
	(retning['hl_v'] !== undefined) ? $('#hvhus2_l2').val(
			parseFloat(retning['hl_v']) / 1000) : 0;
	(retning['hb_v'] !== undefined) ? $('#hvhus2_b2').val(
			parseFloat(retning['hb_v']) / 1000) : 0;
	(retning['hv_V'] !== undefined) ? $('#hvhus2_v2').val(
			parseFloat(retning['hv_V'])) : 0;
	(retning['hl_t'] !== undefined) ? $('#hvhus2_l3').val(
			parseFloat(retning['hl_t']) / 1000) : 0;
	(retning['hl_t'] !== undefined) ? $('#hvhus2_b3').val(
			parseFloat(retning['hb_t']) / 1000) : 0;
	(retning['hl_t'] !== undefined) ? $('#hvhus2_v3').val(
			parseFloat(retning['hv_t'])) : 0;
	(retning['tagunder'] !== undefined) ? $("#" + retning['tagunder']).prop(
			"checked", true) : 0;
	(retning['facon'] !== undefined) ? $("#" + retning['facon']).prop(
			"checked", true) : 0;

}
function load_hvhus3() {
	console.log(retning);
	(retning['hl'] !== undefined) ? $('#hvhus3_l').val(
			parseFloat(retning['hl']) / 1000) : 0;
	(retning['hb'] !== undefined) ? $('#hvhus3_b').val(
			parseFloat(retning['hb']) / 1000) : 0;
	(retning['hv'] !== undefined) ? $('#hvhus3_v').val(
			parseFloat(retning['hv'])) : 0;
	(retning['tagunder'] !== undefined) ? $("#" + retning['tagunder']).prop(
			"checked", true) : 0;

}
function load_vinkel_45() {
	console.log(retning);
	(retning['hl'] !== undefined) ? $('#hvhus2_l').val(
			parseFloat(retning['hl']) / 1000) : 0;
	(retning['hb'] !== undefined) ? $('#hvhus2_b').val(
			parseFloat(retning['hb']) / 1000) : 0;
	(retning['hv'] !== undefined) ? $('#hvhus2_v').val(
			parseFloat(retning['hv'])) : 0;
	(retning['hl_v'] !== undefined) ? $('#hvhus2_l2').val(
			parseFloat(retning['hl_v']) / 1000) : 0;
	(retning['hb_v'] !== undefined) ? $('#hvhus2_b2').val(
			parseFloat(retning['hb_v']) / 1000) : 0;
	(retning['hl_t'] !== undefined) ? $('#hvhus2_l3').val(
			parseFloat(retning['hl_t']) / 1000) : 0;
	(retning['hl_t'] !== undefined) ? $('#hvhus2_b3').val(
			parseFloat(retning['hb_t']) / 1000) : 0;
	(retning['tagunder'] !== undefined) ? $("#" + retning['tagunder']).prop(
			"checked", true) : 0;
	(retning['facon'] !== undefined) ? $("#" + retning['facon']).prop(
			"checked", true) : 0;
}
function load_h_hus() {
	console.log(retning);
	(retning['hl'] !== undefined) ? $('#hvhus2_l').val(
			parseFloat(retning['hl']) / 1000) : 0;
	(retning['hb'] !== undefined) ? $('#hvhus2_b').val(
			parseFloat(retning['hb']) / 1000) : 0;
	(retning['hv'] !== undefined) ? $('#hvhus2_v').val(
			parseFloat(retning['hv'])) : 0;
	(retning['hl_v'] !== undefined) ? $('#hvhus2_l3').val(
			parseFloat(retning['hl_v']) / 1000) : 0;
	(retning['hb_v'] !== undefined) ? $('#hvhus2_b3').val(
			parseFloat(retning['hb_v']) / 1000) : 0;
	(retning['hv_V'] !== undefined) ? $('#hvhus2_v3').val(
			parseFloat(retning['hv_V'])) : 0;
	(retning['hl_t'] !== undefined) ? $('#hvhus2_l2').val(
			parseFloat(retning['hl_t']) / 1000) : 0;
	(retning['hl_t'] !== undefined) ? $('#hvhus2_b2').val(
			parseFloat(retning['hb_t']) / 1000) : 0;
	(retning['hl_t'] !== undefined) ? $('#hvhus2_v2').val(
			parseFloat(retning['hv_t'])) : 0;
	(retning['tagunder'] !== undefined) ? $("#" + retning['tagunder']).prop(
			"checked", true) : 0;
	(retning['facon'] !== undefined) ? $("#" + retning['facon']).prop(
			"checked", true) : 0;

}
