<script>
$( document ).ready(function() {
	kvist = <?php echo $kvist;?>;
	baseurl = <?php echo '"'.base_url('assets/illustration').'"'; ?>;
	kvistfrm(kvist,baseurl);
});
</script>
<?php
$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'class' => 'form-control next',
		'type' => 'submit',
		'content' => 'Frem ->' 
);
$backlbl = "'" . $back . "'";
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $backlbl,
		'content' => '<-- Tilbage         ' 
);
$kvist_capt = array (
		'l' => 'a: Kvistens længde:', 
		'b' => 'b: Kvistens bredde:',
		'v' => 'c: Kvistens vinkel:',
		'v_v' => 'd: Kvistvalmens vinkel:',
		'v_b' => 'e: Kvistvalmens bredde:' 
);
?>



<?php

echo form_open ( 'getdata/kvist', array (
		'id' => 'maalsaet' 
) );
?>
<div class="row">
	<div class="col-md-6">

		<ul class="list-group">
		<?php foreach ( $kvist_capt as $value => $capt ) : ?>	
		<?php
			
if ($value == 'v' || $value == 'v_v') {
				$lett = '°';
			} else {
				$lett = 'm';
			}
			?>
			<li class="list-group-item" id="list<?php echo $value?>">
				<div class="input-group">
					<?php
			echo $capt;
			$data = array (
					'name' => 'kvist' . $value,
					'class' => 'form-control incolright' 
			);
			echo form_input ( $data );
			?>
					<span class="input-group-addon incolrightadd"><?php echo $lett?></span>
				</div>
			</li>

		<?php endforeach;?>
		</ul>
	</div>
	<div class="col-md-6">
		<img id="kvistpic" width="150" height="150">
	</div>
</div>


<?php
echo form_button ( $buttonNext );
echo form_button ( $buttonBack );
echo form_close ();

?>