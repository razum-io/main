<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>IBF Tagsten - <?php (isset($title))?  print_r($title) : false;?></title>

<link href="<?php echo base_url('assets/css/bootstrap.css');?>"
	rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/css/home.css');?>"
	rel="stylesheet" type="text/css">

<script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.js');?>"></script>
<script src="<?php echo base_url('assets/js/forms.js');?>"></script>
<script src="<?php echo base_url('assets/js/ilustration-script.js')?>"></script>
<script src="<?php echo base_url('assets/js/ilustration-form.js')?>"></script>
<script src="<?php echo base_url('assets/js/ilustration-draw.js')?>"></script>
<script src="<?php echo base_url('assets/js/class.js')?>"></script>
<script src="<?php echo base_url('assets/js/formval.js')?>"></script>
<script src="<?php echo base_url('assets/js/floodfill.js')?>"></script>
<script src="<?php echo base_url('assets/js/jqueryvalidator.js')?>"></script>
</head>
<body>
	<div class="container">

		<!-- Static navbar -->
		<div class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo base_url('home/nuberegning');?>"><img
						id="nav_logo" alt="logo"
						src="<?php echo base_url('assets/css/images/logo.png');?>"></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav" id="navia">
						<!-- 						<li class="dropdown"><a href="#" class="dropdown-toggle" -->
						<!-- 						data-toggle="dropdown">Filer <span class="caret"></span></a> -->
						<!-- 						<ul class="dropdown-menu"> -->
						<!-- 						<li><a href="#">Ny beregning</a></li> -->
						<!-- 						<li><a href="#">Åbn</a></li> -->
						<!-- 						</ul></li> -->
						<li><a href="<?php echo base_url('home/nuberegning');?>">Ny
								beregning</a></li>
						<!-- 						<li><a href="">Brugerinfo</a></li> -->
						<!-- 						<li><a href="#">om ansøgning</a></li> -->
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
			<!--/.container-fluid -->
		</div>