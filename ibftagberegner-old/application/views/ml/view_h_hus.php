<style>
#per {
	margin-top: 40px;
}

#imgcont3d {
	padding-top: 150px;
}
</style>
<script>
$( document ).ready(function() {
var stenvalgt = <?php echo '"'.$stenvalgt.'"'; ?>;
h_hus(stenvalgt);
dynValmType = <?php echo $dynValmType; ?>;
baseurl = <?php echo '"'.base_url('assets/illustration').'"'; ?>;
antalVinklerVinkel = <?php echo '"'.$antalvinklervinkel.'"'; ?>;
antalVinkler = <?php echo '"'.$antalvinkler.'"'; ?>;
antalTilbygninger = <?php echo '"'.$antaltilbygninger.'"'; ?>;
antalKnaster = <?php echo '"'.$antalknaster.'"'; ?>;
tag = <?php echo '"'.$tag.'"'; ?>;
drawillustration('h_hus');
retning = (<?php echo json_encode($retning)?>);
load_h_hus(retning);
});
</script>
<?php
$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'class' => 'form-control next',
		'onclick' => 'val_h_hus()',
		'type' => 'button',
		'content' => 'Frem ->' 
);
$back = "'" . base_url ( 'home/vinkel' ) . "'";
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $back,
		'content' => '<-- Tilbage' 
);
$under_capt = array (
		'undertag' => 'Undertag',
		'understrygning' => 'Understrygning' 
);
$valg_af_capt = array (
		'False' => 'Alm. rygningssten',
		'True' => 'Faconrygningssten' 
);
?>
<?php

echo form_open ( 'getdata/h_hus', array (
		'id' => 'h_hus' 
) );
?>
<div class="row">
	<div class="col-md-6">
		<h4>Data for hovedhus 1:</h4>
		<ul class="list-group">

			<li class="list-group-item" id="a">


				<div class="input-group">
		a:  Længde på hus incl. udhæng:
		<?php
		$data = array (
				'name' => 'hvhus2_l',
				'id' => 'hvhus2_l',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>

			<li class="list-group-item" id="b">


				<div class="input-group">
		b:  Bredde på hus incl. udhæng:
		<?php
		$data = array (
				'name' => 'hvhus2_b',
				'id' => 'hvhus2_b',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>

			<li class="list-group-item" id="c">


				<div class="input-group">
		c:  Taghældning:
		<?php
		$data = array (
				'name' => 'hvhus2_v',
				'id' => 'hvhus2_v',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">°</span>
				</div>
			</li>
		</ul>
		<h4>Data for midterbygning:</h4>
		<ul class="list-group">

			<li class="list-group-item" id="d">


				<div class="input-group">
					<span id="labeld">d: Længde på hus:</span>
		<?php
		$data = array (
				'name' => 'hvhus2_l2',
				'id' => 'hvhus2_l2',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>

			<li class="list-group-item" id="e">


				<div class="input-group">
					<span id="labele">e: Bredde på hus:</span>
		<?php
		$data = array (
				'name' => 'hvhus2_b2',
				'id' => 'hvhus2_b2',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>
			<li class="list-group-item" id="v2">


				<div class="input-group">
		f:  Taghældning:
		<?php
		$data = array (
				'name' => 'hvhus2_v2',
				'id' => 'hvhus2_v2',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">°</span>
				</div>
			</li>

		</ul>
		<h4 class="datavinkel2">Data for hovedhus 2</h4>
		<ul class="list-group datavinkel2">

			<li class="list-group-item" id="g">


				<div class="input-group">
		g:  Længde på hus incl. udhæng:
		<?php
		$data = array (
				'name' => 'hvhus2_l3',
				'id' => 'hvhus2_l3',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>

			<li class="list-group-item" id="h">


				<div class="input-group">
		h:  Bredde på hus incl. udhæng:
		<?php
		$data = array (
				'name' => 'hvhus2_b3',
				'id' => 'hvhus2_b3',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>
			<li class="list-group-item" id="v3">


				<div class="input-group">
		i:  Taghældning:
		<?php
		$data = array (
				'name' => 'hvhus2_v3',
				'id' => 'hvhus2_v3',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">°</span>
				</div>
			</li>

		</ul>
	</div>
	<div class="col-md-6" id="per"></div>
</div>
<div class="row">
	<div class="col-md-6">
		<!-- 	Undertag/Understrygning -->
		<h4>Undertag/Understrygning</h4>
		<ul class="list-group">
			
<?php

foreach ( $under_capt as $value => $capt ) :
	$radio_under = array (
			'name' => 'under',
			'value' => $value,
			'id' => $value 
	);
	?>
			<li class="list-group-item">

  <?php echo form_radio($radio_under).$capt?>
  
  			</li>
<?php endforeach; ?>
		</ul>
		<!-- 		Valg af rygningssten -->
	</div>
	<div class="col-md-6" id="valgaf">
		<h4>Valg af rygningssten</h4>
		<ul class="list-group">
			
<?php

foreach ( $valg_af_capt as $value => $capt ) :
	$radio_valg_af = array (
			'name' => 'valg_af',
			'value' => $value,
			'id' => $value 
	);
	?>
			<li class="list-group-item">

  <?php echo form_radio($radio_valg_af).$capt?>
  
  			</li>
<?php endforeach; ?>
		</ul>
	</div>
</div>

<?php
echo form_button ( $buttonNext );
echo form_button ( $buttonBack );
echo form_close ();

?>