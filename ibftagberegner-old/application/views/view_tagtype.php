<script>
$( document ).ready(function() {
	baseurl = <?php echo '"'.base_url('assets/illustration').'"'; ?>;
	tagtype(baseurl);
	retning = <?php echo json_encode($retning)?>;
	load_tagtype(retning);
	});
</script>
<style>
#per{
	margin-top:40px;
}
#imgcont3d{
	margin-top:150px;
}
</style>
<?php
$hovedhus_capt = array (
		'1' => 'Længehus',
		'2' => 'Vinkelhus',
		'3' => 'Tag med ensidig hældning (frit)',
		'4' => 'Tag med ensidig hældning (ved mur)',
		'5' => 'Vinkelhus 45°',
		'6' => 'Pyramidetag',
		'7' => 'H - Hus' 
);
$tilb_capt = array (
		'1' => 'Ingen tilbygning',
		'2' => 'Tilbygning',
		'3' => 'Knast' 
);
$valm_capt = array (
		'1' => 'Gavl',
		'2' => 'Helvalm',
		'3' => 'Halvvalm' 
);
$kvist_capt = array (
		'1' => 'Ingen kvist',
		'2' => 'Kvist med gavltag',
		'3' => 'Kvist med helvalm',
		'4' => 'Kvist med halvvalm' 
);
$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'tagtypesubm()',
		'content' => 'Frem ->' 
);
$back = "'" . base_url ( 'home/farvevalg' ) . "'";
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $back,
		'content' => '<-- Tilbage' 
);
?>

<?php

echo form_open ( 'getdata/tagtype', array (
		'id' => 'tagtype' 
) );
?>

<h4>Vælg ønsket tag, ved at markere felterne</h4>
<div class="row">
	<div class="col-md-6">
	
		<h4>Valg af hovedhus</h4>
		<ul class="list-group">	
	
<?php

foreach ( $hovedhus_capt as $num => $capt ) :
	$radio_hovedhus = array (
			'name' => 'hovedhus',
			'id' => 'hovedhus' . $num,
			'value' => $num,
			'onclick' => 'tagtype()' 
	);
	?>

 <?php if ($num==2 || $num==5):?>
  <li class="list-group-item withdropdown">
  <?php else:?>
  
			
			
			
			<li class="list-group-item">
  <?php endif;?>
  <?php echo form_radio($radio_hovedhus).$capt?>
  <?php if ($num==2):?>
  <select name="antalvinklervinkel" class="form-control" id="vinklervinkelhus"
				onchange="tagtype()">
					<option value="1vinkel">1 Vinkel</option>
					<option value="2vinkler">2 Vinkler</option>
			</select>
  <?php elseif ($num==5):?>
   <select name="antalvinkler" class="form-control" id="vinkel45grader" onchange="tagtype()">
					<option value="1vinkel">1 Vinkel</option>
					<option value="2vinkler">2 Vinkler</option>
			</select>
  <?php endif;?>
  </li>
<?php endforeach; ?>
	</ul>
	</div>
	<!-- /.col-md-6 -->
	<div class="col-md-6" id="per">
		
		
	
	</div>
</div>
<!-- /.row -->
<div class="row">
	<div class="col-md-6">
		<!-- Valg of hofedus -->
		<h4>Valg af hovedhus</h4>
		<ul class="list-group">	
	
<?php

foreach ( $tilb_capt as $num => $capt ) :
	$radio_tilb = array (
			'name' => 'tilb',
			'id' => 'tilb' . $num,
			'value' => $num,
			'onclick' => 'tagtype()' 
	);
	?>
	
 <?php if ($num==2 || $num==3):?>
  <li class="list-group-item withdropdown">
  <?php else:?>
  
			
			
			
			<li class="list-group-item">
  <?php endif;?>
  <?php echo form_radio($radio_tilb).$capt?>
  <?php if ($num==2):?>
  <select name="antaltilbygninger" class="form-control" id="tilbygninger" onchange="tagtype()">
					<option value="1tilbygning">1 Tilbygning</option>
					<option value="2tilbygninger">2 Tilbygninger</option>
			</select>
  <?php elseif ($num==3):?>
  <select name="antalknaster" class="form-control" id="knaster" onchange="tagtype()">
					<option value="1knast">1 Knast</option>
					<option value="2knaster">2 Knaster</option>
			</select>
  <?php endif;?>
  </li>
<?php endforeach; ?>
	</ul>
		<!-- 	End of Valg of hofedus -->
		<br>
		<!-- 		Beggining of Valg af valme -->
		<h4>Valg af valme</h4>
		<ul class="list-group">	
	
<?php

foreach ( $valm_capt as $num => $capt ) :
	$radio_valm = array (
			'name' => 'valm',
			'id' => 'valm' . $num,
			'value' => $num,
			'onclick' => 'tagtype()' 
	);
	?>
<li class="list-group-item">
<?php echo form_radio($radio_valm).$capt;?>
</li>
<?php
endforeach
;
?>
	</ul>
	</div>
	<!-- /.col-md-6 -->
	<div class="col-md-6">
		<h4>Valg af kviste</h4>
		<ul class="list-group">
			<li class="list-group-item">
		Antal kviste
		<?php
		$data = array (
				'name' => 'antalkviste',
				'id' => 'antalkviste',
				'value' => '0',
				'class' => 'form-control incol',
				'onchange' => 'tagtype()' 
		);
		echo form_input ( $data );
		?>
		</li>
<?php

foreach ( $kvist_capt as $num => $capt ) :
	$radio_kvist = array (
			'name' => 'kvist',
			'id' => 'kvist' . $num,
			'value' => $num,
			'onclick' => 'tagtype()' 
	);
	?>
	<li class="list-group-item">

  <?php echo form_radio($radio_kvist).$capt?>
  
  </li>
<?php endforeach; ?>
</ul>
<?php
echo form_button ( $buttonNext );
echo form_button ( $buttonBack );
echo form_close ();
?>
	</div>
	<!-- /.col-md-6 -->
</div>
<!-- /.row -->
