<script>
$(document).ready(function(){
	$(".clickableRow").click(function() {
		window.document.location = $(this).attr("href");
	});
})
</script>
<div class="row">
			<div class="col-md-12 ">
				<div class="panel panel-primary">
					<div class="panel-heading">Colors <a href="<?=base_url('color/add_color/')?>" style="color:white; float:right" class=panelLink>Add Color</a></div>
					<table class="table">
						<tr>
							<th>Name</th>
							<th>Dobbelt-S</th>
							<th>Vinge Okonomi</th>
							<th>Vinge Okonomi plus</th>
							<th></th>
						</tr>
						
						<? foreach($data as $row){ ?>
						<tr>
							<td><?=$row->name?>
							<?if ($row->hidden) echo "<BR>(hidden)";?>
							</td>
							<td><? if(($row->dobbelt)){ ?><img src="<?=base_url('/assets/img/'.$row->dobbelt_file)?>"/><? } ?></td>
							<td><? if(($row->vinge)){ ?><img src="<?=base_url('/assets/img/'.$row->vinge_file)?>"/><? } ?></td>
							<td><? if(($row->vinge_plus)){ ?><img src="<?=base_url('/assets/img/'.$row->vinge_plus_file)?>"/><? } ?></td>
							<td>
								<?// if($row->code==""){ ?>
								<?// } ?>
								<a class="btn" href="<?=base_url('color/update_color/'.$row->id)?>"
									>
									[Edit]
								</a>
								<a class="btn" href="<?=base_url('color/copy_color/'.$row->id)?>"
									>
									[Copy]
								</a>
								<? if ($row->hidden) { ?>
								<a class="btn" href="<?=base_url('color/show/'.$row->id)?>">
									[Show]
								</a>
								<? } else { ?>
								<a class="btn" href="<?=base_url('color/hide/'.$row->id)?>">
									[Hide]
								</a>
								<? } ?>
								<a class="btn" href="<?=base_url('color/delete/'.$row->id)?>"
									onclick="return confirm('Are you sure?');">
									[Delete]
								</a>
							</td>
						</tr>
						<?}?>

					</table>
				</div>
				<center>

			</div>
		</div>