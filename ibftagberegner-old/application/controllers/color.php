<?
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Color extends CI_Controller {
		
	
	function check2int($val){
		if($val=='on'){
			$i = 1;
		}else{
			$i = 0;
		}
		if($val=='0'){
			$i=0;
		}
		return $i;
	}
		
	public function index(){
		$data ['title'] = '';
		$this->load->view ( '/include/header', $data );
		
		$this->load->model('model_color');
		$colorList['data'] = $this->model_color->get();
		$this->load->view ( 'view_color', $colorList );
		
	}
	
	public function copy_color($id){
		$this->load->model('model_color');
		$color=$this->model_color->get_id($id);
		
		$data=array(
			'name'=>'Copy_of_'.$color['name'],
			'dobbelt'=>$color['dobbelt'],
			'vinge'=>$color['vinge'],
			'vinge_plus'=>$color['vinge_plus'],
			'dobbelt_file'=>$color['dobbelt_file'],
			'vinge_file'=>$color['vinge_file'],
			'vinge_plus_file'=>$color['vinge_plus_file'],
			'field'=>$color['field']
		);
		
		$this->model_color->insert($data);
		$last_id=$this->db->insert_id();
		$this->load->model('model_color_parts');
		$queryData=array(
			$color['field']=>'Ja'
		);
		$varerList=$this->model_color_parts->get_varer($queryData);
		foreach($varerList as $obj){
			$data=array();
			$data=array(
				'color_id'=>$last_id,
				'ibf1'=>$obj->IBF,
				'tun1'=>$obj->Tun,
				'navn1'=>$obj->Navn,
				'ibf2'=>$obj->IBF,
				'tun2'=>$obj->Tun,
				'navn2'=>$obj->Navn
			);
			$this->model_color_parts->insert($data);
		}
		// echo "<pre>";
		// print_r($varerList);
		// die();
		redirect(base_url('/color'));
	}
	
	public function add_color(){
		$data ['title'] = '';
		$this->load->view ( '/include/header', $data );
		$data['name']="";
		$data['dobbelt']="";
		$data['vinge']="";
		$data['vinge_plus']="";
		$data['dobbelt_file']="";
		$data['vinge_file']="";
		$data['vinge_plus_file']="";
		$data['action']='insert';
		$this->load->view ( 'view_color_form', $data );
	}
	
	public function update_color($id){
		$data ['title'] = '';
		$this->load->view ( '/include/header', $data );
		$this->load->model('model_color');
		$data=$this->model_color->get_id($id);
		$data['action']='update/'.$id;
		$this->load->view ( 'view_color_form', $data );
		
		$this->load->model('model_color_parts');
		$queryData=array(
			'color_id'=>$id
		);
		$data['id']=$id;
		$data['data']=$this->model_color_parts->get($queryData);
		$this->load->view('view_color_parts', $data);
		
	}
	
	public function color_parts_update($id){
		$idList= $_POST['id'];
		$replace_what = $_POST['replace_what'];
		$replace_with = $_POST['replace_with'];
		
		$this->load->model('model_color_parts');
		foreach($idList as $key=>$value){
			// find replace first
			if ($replace_what!='')
				$_POST['navn2_'.$value] = str_replace($replace_what,$replace_with,$_POST['navn2_'.$value]);
			
			$data=array();
			$data=array(
				'ibf2'=>$_POST['ibf2_'.$value],
				'tun2'=>$_POST['tun2_'.$value],
				'navn2'=>$_POST['navn2_'.$value]
			);
			$this->model_color_parts->update($data,$value);
		}
		
		
		
		redirect(base_url('/color/update_color/'.$id));
	}
	
	public function update($id){
		$config['upload_path'] = './assets/img';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);
		$files=$_FILES;
		$err=true;
		if(isset($_POST['dobbelt'])){
			if(!isset($files['dobbelt_file'])){
				$err=false;
			}
		}else{
			$_POST['dobbelt']=0;
		}
		if(isset($_POST['vinge'])){
			if(!isset($files['vinge_file'])){
				$err=false;
			}
		}else{
			$_POST['vinge']=0;
		}
		if(isset($_POST['vinge_plus'])){
			if(!isset($files['vinge_plus_file'])){
				$err=false;
			}
		}else{
			$_POST['vinge_plus']=0;
		}
		if($err){
			$data=array(
				'name'=>$_POST['name'],
				'dobbelt'=>$this->check2int($_POST['dobbelt']),
				'vinge'=>$this->check2int($_POST['vinge']),
				'vinge_plus'=>$this->check2int($_POST['vinge_plus'])
			);
			$this->load->model('model_color');
			$this->model_color->update($data,$id);
			$last_id=$id;
			
			
			$dataUpdate=array();
			
			if($files['dobbelt_file']['name']!=""){
				$dataUpdate['dobbelt_file']=$last_id.'_dobbelt_file.jpg';
				$_FILES['userfile']['name']= $last_id.'_dobbelt_file.jpg';
				$_FILES['userfile']['type']= $files['dobbelt_file']['type'];
				$_FILES['userfile']['tmp_name']= $files['dobbelt_file']['tmp_name'];
				$_FILES['userfile']['error']= $files['dobbelt_file']['error'];
				$_FILES['userfile']['size']= $files['dobbelt_file']['size'];
				$this->upload->do_upload();
			}
			if($files['vinge_file']['name']!=""){
				$dataUpdate['vinge_file']=$last_id.'_vinge_file.jpg';
				$_FILES['userfile']['name']= $last_id.'_vinge_file.jpg';
				$_FILES['userfile']['type']= $files['vinge_file']['type'];
				$_FILES['userfile']['tmp_name']= $files['vinge_file']['tmp_name'];
				$_FILES['userfile']['error']= $files['vinge_file']['error'];
				$_FILES['userfile']['size']= $files['vinge_file']['size'];
				$this->upload->do_upload();
			}
			if($files['vinge_plus_file']['name']!=""){
				$dataUpdate['vinge_plus_file']=$last_id.'_vinge_plus_file.jpg';
				$_FILES['userfile']['name']= $last_id.'_vinge_plus_file.jpg';
				$_FILES['userfile']['type']= $files['vinge_plus_file']['type'];
				$_FILES['userfile']['tmp_name']= $files['vinge_plus_file']['tmp_name'];
				$_FILES['userfile']['error']= $files['vinge_plus_file']['error'];
				$_FILES['userfile']['size']= $files['vinge_plus_file']['size'];
				$this->upload->do_upload();
			}
			if(count($dataUpdate)!=0)
				$this->model_color->update($dataUpdate,$last_id);
		}else{
			
		}
		redirect(base_url('/color'));
		
	}
	
	public function delete($id){
		$this->load->model('model_color');
		$data=array(
			'id'=>$id
		);
		$this->model_color->delete($data);
		redirect(base_url('/color'));
	}
	
	public function show($id){
		$this->load->model('model_color');
		$data=array(
			'id'=>$id
		);
		$this->model_color->show($data);
		redirect(base_url('/color'));
	}

	public function hide($id){
		$this->load->model('model_color');
		$data=array(
			'id'=>$id
		);
		$this->model_color->hide($data);
		redirect(base_url('/color'));
	}

	public function insert(){
		$config['upload_path'] = './assets/img';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);
		$files=$_FILES;
		$err=true;
		if(isset($_POST['dobbelt'])){
			if(!isset($files['dobbelt_file'])){
				$err=false;
			}
		}else{
			$_POST['dobbelt']=0;
		}
		if(isset($_POST['vinge'])){
			if(!isset($files['vinge_file'])){
				$err=false;
			}
		}else{
			$_POST['vinge']=0;
		}
		if(isset($_POST['vinge_plus'])){
			if(!isset($files['vinge_plus_file'])){
				$err=false;
			}
		}else{
			$_POST['vinge_plus']=0;
		}
		if($err){
			$data=array(
				'name'=>$_POST['name'],
				'dobbelt'=>$this->check2int($_POST['dobbelt']),
				'vinge'=>$this->check2int($_POST['vinge']),
				'vinge_plus'=>$this->check2int($_POST['vinge_plus'])
			);
			$this->load->model('model_color');
			$this->model_color->insert($data);
			$last_id=$this->db->insert_id();
			
			
			$dataUpdate=array();
			
			if($files['dobbelt_file']['name']!=""){
				$dataUpdate['dobbelt_file']=$last_id.'_dobbelt_file.jpg';
				$_FILES['userfile']['name']= $last_id.'_dobbelt_file.jpg';
				$_FILES['userfile']['type']= $files['dobbelt_file']['type'];
				$_FILES['userfile']['tmp_name']= $files['dobbelt_file']['tmp_name'];
				$_FILES['userfile']['error']= $files['dobbelt_file']['error'];
				$_FILES['userfile']['size']= $files['dobbelt_file']['size'];
				$this->upload->do_upload();
			}
			if($files['vinge_file']['name']!=""){
				$dataUpdate['vinge_file']=$last_id.'_vinge_file.jpg';
				$_FILES['userfile']['name']= $last_id.'_vinge_file.jpg';
				$_FILES['userfile']['type']= $files['vinge_file']['type'];
				$_FILES['userfile']['tmp_name']= $files['vinge_file']['tmp_name'];
				$_FILES['userfile']['error']= $files['vinge_file']['error'];
				$_FILES['userfile']['size']= $files['vinge_file']['size'];
				$this->upload->do_upload();
			}
			if($files['vinge_plus_file']['name']!=""){
				$dataUpdate['vinge_plus_file']=$last_id.'_vinge_plus_file.jpg';
				$_FILES['userfile']['name']= $last_id.'_vinge_plus_file.jpg';
				$_FILES['userfile']['type']= $files['vinge_plus_file']['type'];
				$_FILES['userfile']['tmp_name']= $files['vinge_plus_file']['tmp_name'];
				$_FILES['userfile']['error']= $files['vinge_plus_file']['error'];
				$_FILES['userfile']['size']= $files['vinge_plus_file']['size'];
				$this->upload->do_upload();
			}
			if(count($dataUpdate)!=0)
				$this->model_color->update($dataUpdate,$last_id);
		}else{
			
		}
		redirect(base_url('/color'));
	}
}